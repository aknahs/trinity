#include "scheduler.h"

//this function is runned in a re-used thread by the worker_pool.c
void *handle_client(void *data){
    thread_ctx *ctx = (thread_ctx *) data;
    int socket = ctx->socket;
    char buf[CLIENT_REGISTER_MSG_LEN] = "";
    char * bptr = buf; 
    fd_set read_fds;
    struct timeval timeout;
    int len;

    //Select will wait for 1 minute
    timeout.tv_sec  = CLIENT_REGISTER_TIMEOUT;
    timeout.tv_usec = 0;

    FD_SET(socket, &read_fds);

    //not using select would consume cpu while waiting for message
    //i want to avoid funny multiple connection hacks. Timeout does the trick
    int sel = select(socket+1, &read_fds, NULL, NULL, &timeout);
    if(sel > 0) {

        memset(buf,0,CLIENT_REGISTER_MSG_LEN);
        // handle data from a client
        if(receive_all_fixed_size(socket,buf,CLIENT_REGISTER_MSG_LEN)!=-1){
            do_debug(LOG_LEVEL_LOW,"handle_client : Received : %s\n",buf);
        }
        else{
            do_debug(LOG_LEVEL_LOW,"handle_client : could not receiveall\n");
        }

        worker *wk = get_worker_next();

        if(wk !=NULL){

            //int traffic_port; //for communicating with client
            //int control_port; //for communicating with all clients
            //int register_port; //for communicating with scheduler

            sprintf(bptr,"%d|%s|%d|%d|%d|%d", wk->id, wk->ip, wk->traffic_port, wk->control_port, wk->ctip, 0);
            /*
            //set ip
            strcpy(bptr,wk->ip);

            //set port
            len = strlen(bptr);
            bptr = bptr+len+1;
            sprintf(bptr,"%d",wk->port);

            //set id
            len = strlen(bptr);
            bptr = bptr+len+1;
            sprintf(bptr,"%d",wk->id);
            */

            do_debug(LOG_LEVEL_LOW,"handle_client - Sending %s\n",buf);
            if(send_all_fixed_size(socket, buf, CLIENT_REGISTER_MSG_LEN)==-1)
                do_debug(LOG_LEVEL_LOW,"handle_client : could not sendall\n");
        }
        else{
            do_debug(LOG_LEVEL_LOW,"handle_client : Could not retrieve a worker!\n");
        }

    }
    else
        do_debug(LOG_LEVEL_LOW,"handle_client : Select failed or timed out\n");

    close(socket);
    free(ctx);

    return NULL;
}

void *worker_register_thread(){
    fd_set read;
    int listener = create_tcp_server_socket(WORKER_REGISTER_PORT);
    control_message *msg;
    char *message;
    int worker_id;
    int counter=0;
    int i;

    struct sockaddr_storage remoteaddr; // worker address
    socklen_t addrlen;
    int newfd;

    char worker_ip[INET6_ADDRSTRLEN];
    int worker_port;

    int worker_traffic_port,worker_control_port,worker_scheduler_port;

    for(;;){
        FD_ZERO(&read);
        FD_SET(listener, &read);

        do_debug(LOG_LEVEL_LOW,"worker_register_thread : listening for worker register\n");

        if (select(listener+1, &read, NULL, NULL, NULL) == -1){
                perror("worker_register_thread : error on select");
                //exit(4);
        }

        addrlen = sizeof remoteaddr;
        newfd = accept(listener,
                        (struct sockaddr *)&remoteaddr,
                        &addrlen);

        if (newfd == -1) {
            perror("accept");
            continue;
        } else {
                        
            msg = receive_all_variable_size(newfd);

            if(msg != NULL){
                message = msg->message;

                do_debug(LOG_LEVEL_LOW,"Retrieving worker from : %s\n",message);

                for(i=0; i < msg->size; i++)
                    if(message[i]=='|')
                        message[i]='\0';

                //Sets the ip of the client (client_ip)
                if(inet_ntop(remoteaddr.ss_family,get_in_addr((struct sockaddr*)&remoteaddr),worker_ip, INET6_ADDRSTRLEN) == NULL){
                  perror("client_listen_control_messages : inet_ntop");
                  continue;
                  //exit(4);
                }

                do_debug(LOG_LEVEL_LOW,"worker_register_thread : Setted worker_ip to %s\n",worker_ip);
                
                struct sockaddr *sa = (struct sockaddr *) &remoteaddr;

                //Setting client port
                if (sa->sa_family == AF_INET)
                  worker_port = (int) ((struct sockaddr_in *)sa)->sin_port;
                else
                  worker_port = (int) ((struct sockaddr_in6 *)sa)->sin6_port;

                //traffic_port,control_port,scheduler_port
                worker_traffic_port=atoi(message);
                counter = strlen(message) +1;
                worker_control_port=atoi(message + counter);
                counter += strlen(message + counter) +1;
                worker_scheduler_port=atoi(message + counter);

                worker_id = register_worker(worker_ip,worker_control_port,worker_traffic_port,worker_scheduler_port);
                //char *ip,int ctrport,int traffport, int regport

                do_debug(LOG_LEVEL_LOW,"Registered worker %d - %s:%d:%d:%d\n",worker_id,worker_ip,worker_control_port,worker_traffic_port,worker_scheduler_port);
                
                free(msg->message);
                free(msg);
                close(newfd); 
            }
            else
                do_debug(LOG_LEVEL_LOW,"Could not retrieve message.\n");
        }
    }
}

int main(void)
{
    fd_set master;    // master file descriptor list
    fd_set read_fds;  // temp file descriptor list for select()
    int fdmax;        // maximum file descriptor number

    int listener;     // listening socket descriptor
    int newfd;        // newly accept()ed socket descriptor
    struct sockaddr_storage remoteaddr; // client address
    socklen_t addrlen;

    char remoteIP[INET6_ADDRSTRLEN];

    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    pthread_t worker_reg_t;

    do_debug(LOG_LEVEL_LOW,"Welcome to the scheduler\n");


    if(DUMMY_WORKER){
        //int worker_id = register_worker("176.34.202.100",55560,55556,9034); //ip, ctr, traffic, reg
        //int worker_id = register_worker("176.34.202.100",55560);
    }
    //else{
        if(pthread_create(&worker_reg_t, NULL, worker_register_thread, NULL) != 0)
            perror("error: pthread");
    //}

    listener = create_tcp_server_socket(CLIENT_REGISTER_PORT);

    //THREAD_POOL CODE----------------
    do_debug(LOG_LEVEL_LOW,"Initializing... please wait\n");
    init_threads();
    sleep(10);
    do_debug(LOG_LEVEL_LOW,"Pool of threads initialized!\n");
    thread_inception = &handle_client;
    thread_ctx *ctx = NULL;
    //--------------------------------

    FD_SET(listener, &master);
    fdmax = listener;

    for(;;) {
        read_fds = master;
        do_debug(LOG_LEVEL_LOW,"On select...\n");
        if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
            perror("select");
            //exit(4);
        }
        do_debug(LOG_LEVEL_LOW,"Passed select...\n");

 
        if (FD_ISSET(listener, &read_fds)) {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
                    newfd = accept(listener,
                        (struct sockaddr *)&remoteaddr,
                        &addrlen);

                    if (newfd == -1) {
                        perror("accept");
                    } else {
                        do_debug(LOG_LEVEL_LOW,"selectserver: new connection from %s on "
                            "socket %d\n",
                            inet_ntop(remoteaddr.ss_family,
                                get_in_addr((struct sockaddr*)&remoteaddr),
                                remoteIP, INET6_ADDRSTRLEN),
                            newfd);
                        //this could be done once per thread in the pool..oh well
                        ctx = (thread_ctx *) malloc(sizeof(thread_ctx));
                        ctx->socket = newfd;
                        run_thread((void *) ctx);
                    }
        }
    }
    
    return 0;
}



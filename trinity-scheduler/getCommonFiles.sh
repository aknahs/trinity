#!/bin/bash

echo "Copying common files to trinity-scheduler..."
for i in utils worker_pool thread_pool
do
		echo "--->Copy $i to trinity-scheduler/"
		cp ../common/$i.* .
done
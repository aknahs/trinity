#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "utils.h"
#include "thread_pool.h"
#include "worker_pool.h"

//for debug purposes (pre-inserted worker)
#define DUMMY_WORKER 1

//Client listening macros
#define WORKER_REGISTER_PORT 9036 //separated for security reasons

#define CLIENT_REGISTER_PORT 9034
#define CLIENT_REGISTER_MSG_LEN 128
#define CLIENT_REGISTER_TIMEOUT 30

//for the listener
#define SCHEDULER_MAX_USERS 100

typedef struct th_ctx{
    int socket;
} thread_ctx;

//this function is runned in a re-used thread by the worker_pool.c
void *handle_client(void *data);

#endif
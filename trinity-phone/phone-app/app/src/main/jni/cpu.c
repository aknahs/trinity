#include <sys/time.h>
#include <time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>                                                                  
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <dirent.h>
#include <sys/stat.h>

#include "utils.h"

#define SLEEP_TIME 1
#define INITIAL_SLEEP 30

/* Vbes used for the logger */
#define LOGGER_BUFLEN 512
#define PORT 9930
#define SRV_IP "127.0.0.1"

#define DEFAULT_PROC "/system/bin/spectrumproxy"

typedef struct{
    long unsigned utime;
    long unsigned stime;
    long unsigned totaltime;
} cputime;

void diep(const char *msg)
{
    perror(msg);
    exit(0);
}

long unsigned previous_val = 100;

/****************************************
* function get_cpu_times
*
* usage: returns a cputime struct for a
*        given process
****************************************/
cputime get_cpu_times(pid_t lpid){
    char buf[512],*begptr ,*endptr;
    cputime result;
    int i,j;
    unsigned long partial_cpu=0;

    //Open and close asap
    snprintf(buf, sizeof(buf), "/proc/stat");
    FILE* cpufp = fopen(buf, "r");
    //Get the total cpu time
    fgets(buf, sizeof(buf), cpufp);
    fclose(cpufp);

    begptr = buf;
    for(i=0;begptr[i]!='\0';i++)
        if(begptr[i]==' '){
            begptr += i + 1;
            break;
        }

    //Sum cpu entries
    while((partial_cpu=strtol(begptr, &endptr, 10))!='\0'){
        result.totaltime += partial_cpu;
        begptr = endptr;
    }

    //Open and close asap
    snprintf(buf, sizeof(buf), "/proc/%ld/stat", (long) lpid);
    FILE* pidfp = fopen(buf, "r");
    //Get the utime and stime
    fgets(buf, sizeof(buf), pidfp);
    fclose(pidfp);

    //top.c does this with very long sscanf
    //this should be faster me thinks! :P
    begptr = buf;
    for(j=0;j!=13;begptr+=1)
        if(*begptr==' ')
            j++;

    result.utime = strtol(begptr, &endptr, 10);
    begptr = endptr;
    result.stime = strtol(begptr, &endptr, 10);
    
    return result;
}

/**************************************************************************
* function: print_log
*
* usage: communicates with logger via local UDP socket                                                          *
**************************************************************************/
void print_log(char *buf, size_t sbuf){

    struct sockaddr_in si_other;
    int s, slen=sizeof(si_other);
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
        printf("ERROR: UDP LOGGER socket");
        exit(1);
    }
    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);
    if (inet_aton(SRV_IP, &si_other.sin_addr)==0) {
        printf("ERROR: UDP LOGGER inet_aton() failed\n");
        exit(1);
    }
    if (sendto(s, buf, sbuf, 0, &si_other, slen)==-1){
        diep("sendto()");
    }
    close(s);
}

/****************************************
* function proc_find
*
* usage: returns the pid of a given process
****************************************/
pid_t proc_find(const char* name) 
{
    DIR* dir;
    struct dirent* ent;
    char* endptr;
    char buf[LOGGER_BUFLEN];


    if (!(dir = opendir("/proc"))) {
        perror("can't open /proc");
        return -1;
    }

    while((ent = readdir(dir)) != NULL) {
        /* if endptr is not a null character, the directory is not
         * entirely numeric, so ignore it */
        long lpid = strtol(ent->d_name, &endptr, 10);
        if (*endptr != '\0') {
            continue;
        }

        /* try to open the cmdline file */
        snprintf(buf, sizeof(buf), "/proc/%ld/cmdline", lpid);
        FILE* fp = fopen(buf, "r");

        if (fp) {
            if (fgets(buf, LOGGER_BUFLEN, fp) != NULL) {
                /* check the first token in the file, the program name */
                char* first = strtok(buf, " ");
                if (!strcmp(first, name)) {
                    fclose(fp);
                    closedir(dir);
                    return (pid_t)lpid;
                }
            }
            fclose(fp);
        }

    }

    closedir(dir);
    return -1;
}

int main(int argc, char *argv[]){

    char proc_name[512];

    do_debug(LOG_LEVEL_LOW,"Starting Cpu logging\n");
    if(argc == 2)
        strcpy(proc_name,argv[1]);
    else
        strcpy(proc_name,DEFAULT_PROC);

    do_debug(LOG_LEVEL_LOW,"Logging %s\n", proc_name);

    pid_t pidproxy = proc_find(proc_name);
    //Used to send data to the logger
    char log_buffer [LOGGER_BUFLEN];
    long unsigned user_util, sys_util,totaldelta;
    cputime currentcpu = {0,0,0}, previouscpu = {0,0,0};

    do_debug(LOG_LEVEL_HIGH,"#CPU,deltau_u,delta_s,delta_total,perc_u,perc_sys\n");

    for(currentcpu=get_cpu_times(pidproxy); 1; previouscpu = currentcpu, currentcpu=get_cpu_times(pidproxy)){
        
        totaldelta = currentcpu.totaltime - previouscpu.totaltime;
  
        user_util = (currentcpu.utime - previouscpu.utime) * 100 / totaldelta;
        sys_util = (currentcpu.stime - previouscpu.stime) * 100 / totaldelta;
 
        //if(sys_util != previous_val){
            memset(log_buffer, 0, LOGGER_BUFLEN);
            sprintf(log_buffer, "CPU,%ld,%ld,%ld,%ld%%,%ld%%\n",(currentcpu.utime - previouscpu.utime),
            		(currentcpu.stime - previouscpu.stime),totaldelta, user_util,sys_util);
            do_debug(LOG_LEVEL_HIGH,log_buffer);
        //}  

        previous_val = sys_util;
        sleep(SLEEP_TIME);
    }

    return 0;
}

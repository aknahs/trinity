#ifndef TUN_CLIENT_H_
#define TUN_CLIENT_H_

#include "tun_tools.h"
#include "worker_pool.h" //to get the worker structure
#include "utils.h"
#include "rule_set.h"
#include "packet_actions.h"

//known ips (note this should be names instead)
//#define SCHEDULER_IP "176.34.202.100"

//Worker traffic notification macros
//#define WORKER_TRAFFIC_PORT 55556
#define WORKER_TRAFFIC_MSG_LEN 4096

//Worker monitoring notification macros
//#define WORKER_CONTROL_PORT 55560
#define WORKER_CONTROL_MSG_LEN 4096

//Scheduler notification macros
#define SCHEDULER_REGISTER_PORT 9034
#define SCHEDULER_REGISTER_MSG_LEN 128 //TODO: to rethink later

//Change tunnel route port
#define ROUTE_CONTROL_PORT 55666
#define ROUTE_CONTROL_SIZE 70

#define EVENT_LISTEN_PORT 55559
#define EVENT_MSG_LEN 128 //ip33 + 3 proto + 1 separator + 1 terminator

//tun interface name
#define IF_NAME "tun11"
#define IF_PATH "/dev/tun"

//Time in between statistical saves (seconds)
#define STATS_INTERVAL 120

#define RRC_LISTEN_PORT 9930
#define RRC_STATE_LENGTH 24
#define RRC_TIMEOUT 30

int tap_fd,server_fd;
struct sockaddr_in client,server;
worker *registered_worker = NULL;
char rrc_state[RRC_STATE_LENGTH];

/*
Naming convention : (module it communicates with)_notify/listen_function
*/

//gets a worker from the scheduler (blocking!)
worker *scheduler_notify_register_client(char *ip);

//listens for ip change notifications from monitor
void *monitor_listen_ip_change(void *arg);

//notifies worker of an ip change
void worker_notify_ip_change(char *ip);

//wrapper for the uid logic
uid_node * get_uid_attributes(prg_node *node, int direction);

#endif

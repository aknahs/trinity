#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdarg.h>
//#include <linux/if.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "utils.h"

//Tun_client notification macros
#define TUN_CLIENT_PORT 55559
//Change tunnel route port
#define ROUTE_CONTROL_PORT 55666
#define ROUTE_CONTROL_SIZE 70
//Root runner
#define SERVICE_ROOT_PORT 55570
//maximum ip route command length
#define COMMAND_LEN 256

//Initial commands------------------------------------------------
#define CREATE_TUN "openvpn --mktun --dev tun11"
#define SET_TUN_UP "ip link set tun11 up"
//#define CREATE_VIRTUAL_NETWORK "ip addr add 10.0.1.1/24 dev tun11"
//----------------------------------------------------------------

//#define ADD_VIRTUAL_NETWORK "ip route add 10.0.1.0/24 src 10.0.1.1 proto kernel scope link dev tun11"
#define TUN_NAME "tun11"
#define REMOVE_DEFAULT "ip route del default"
//#define REMOVE_DEFAULT_TUN "ip route del default via 10.0.1.1 dev tun11"
//#define ADD_DEFAULT_TUN "ip route add default via 10.0.1.1 dev tun11"

#define ERR_RET(x) do { perror(x); return EXIT_FAILURE; } while (0);

//size for the netlink messages
#define BUFFER_SIZE 4095 //8192

struct route_info {
	struct in_addr dst_addr;
	struct in_addr src_addr;
	struct in_addr gateway;
	int if_index;
};

enum routing_return {
	ALL_OK, //All routes verified
	WRONG_SERVER, //wrong server route
	WRONG_DEFAULT, //not tun
	ITS_SUNNY_OUTSIDE, //wish i had swimwear now
	UNFINISHED, //nop
};

int route_listener(int sock, struct sockaddr_nl *addr);

int route_mask(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address);

void del_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out);

void new_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out);

void new_interface();

void del_interface();

void xrtmdeladdr();

void xrtmnewaddr();

void guess_rule(char *action, int mask, char * pref_address, char *source_address,
		char *destination_address, char *gateway_address);

void parse_addr_messages(struct nlmsghdr *nlh);

void parse_link_messages(struct nlmsghdr *nlh);

void parse_route_messages(struct nlmsghdr *nlh);

char *find_if_name(unsigned int index);

void implement_iptable_rules();

int iptable_change_listener();

void run_as_root(char *cmd);

void set_dns();

int parse_routes(struct nlmsghdr *nl_hdr, struct route_info *rtInfo);

int read_netlink_sockets(int sockFd, char *bufPtr, size_t buf_size, int seqNum, int pId);

void notify_change();

int is_interface_online(char *itface);

void *check_interface_state();

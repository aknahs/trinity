MY_DIR:= $(call my-dir)
LOCAL_PATH := $(MY_DIR)

#----------------------------------
#Build : utils lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := utils
LOCAL_SRC_FILES := utils.c

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)
#include $(BUILD_SHARED_LIBRARY)

#----------------------------------
#Build : packets_queue lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := packet_queue
LOCAL_SRC_FILES := packet_queue.c
LOCAL_STATIC_LIBRARIES := utils

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)
#include $(BUILD_SHARED_LIBRARY)

#----------------------------------
#Build : packet_actions lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := packet_actions
LOCAL_SRC_FILES := packet_actions.c
LOCAL_STATIC_LIBRARIES := utils packet_queue

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)
#include $(BUILD_SHARED_LIBRARY)

#----------------------------------
#Build : worker_pool lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := worker_pool
LOCAL_SRC_FILES := worker_pool.c
LOCAL_STATIC_LIBRARIES := utils

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)

#----------------------------------
#Build : tun_tools lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := tun_tools
LOCAL_SRC_FILES := tun_tools.c
LOCAL_STATIC_LIBRARIES := utils

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)

#----------------------------------
#Build : uid_tools lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := uid_tools
LOCAL_SRC_FILES := uid_tools.c
LOCAL_STATIC_LIBRARIES := utils

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)

#----------------------------------
#Build : rule_set lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := rule_set
LOCAL_SRC_FILES := rule_set.c
LOCAL_STATIC_LIBRARIES := utils

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)

#----------------------------------
#Build : build uid_monitor lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := uid_monitor
LOCAL_SRC_FILES := uid_monitor.c
LOCAL_STATIC_LIBRARIES := utils uid_tools rule_set

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)

#----------------------------------
#Build : db_connector lib

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := db_connector
LOCAL_SRC_FILES := db_connector.c
LOCAL_STATIC_LIBRARIES := uid_monitor

LOCAL_MODULE_TAGS := eng

include $(BUILD_STATIC_LIBRARY)

#----------------------------------
#Build : tun_client executable

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := tun_client
LOCAL_SRC_FILES := tun_client.c
LOCAL_STATIC_LIBRARIES := utils tun_tools uid_monitor uid_tools worker_pool db_connector rule_set packet_actions

LOCAL_MODULE_TAGS := eng

#include $(BUILD_SHARED_LIBRARY)
include $(BUILD_EXECUTABLE)

#----------------------------------
#Build : monitor_master executable

#LOCAL_PATH := $(MY_DIR)
#include $(CLEAR_VARS)

#LOCAL_SHARED_LIBRARIES := libcutils liblog
#LOCAL_MODULE    := monitor_master
#LOCAL_SRC_FILES := monitor_master.c
#LOCAL_STATIC_LIBRARIES := utils

#LOCAL_MODULE_TAGS := eng

#include $(BUILD_EXECUTABLE)

#----------------------------------
#Build : new_monitor executable

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := new_monitor
LOCAL_SRC_FILES := new_monitor.c
LOCAL_STATIC_LIBRARIES := utils

LOCAL_MODULE_TAGS := eng

include $(BUILD_EXECUTABLE)

#----------------------------------
#Build : cpu file <OPTIONAL>

LOCAL_PATH := $(MY_DIR)
include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_MODULE    := cpu_reg
LOCAL_SRC_FILES := cpu.c
LOCAL_STATIC_LIBRARIES := utils

LOCAL_MODULE_TAGS := eng

include $(BUILD_EXECUTABLE)
#include "monitor_master.h"
#include <pthread.h>

/*When an IP change is found, the tun_client should be notified*/
struct sockaddr_in tun_client_address;
int tun_client_sock_fd;

struct sockaddr_in service_root_address;
int service_root_sock_fd;

char *actual_gateway_address = NULL;
int actual_gateway_index = -1;
char *actual_interface_name = NULL;
char *actual_interface_ip = NULL;

char tunnel_server_ip[34]; //TODO : remove hardcoded value

char log_version[] = "1.0";

int timer = 0;
int is_running = 0;
pthread_t thr;
int initialized_flag = 0;

void notify_change(char *itface) {
	do_debug(LOG_LEVEL_LOW, "Notifying tun_client of interface change! %s\n",
			itface);
	char buf[strlen(itface) + 3 + 1 + 1];

	memset(buf, 0, strlen(itface) + 3 + 1 + 1);
	sprintf(buf, "%3d|%s", PROTO_IPCHANGE, itface);

	if (sendto(tun_client_sock_fd, buf, strlen(buf), 0, &tun_client_address,
			sizeof(tun_client_address)) == -1)
		perror("sendto()");
}

void run_as_root(char *cmd) {
	do_debug(LOG_LEVEL_LOW, "Running command as root! %s\n", cmd);
	int len = strlen(cmd);
	int msg_size = 3 + 1 + len + 1; //Assumes max of strlen of 3 digits
	char msg[msg_size];

	memset(msg, 0, msg_size);
	sprintf(msg, "%3d|%s", len, cmd); //forces 3 digit representation of len

	if (sendto(service_root_sock_fd, msg, strlen(msg), 0, &service_root_address,
			sizeof(service_root_address)) == -1)
		perror("sendto()");
}

int readNlSock(int sockFd, char *bufPtr, size_t buf_size, int seqNum, int pId) {
	struct nlmsghdr *nl_hdr;
	int readLen = 0, msgLen = 0;

	do {
		/* Recieve response from the kernel */
		if ((readLen = recv(sockFd, bufPtr, buf_size - msgLen, 0)) < 0)
			ERR_RET("SOCK READ: ");

		nl_hdr = (struct nlmsghdr *) bufPtr;

		/* Check if the header is valid */
		if ((NLMSG_OK(nl_hdr, readLen) == 0)
				|| (nl_hdr->nlmsg_type == NLMSG_ERROR))
			ERR_RET("Error in recieved packet");

		/* Check if the its the last message */
		if (nl_hdr->nlmsg_type == NLMSG_DONE) {
			break;
		} else {
			/* Else move the pointer to buffer appropriately */
			bufPtr += readLen;
			msgLen += readLen;
		}

		/* Check if its a multi part message */
		if ((nl_hdr->nlmsg_flags & NLM_F_MULTI) == 0) {
			/* return if its not */
			break;
		}
	} while ((nl_hdr->nlmsg_seq != seqNum) || (nl_hdr->nlmsg_pid != pId));

	return msgLen;
}

/* parse the route info returned */
int parseRoutes(struct nlmsghdr *nl_hdr, struct route_info *rtInfo) {
	struct rtmsg *rt_msg;
	struct rtattr *rt_attr;
	int rt_len;

	rt_msg = (struct rtmsg *) NLMSG_DATA(nl_hdr);

	/* If the route is not for AF_INET or does not belong to main routing table then return. */
	if ((rt_msg->rtm_family != AF_INET) || (rt_msg->rtm_table != RT_TABLE_MAIN))
		return -1;

	/* get the rtattr field */
	rt_attr = (struct rtattr *) RTM_RTA(rt_msg);
	rt_len = RTM_PAYLOAD(nl_hdr);

	for (; RTA_OK(rt_attr, rt_len); rt_attr = RTA_NEXT(rt_attr, rt_len)) {
		switch (rt_attr->rta_type) {
		case RTA_OIF:
			memcpy(&rtInfo->if_index, RTA_DATA(rt_attr),
					sizeof(rtInfo->if_index));
			break;

		case RTA_GATEWAY:
			memcpy(&rtInfo->gateway, RTA_DATA(rt_attr),
					sizeof(rtInfo->gateway));
			break;

		case RTA_PREFSRC:
			memcpy(&rtInfo->src_addr, RTA_DATA(rt_attr),
					sizeof(rtInfo->src_addr));
			break;

		case RTA_DST:
			memcpy(&rtInfo->dst_addr, RTA_DATA(rt_attr),
					sizeof(rtInfo->dst_addr));
			break;
		}
	}
	return 0;
}

//Routing table should be cleaned before running this! Otherwise it might get tun11 as the iface
int get_gateway_info(int reset, char *prefsource, char* destination,
		char* gateway, int index) {
	if (reset)
		return UNFINISHED;
	//if (strcmp(gateway, "10.0.1.1") == 0)
	//	return UNFINISHED;
	// Check if default gateway
	if (strcmp(destination, "0.0.0.0") == 0
			&& strcmp(prefsource, "0.0.0.0") == 0) {
		actual_gateway_index = index;
		do_debug(LOG_LEVEL_LOW, "FOUND : actual_gateway_index %d\n",
				actual_gateway_index);
		if (actual_gateway_address == NULL)
			actual_gateway_address = (char *) malloc(33 + 1);
		strcpy(actual_gateway_address, gateway);
		do_debug(LOG_LEVEL_LOW, "FOUND : actual_gateway_address %s\n",
				actual_gateway_address);
	} else if (strcmp(prefsource, "0.0.0.0") != 0) {
		if (actual_interface_ip == NULL)
			actual_interface_ip = (char *) malloc(33 + 1);
		strcpy(actual_interface_ip, prefsource);
		do_debug(LOG_LEVEL_LOW, "FOUND : actual_interface_ip %s\n",
				actual_interface_ip);
	}

	return UNFINISHED;
}

int confirm_routing_rules(int reset, char *prefsource, char* destination,
		char* gateway, int index) {
	//This should be a mask but i shall keep it like this for simplicity of reading
	static int has_vn = 0;
	static int has_server = 0;
	static int has_default_tun = 0;

	if (reset) {
		do_debug(LOG_LEVEL_LOW, "confirm_routing_rules: initializing!\n");
		has_default_tun = has_server = has_vn = 0;
		return UNFINISHED;
	}

	if (strcmp(destination, tunnel_server_ip) == 0) {
		if (strcmp(actual_interface_name, "tun11") == 0) {
			do_debug(LOG_LEVEL_LOW,
					"confirm_routing_rules: wrong server definition!\n");
			return WRONG_SERVER;
		}
		do_debug(LOG_LEVEL_LOW, "confirm_routing_rules: confirmed server!\n");
		has_server = 1;
	} else {
		if (strcmp(destination, "10.0.1.0") == 0) {
			do_debug(LOG_LEVEL_LOW,
					"confirm_routing_rules: confirmed 10.0.1.0/24!\n");
			has_vn = 1; //this rule is somewhat static so no need to check anything else
		} else {
			if (strcmp(prefsource, "0.0.0.0") == 0
					&& strcmp(destination, "0.0.0.0") == 0) {
				if (strcmp(gateway, "10.0.1.1") == 0) {
					do_debug(LOG_LEVEL_LOW,
							"confirm_routing_rules: default tun!\n");
					has_default_tun = 1;
				} else {
					do_debug(LOG_LEVEL_LOW,
							"confirm_routing_rules: wrong default!\n");
					return WRONG_DEFAULT;
				}
			}
		}
	}

	if (has_default_tun && has_server && has_vn) {
		do_debug(LOG_LEVEL_LOW,
				"confirm_routing_rules: all confirmed! continuing..\n");
		return ALL_OK;
	} else {
		do_debug(LOG_LEVEL_LOW,
				"confirm_routing_rules: shall continue checking!\n");
		return UNFINISHED;
	}
}

int get_info(int (*f)(int, char*, char*, char*, int)) {
	struct nlmsghdr *nl_msg;
	struct rtmsg *rt_msg;
	struct route_info route_info;
	char msgBuf[BUFFER_SIZE]; // pretty large buffer
	char prefsource[33 + 1];
	char destination[33 + 1];
	char gateway[33 + 1];

	int ret;

	int sock, len, msgSeq = 0;

	/* Create Socket */
	if ((sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE)) < 0)
		perror("Socket Creation: ");

	/* Initialize the buffer */
	memset(msgBuf, 0, sizeof(msgBuf));

	/* point the header and the msg structure pointers into the buffer */
	nl_msg = (struct nlmsghdr *) msgBuf;
	rt_msg = (struct rtmsg *) NLMSG_DATA(nl_msg);

	/* Fill in the nlmsg header*/
	nl_msg->nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg)); // Length of message.
	nl_msg->nlmsg_type = RTM_GETROUTE; // Get the routes from kernel routing table .

	nl_msg->nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST; // The message is a request for dump.
	nl_msg->nlmsg_seq = msgSeq++; // Sequence of the message packet.
	nl_msg->nlmsg_pid = getpid(); // PID of process sending the request.

	/* Send the request */
	if (send(sock, nl_msg, nl_msg->nlmsg_len, 0) < 0) {
		fprintf(stderr, "Write To Socket Failed...\n");
		return;
	}

	/* Read the response */
	if ((len = readNlSock(sock, msgBuf, sizeof(msgBuf), msgSeq, getpid()))
			< 0) {
		fprintf(stderr, "Read From Socket Failed...\n");
		return;
	}

	(*f)(1, NULL, NULL, NULL, -1); //reset counters (if any)

	/* Parse and print the response */
	for (; NLMSG_OK(nl_msg, len); nl_msg = NLMSG_NEXT(nl_msg, len)) {
		memset(&route_info, 0, sizeof(route_info));
		if (parseRoutes(nl_msg, &route_info) < 0)
			continue; // don't check route_info if it has not been set up

		memset(prefsource, 0, 33 + 1);
		memset(destination, 0, 33 + 1);
		memset(gateway, 0, 33 + 1);
		strcpy(prefsource, (char *) inet_ntoa(route_info.src_addr));
		strcpy(destination, (char *) inet_ntoa(route_info.dst_addr));
		strcpy(gateway, (char *) inet_ntoa(route_info.gateway));

		do_debug(LOG_LEVEL_LOW,
				"FOUND: [prefsource %s destination %s gateway %s if_out %d]\n",
				prefsource, destination, gateway, route_info.if_index);

		ret = (*f)(0, prefsource, destination, gateway, route_info.if_index);

		if (ret != UNFINISHED && ret != ALL_OK) {
			do_debug(LOG_LEVEL_LOW, "get_info : BREAK!\n");
			break;
		}
	}

	close(sock);
	return ret;
}

//ioctl is a synchronous run_as_root call.
void find_if_name(unsigned int index) {
	int fd;
	if (actual_interface_name == NULL)
		actual_interface_name = (char *) malloc(IFNAMSIZ + 1);
	memset(actual_interface_name, 0, IFNAMSIZ + 1);
	//interface still not known
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	struct ifreq ifr;
	ifr.ifr_ifindex = index;
	ioctl(fd, SIOCGIFNAME, (void*) &ifr);
	memcpy(actual_interface_name, ifr.ifr_name, IFNAMSIZ);
	do_debug(LOG_LEVEL_LOW, "if name : %s\n", actual_interface_name);
	close(fd);
}

//ioctl is a synchronous run_as_root call. (not used!)
char * find_if_ip(unsigned int index) {
	int fd;
	char *ip = (char *) malloc(33 + 1);
	memset(ip, 0, 33 + 1);
	//interface still not known
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	struct ifreq ifr;
	ifr.ifr_ifindex = index;
	ioctl(fd, SIOCGIFADDR, (void*) &ifr);
	strcpy(ip, inet_ntoa(((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr));
	do_debug(LOG_LEVEL_LOW, "my ip : %s\n", ip);
	close(fd);
	return ip;
}

void set_dns() {
	//Set dns to google dns
	do_debug(LOG_LEVEL_LOW, "set dns\n");
	run_as_root("setprop net.rmnet0.dns1 8.8.8.8");
	run_as_root("setprop net.rmnet0.dns2 8.8.4.4");

	run_as_root("setprop net.dns1 8.8.8.8");
	run_as_root("setprop net.dns2 8.8.4.4");

	run_as_root("setprop net.pdp0.dns1 8.8.8.8");
	run_as_root("setprop net.pdp0.dns2 8.8.4.4");

	run_as_root("setprop dhcp.eth0.dns1 8.8.8.8");
	run_as_root("setprop dhcp.eth0.dns2 8.8.4.4");

	//TODO : DISABLE APN PROXY
}

/*
 # 3G with monitor_master
 10.80.48.1 dev pdp0
 10.80.48.0/24 dev pdp0  src 10.80.48.165
 10.0.1.0/24 dev tun11  src 10.0.1.1 xxxxxxxxxxx
 176.34.202.100 via 10.80.48.1 dev pdp0  src 10.80.48.165
 default via 10.0.1.1 dev tun11
 */

/* 3G without monitor_master
 10.127.169.1 dev pdp0
 10.127.169.0/24 dev pdp0  src 10.127.169.208
 default via 10.127.169.1 dev pdp0
 */

/* wifi with monitor_master
 176.34.202.100 via 192.168.0.1 dev eth0  src 192.168.1.52
 10.0.1.0/24 dev tun11  src 10.0.1.1
 192.168.0.0/22 dev eth0  src 192.168.1.52
 default via 10.0.1.1 dev tun11
 */

void set_virtual_network() {
	//do_debug(LOG_LEVEL_LOW,"remove VN : %s\n", "ip route del 10.0.1.0/24");
	//run_as_root("ip route del 10.0.1.0/24");
	do_debug(LOG_LEVEL_LOW, "add VN : %s\n",
			"ip route add 10.0.1.0/24 dev tun11  via 10.0.1.1");
	//run_as_root("ip route add 10.0.1.0/24 dev tun11  via 10.0.1.1");
	//run_as_root(ADD_TUN_ADDR);
}

void set_server_route(char *server, char *src, char *gateway, char *iface) {
	char command[COMMAND_LEN];
	char *cmd = command;
	int pos = 0;

	memset(command, 0, COMMAND_LEN);
	sprintf(command, "ip route add %s", tunnel_server_ip);
	//13+strlen(SERVER)
	pos += 13 + strlen(tunnel_server_ip);

	if (src != NULL && strcmp(src, "") != 0) {
		sprintf(cmd + pos, " src %s", src);
		pos += 5 + strlen(src);
	}

	if (gateway != NULL && strcmp(gateway, "") != 0) {
		sprintf(cmd + pos, " via %s", gateway);
		pos += 5 + strlen(gateway);
	}

	sprintf(cmd + pos, " dev %s", iface);

	do_debug(LOG_LEVEL_LOW, "set_server_route : guess %s\n", command);
	run_as_root(command);
}

void set_default_route() {
	//do_debug(LOG_LEVEL_LOW, "del default route : ip route del default\n");
	run_as_root("ip route del default"); //Strangely often cm10 defined default more than once -_-'
	run_as_root("ip route del default");
	run_as_root("ip route del default");
	//do_debug(LOG_LEVEL_LOW, "set tun default : %s\n", TUN_DEFAULT);
	run_as_root(TUN_DEFAULT);
}

void client_notify_init() {
	int slen = sizeof(tun_client_address);
	if ((tun_client_sock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf("client_notify_init: socket creation failed");
		exit(1);
	}
	memset((char *) &tun_client_address, 0, sizeof(tun_client_address));
	tun_client_address.sin_family = AF_INET;
	tun_client_address.sin_port = htons(TUN_CLIENT_PORT);
	tun_client_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
}

void service_root_init() {
	int slen = sizeof(service_root_address);
	if ((service_root_sock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
			== -1) {
		printf("service_root_init: socket creation failed");
		exit(1);
	}
	memset((char *) &service_root_address, 0, sizeof(service_root_address));
	service_root_address.sin_family = AF_INET;
	service_root_address.sin_port = htons(SERVICE_ROOT_PORT);
	service_root_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
}

void set_routes(char *server, char *src, char *gateway, char *iface) {
	sleep(1);
	//Sets default as the tun tunnel
	set_default_route();
	//Sets virtual local network
	set_virtual_network();
	//Sets the path to the worker
	set_server_route(server, src, gateway, iface);
	//Sets dns to google dns
	set_dns();

	do_debug(LOG_LEVEL_LOW, "Done setting routes!\n");
}

void route_init() {
	int ret;

	clean_rules();
	sleep(1);

	free(actual_interface_name);
	actual_interface_name = NULL;
	free(actual_interface_ip);
	actual_interface_ip = NULL;
	free(actual_gateway_address);
	actual_gateway_address = NULL;

	//Check if it is pdp0 or eth0 the default root
	get_info(get_gateway_info); //sets actual_gateway index and address
	find_if_name(actual_gateway_index); //sets actual_interface_name
	//char * iface_ip = find_if_ip(actual_gateway_index);

	//do_debug(LOG_LEVEL_LOW, "create tun device\n");
	run_as_root(CREATE_TUN);

	do_debug(LOG_LEVEL_LOW, "set tun up\n");
	run_as_root("ip link set tun11 up");

	do_debug(LOG_LEVEL_LOW, "add local addr\n");
	run_as_root("ip addr add 10.0.1.1/24 dev tun11");

	set_routes(tunnel_server_ip, actual_interface_ip, actual_gateway_address,
			actual_interface_name);

	sleep(1);

	ret = get_info(confirm_routing_rules);
	if (ret != ALL_OK) {
		do_debug(LOG_LEVEL_LOW, "Recurse route_init()\n");
		route_init();
	}

	notify_change();
	do_debug(LOG_LEVEL_LOW, "Routing rules confirmed!\n");
	do_debug(LOG_LEVEL_LOW, "####################\n");
}

void new_interface(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out) {
	/*do_debug(
	 "NEW INTERFACE: [prefsource %s source %s destination %s gateway %s if_in %d if_out %d]\n",
	 pref_address, source_address, destination_address, gateway_address,
	 iface_in, iface_out);
	 //do_debug(LOG_LEVEL_LOW,"####################\n");*/
}

void del_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out) {
	/*
	static char last_src[32 + 1];
	static char last_gateway[33];
	char if_name_out[33];
	static int last_int = -1;

	memset(last_src, 0, 33);
	memset(last_gateway, 0, 33);

	do_debug(LOG_LEVEL_LOW,
			"DEL ROUTE: [prefsource %s source %s destination %s gateway %s if_in %d if_out %d]\n",
			pref_address, source_address, destination_address, gateway_address,
			iface_in, iface_out);

	int mask = route_mask(pref_address, source_address, destination_address,
			gateway_address);
	do_debug(LOG_LEVEL_LOW, "New route mask : %d\n", mask);

	//Its a tun routing rule
	if (strcmp(gateway_address, TUN_IP) == 0
			|| strcmp(destination_address, tunnel_server_ip) == 0) {
		do_debug(LOG_LEVEL_LOW, "DEL : Guess : Ignore tun/server!\n");
		return;
	}

	switch (mask) {
	case (1): //0001 - Default route
		do_debug(LOG_LEVEL_LOW, "Guess : ip route del default via %s dev %s\n",
				gateway_address, actual_interface_name);

		break;
	case (2): //0010 - Map address (e.g Server rule)
		do_debug(LOG_LEVEL_LOW, "Guess : ip route del %s dev %s\n",
				destination_address, actual_interface_name);

		if (strcmp(destination_address, tunnel_server_ip) != 0) {
			do_debug(LOG_LEVEL_LOW, "CLEANING ROUTES!!!");
			clean_rules();
		}

		break;
	case (3): //0011 - Complete route
		do_debug(LOG_LEVEL_LOW, "Guess : ip route del %s via %s dev %s\n",
				destination_address, gateway_address, actual_interface_name);
		if (strcmp(destination_address, tunnel_server_ip) != 0) {
			do_debug(LOG_LEVEL_LOW, "CLEANING ROUTES!!!");
			clean_rules();
		}

		break;
	case (10): //1010 - Set pref_source!
		do_debug(LOG_LEVEL_LOW, "Guess : ip route del %s/24 src %s dev %s",
				destination_address, pref_address, actual_interface_name);

		break;
	case (11): //1011
		do_debug(LOG_LEVEL_LOW, "Guess : ip route del %s src %s via %s dev %s",
				destination_address, pref_address, gateway_address,
				actual_interface_name);
		if (strcmp(destination_address, tunnel_server_ip) != 0) {
			do_debug(LOG_LEVEL_LOW, "CLEANING ROUTES!!!");
			clean_rules();
		}

		break;
	default:
		do_debug(LOG_LEVEL_LOW, "Guess : Ignore!\n");
		break;
	}
	*/
}

int route_mask(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address) {
	int mask = 0;
	if (strcmp(pref_address, "") != 0)
		mask = mask | 8;
	if (strcmp(source_address, "") != 0)
		mask = mask | 4;
	if (strcmp(destination_address, "") != 0)
		mask = mask | 2;
	if (strcmp(gateway_address, "") != 0)
		mask = mask | 1;

	return mask;
}

void *apply_route(void *arg) {
	is_running = 1;
	do_debug(LOG_LEVEL_LOW, "apply_route: Scheduling routing changes!\n");
	while (timer > 0) {
		do_debug(LOG_LEVEL_LOW, "apply_route: timer %d!\n", timer);
		sleep(1);
		timer--;
	}
	do_debug(LOG_LEVEL_LOW, "apply_route: Setting rules!\n");
	route_init();

	is_running = 0;
	pthread_exit(NULL);
}

//TODO : maybe re-implement the epoch theorem here
void new_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out) {

	static char last_src[32 + 1];
	static char last_gateway[33];
	char if_name_out[33];
	static int last_int = -1;

	memset(last_src, 0, 33);
	memset(last_gateway, 0, 33);

	do_debug(LOG_LEVEL_LOW,
			"NEW ROUTE: [prefsource %s source %s destination %s gateway %s if_in %d if_out %d]\n",
			pref_address, source_address, destination_address, gateway_address,
			iface_in, iface_out);

	int mask = route_mask(pref_address, source_address, destination_address,
			gateway_address);
	do_debug(LOG_LEVEL_LOW, "New route mask : %d\n", mask);

	//Its a tun routing rule
	if (strcmp(gateway_address, TUN_IP) == 0
			|| strcmp(destination_address, tunnel_server_ip) == 0) {
		do_debug(LOG_LEVEL_LOW, "Guess : Ignore tun/server!\n");
		return;
	}

	//Getting if name
	if (iface_out != last_int) {
		strcpy(if_name_out, actual_interface_name);
		find_if_name(iface_out);

		//memset(last_gateway, 0, 33);
		//memset(last_src, 0, 33);

		if (strcmp(actual_interface_name, "tun11") == 0) {
			strcpy(actual_interface_name, if_name_out);
			do_debug(LOG_LEVEL_LOW, "Guess : Ignore tun!\n");
			return; //Its a tun routing rule
		}
		//else
		//	do_debug(LOG_LEVEL_LOW,"New : Storing if_name %s\n", if_name_out);

		timer = 5;
	} else
		timer = 3;

	switch (mask) {
	case (1): //0001 - Default route
		do_debug(LOG_LEVEL_LOW, "Guess : ip route add default via %s dev %s\n",
				gateway_address, actual_interface_name);
		//do_debug(LOG_LEVEL_LOW,"Guess : Should redefine default!\n");
		//set_default_route();
		break;
	case (2): //0010 - Map address (e.g Server rule)
		do_debug(LOG_LEVEL_LOW, "Guess : ip route add %s dev %s\n",
				destination_address, actual_interface_name);
		//do_debug(LOG_LEVEL_LOW,"Guess : Should save gateway address (destination)\n");
		//memset(last_gateway, 0, 33);
		//strcpy(last_gateway, destination_address);
		//do_debug(LOG_LEVEL_LOW,"Guess : Ignore!\n");
		break;
	case (3): //0011 - Complete route
		do_debug(LOG_LEVEL_LOW, "Guess : ip route add %s via %s dev %s\n",
				destination_address, gateway_address, actual_interface_name);
		//do_debug(LOG_LEVEL_LOW,"Guess : Should save gateway address (gateway)\n");
		//memset(last_gateway, 0, 33);
		//strcpy(last_gateway, gateway_address);
		//set_routes(SERVER, last_src, gateway_address, actual_interface_name);
		break;
	case (10): //1010 - Set pref_source!
		do_debug(LOG_LEVEL_LOW, "Guess : ip route add %s/22 src %s dev %s",
				destination_address, pref_address, actual_interface_name);
		//do_debug(LOG_LEVEL_LOW,"Guess : Should save src (pref_address)\n");
		//memset(last_src, 0, 33);
		//strcpy(last_src, pref_address);
		//set_routes(SERVER, pref_address, last_gateway, actual_interface_name);
		break;
	case (11): //1011
		do_debug(LOG_LEVEL_LOW, "Guess : ip route add %s src %s via %s dev %s",
				destination_address, pref_address, gateway_address,
				actual_interface_name);
		//memset(last_gateway, 0, 33);
		//strcpy(last_gateway, gateway_address);
		//set_routes(SERVER, pref_address, gateway_address,actual_interface_name);
		break;
	default:
		do_debug(LOG_LEVEL_LOW, "Guess : Ignore!\n");
		break;
	}

	//Schedule the routing policies
	if (!is_running)
		pthread_create(&thr, NULL, apply_route, NULL);

	last_int = iface_out;
}

/*
 Attributes
 rta_type        value type         description
 --------------------------------------------------------------
 RTA_UNSPEC      -                  ignored.
 ->RTA_DST         protocol address   Route destination address.
 ->RTA_SRC         protocol address   Route source address.
 ->RTA_IIF         int                Input interface index.
 ->RTA_OIF         int                Output interface index.
 ->RTA_GATEWAY     protocol address   The gateway of the route
 RTA_PRIORITY    int                Priority of route.
 RTA_PREFSRC
 RTA_METRICS     int                Route metric
 RTA_MULTIPATH
 RTA_PROTOINFO
 RTA_FLOW
 RTA_CACHEINFO
 */

int route_listener(int sock, struct sockaddr_nl *addr) {
	int received_bytes = 0;
	struct nlmsghdr *nlh;
	char destination_address[32] = "";
	char source_address[32] = "";
	char gateway_address[32] = "";
	char pref_address[32] = "";
	int iface_in = -1, iface_out = -1;
	struct rtmsg *route_entry; /* This struct represent a route entry \
                                    in the routing table */
	struct rtattr *route_attribute; /* This struct contain route \
                                            attributes (route type) */
	int route_attribute_len = 0;
	char buffer[BUFFER_SIZE];

	bzero(destination_address, sizeof(destination_address));
	bzero(gateway_address, sizeof(gateway_address));
	bzero(buffer, sizeof(buffer));

	/* Receiving netlink socket data */
	while (1) {
		received_bytes = recv(sock, buffer, sizeof(buffer), 0);
		if (received_bytes < 0)
			ERR_RET("recv");
		/* cast the received buffer */
		nlh = (struct nlmsghdr *) buffer;
		/* If we received all data ---> break */
		if (nlh->nlmsg_type == NLMSG_DONE)
			break;
		/* We are just intrested in Routing information */
		if (addr->nl_groups == RTMGRP_IPV4_ROUTE)
			break;
	}

	/* Reading netlink socket data */
	/* loop through all entries */
	/* For more informations on some functions :
	 * http://www.kernel.org/doc/man-pages/online/pages/man3/netlink.3.html
	 * http://www.kernel.org/doc/man-pages/online/pages/man7/rtnetlink.7.html
	 */

	//about macros http://qos.ittc.ku.edu/netlink/html/node11.html
	for (; NLMSG_OK(nlh, received_bytes);
			nlh = NLMSG_NEXT(nlh, received_bytes)) {
		/* Get the route data */
		route_entry = (struct rtmsg *) NLMSG_DATA(nlh);

		/* We are just interested in main routing table */
		if (route_entry->rtm_table != RT_TABLE_MAIN)
			continue;

		/* Get attributes of route_entry */
		route_attribute = (struct rtattr *) RTM_RTA(route_entry);

		/* Get the route atttibutes len */
		route_attribute_len = RTM_PAYLOAD(nlh);
		/* route_listener through all attributes */
		for (; RTA_OK(route_attribute, route_attribute_len); route_attribute =
				RTA_NEXT(route_attribute, route_attribute_len)) {
			/* Get the destination address */
			if (route_attribute->rta_type == RTA_DST) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute),
						destination_address, sizeof(destination_address));
			}
			/* Get the source address */
			if (route_attribute->rta_type == RTA_SRC) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute), source_address,
						sizeof(destination_address));
			}
			/* Get the gateway (Next hop) */
			if (route_attribute->rta_type == RTA_GATEWAY) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute), gateway_address,
						sizeof(gateway_address));
			}
			/* Get the interface in */
			if (route_attribute->rta_type == RTA_IIF) {
				memcpy(&iface_in, RTA_DATA(route_attribute), sizeof(int));
			}

			/* Get the interface out */
			if (route_attribute->rta_type == RTA_OIF) {
				memcpy(&iface_out, RTA_DATA(route_attribute), sizeof(int));
			}

			if (route_attribute->rta_type == RTA_PREFSRC) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute), pref_address,
						sizeof(pref_address));
			}
		}

		/* Now we can dump the routing attributes */
		if (nlh->nlmsg_type == RTM_DELROUTE) {
			del_route(pref_address, source_address, destination_address,
					gateway_address, iface_in, iface_out);
		}
		if (nlh->nlmsg_type == RTM_NEWROUTE) {
			new_route(pref_address, source_address, destination_address,
					gateway_address, iface_in, iface_out);
		}
		if (nlh->nlmsg_type == RTM_NEWLINK) {
			new_interface(pref_address, source_address, destination_address,
					gateway_address, iface_in, iface_out);
		}
	}
	return 0;
}

/*
 * default via 10.0.1.1 dev tun11
 10.0.1.0/24 via 10.0.1.1 dev tun11
 176.34.202.100 via 192.168.0.1 dev wlan0  src 192.168.1.174
 192.168.0.0/22 dev wlan0  proto kernel  scope link  src 192.168.1.174  metric 309
 192.168.0.1 dev wlan0  scope link
 *
 */
void clean_rules() {

	char command[COMMAND_LEN];
	memset(command, 0, COMMAND_LEN);
	sprintf(command, "ip route del %s", tunnel_server_ip);
	do_debug(LOG_LEVEL_LOW, "remove server\n");
	run_as_root(command);

	do_debug(LOG_LEVEL_LOW, "remove default\n");
	//run_as_root("ip route del 176.34.202.100");
	run_as_root("ip route del 10.0.1.0/24");
	run_as_root("ip route del dev tun11");
}

void termination_handler(int signum) {
	char command[COMMAND_LEN];
	do_debug(LOG_LEVEL_LOW, "*Le* Quiting like a sir!\n");
	clean_rules();

	if (actual_interface_name != NULL) {
		memset(command, 0, COMMAND_LEN);
		do_debug(LOG_LEVEL_LOW, "adding default\n");
		if (actual_gateway_address != NULL)
			sprintf(command, "ip route add default via %s dev %s",
					actual_gateway_address, actual_interface_name);
		else
			sprintf(command, "ip route add default dev %s",
					actual_interface_name);
		run_as_root(command);
	}

	exit(0);
}

//Listens to tunnel destination changes
void *event_listener() {
	struct sockaddr_in si_me, si_other;
	int s, i, slen = sizeof(si_other);
	int nbytes = 0, ret;
	char buf[ROUTE_CONTROL_SIZE];
	char interface[IFNAMSIZ + 1];
	char *buf_ptr;

	int protocol;

	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		perror("event_listener Error: socket");
	}

	bzero((char *) &si_me, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(ROUTE_CONTROL_PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	do_debug(LOG_LEVEL_LOW,
			"--->event_listener : Listening for route control messages on port %d\n",
			ROUTE_CONTROL_PORT);

	if (bind(s, (struct sockaddr *) &si_me, sizeof(si_me)) == -1)
		perror("event_listener Error: bind");

	while (1) {
		fd_set rd_set;

		FD_ZERO(&rd_set);
		FD_SET(s, &rd_set);

		//do_debug(LOG_LEVEL_LOW,"tun_client : Waiting for traffic (select)\n");
		ret = select(s + 1, &rd_set, NULL, NULL, NULL);

		if (ret < 0 && errno == EINTR) {
			continue;
		}

		if (ret < 0) {
			perror("select()");
			exit(1);
		}

		memset(buf, 0, ROUTE_CONTROL_SIZE);

		if ((nbytes = recvfrom(s, buf, ROUTE_CONTROL_SIZE, 0,
				(struct sockaddr *) &si_other, &slen)) == -1)
			perror("event_listener Error: recvfrom()");

		strcpy(tunnel_server_ip, buf);
		do_debug(LOG_LEVEL_LOW, "Registered tunnel destination ip %s\n",
				tunnel_server_ip);

		initialized_flag = 1;
	}
	close(s);
}

int main(int argc, char **argv) {
	int sock = -1;
	struct sockaddr_nl addr;
	pthread_t route_tr;

	do_debug(LOG_LEVEL_LOW,"Monitor_master - Version %s\n",log_version);

	sleep(BOOT_SLEEP);
	do_debug(LOG_LEVEL_LOW, "Preparing device\n");

	if (pthread_create(&route_tr, NULL, event_listener, NULL) != 0)
		perror("error: pthread");

	while (!initialized_flag) {
		//I know this should be a conditional wait but this only happens for the
		//first execution while waiting for the client to start
		do_debug(LOG_LEVEL_LOW, "Waiting for worker ip address\n");
		sleep(1);
	}

	client_notify_init();
	service_root_init();

	route_init();

	if (signal(SIGINT, termination_handler) == SIG_IGN)
		signal(SIGINT, SIG_IGN);
	if (signal(SIGHUP, termination_handler) == SIG_IGN)
		signal(SIGHUP, SIG_IGN);
	if (signal(SIGTERM, termination_handler) == SIG_IGN)
		signal(SIGTERM, SIG_IGN);

	/* Zeroing addr */
	bzero(&addr, sizeof(addr));

	if ((sock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0)
		ERR_RET("socket");

	addr.nl_family = AF_NETLINK;
	addr.nl_groups = RTMGRP_IPV4_ROUTE;

	if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0)
		ERR_RET("bind");

	while (1) {
		route_listener(sock, &addr);
	}

	/* Close sockets */
	close(sock);
	close(tun_client_sock_fd);
	//destroy();

	return 0;
}

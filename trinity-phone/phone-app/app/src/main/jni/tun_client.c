#include "tun_client.h"

int register_counter = 0;
//Connection caching stats
int stats_con_cached = 0, stats_con_not_cached = 0;
//Uid caching stats
int stats_uid_cached = 0, stats_uid_not_cached = 0;
//Unparsable packets
int stats_broken_packets = 0;
//Screen state
int stats_screen_state = 1;
//Connection type state
int stats_con_type = STATS_CON_TYPE_MOBILE;

/*IP(16)+PORT(5)+ID(6)*/
void process_worker(char *buf) {
	int len = strlen(buf), i;

	//processing 1|176.34.202.100|55556|55560|3|0 (len 30)
	do_debug(LOG_LEVEL_LOW, "process_worker : processing %s (len %d)\n", buf,
			len);

	for (i = 0; i < len; i++) {
		if (buf[i] == '|')
			buf[i] = '\0';
	}

	//wk->id, wk->ip, wk->traffic_port, wk->control_port, 0);
	registered_worker->id = atoi(buf);
	len = strlen(buf) + 1;

	do_debug(LOG_LEVEL_LOW, "process_worker : worker id = %d\n",
			registered_worker->id);

	//get ip
	do_debug(LOG_LEVEL_LOW, "process_worker : processing ip %s\n", buf + len);
	strcpy(registered_worker->ip, buf + len);
	len += strlen(buf + len) + 1;

	do_debug(LOG_LEVEL_LOW, "process_worker : worker ip = %s\n",
			registered_worker->ip);

	//get traffic port
	registered_worker->traffic_port = atoi(buf + len);
	len += strlen(buf + len) + 1;

	do_debug(LOG_LEVEL_LOW, "process_worker : worker traffic port = %d\n",
			registered_worker->traffic_port);

	registered_worker->control_port = atoi(buf + len);
	len += strlen(buf + len) + 1;

	do_debug(LOG_LEVEL_LOW, "process_worker : worker control port = %d\n",
			registered_worker->control_port);

	registered_worker->ctip = atoi(buf+len);
	len += strlen(buf+len)+1;

	do_debug(LOG_LEVEL_LOW, "process_worker : Client tun IP = %d\n",
			registered_worker->ctip);

	//this is not received since its internal to the worker
	registered_worker->register_port = 0;
}

worker *scheduler_notify_register_client(char *ip) {
	char buf[SCHEDULER_REGISTER_MSG_LEN] = "";

	if (registered_worker == NULL) {
		registered_worker = (worker *) malloc(sizeof(worker));
		registered_worker->ip = (char *) malloc(sizeof(34));
	}

	do_debug(LOG_LEVEL_LOW,
			"scheduler_notify_register_client : Registering in scheduler %s\n",
			ip);

	//scheduler communication here
	int sch_socket = create_tcp_client_socket(ip, SCHEDULER_REGISTER_PORT);

	send_all_fixed_size(sch_socket, "8=>", SCHEDULER_REGISTER_MSG_LEN);

	receive_all_fixed_size(sch_socket, buf, SCHEDULER_REGISTER_MSG_LEN);

	do_debug(LOG_LEVEL_LOW, "scheduler_notify_register_client : Registered!\n",
			buf);
	process_worker(buf);

	close(sch_socket);

	worker_notify_ip_change("dunno"); //TODO: Remove this string

	return registered_worker;
}

void worker_notify_ip_change(char *ip) {
	int i, sch_socket;

	do_debug(LOG_LEVEL_LOW,
			"--->worker_notify_ip_change : Will register on worker {%s} and port %d\n",
			registered_worker->ip, registered_worker->control_port);

	//Not really necessary to retry. If there is no connection, monitor will notify once it exists again
	for (i = 0; i < 3; i++) {
		//worker communication here
		sch_socket = create_tcp_client_socket(registered_worker->ip,
				registered_worker->control_port);

		if (sch_socket < 0)
			continue;
		else
			break;
	}

	do_debug(LOG_LEVEL_LOW,
			"--->worker_notify_ip_change : Sending notify IP change - %s\n",
			ip);

	send_proto_msg(sch_socket, PROTO_IPCHANGE, ip);

	close(sch_socket);

	//This is a pseudo (non unique yet differentiating) identifier only for debugging purposes
	register_counter = (register_counter + 1) % (999);

}

//Tries to update the packages at the worker
//if it fails it does not update the table but will at a later time
//i.e. when another packet is sent
//note that the probability of this lock not being able to acquire is very low
//since it is only locked when a new package is installed for a very brief time
void send_if_new_packages() {
	int status;
	char *msg;

	if (is_update_set()) {

		status = try_unset_lock();

		if (status) {
			//Protocol(3)|Size(4)|Data(Size)
			msg = aggregate_packages();
			//Protocol(3)|Size(4)|Userid(3)|Data(Size)
			int size = strlen(msg) + 4;
			char newmsg[size];
			memset(newmsg,0,size);
			sprintf(newmsg, "%d|%s", registered_worker->ctip, msg);

			if (msg == NULL) {
				do_debug(LOG_LEVEL_LOW,
						"send_if_new_packages : couldnt find new packages!\n");
				return;
			}

			//i am glad i made this "automatic". So long sucking socks
			int sch_socket = create_tcp_client_socket(registered_worker->ip,
					registered_worker->control_port);

			send_proto_msg(sch_socket, PROTO_PACKAGES, msg);

			close(sch_socket);
			free(msg);
		}
	}
}

void *rrc_listener() {
	struct sockaddr_in si_me, si_other;
	int s, i, slen = sizeof(si_other);
	int nbytes = 0, ret;
	char buf[EVENT_MSG_LEN];

	struct timeval timeout;

	do_debug(LOG_LEVEL_LOW, "--->rrc_listener : Listener thread launched\n");

	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		perror("event_listener Error: socket");
	}

	bzero((char *) &si_me, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(RRC_LISTEN_PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	do_debug(LOG_LEVEL_LOW,
			"--->rrc_listener : Listening for rrc changes on port %d\n",
			RRC_LISTEN_PORT);

	if (bind(s, (struct sockaddr *) &si_me, sizeof(si_me)) == -1)
		perror("--->rrc_listener : event_listener Error: bind");

	while (1) {
		fd_set rd_set;

		FD_ZERO(&rd_set);
		FD_SET(s, &rd_set);
		//Select will wait
		timeout.tv_sec = RRC_TIMEOUT;
		timeout.tv_usec = 0;

		//do_debug(LOG_LEVEL_LOW,"tun_client : Waiting for traffic (select)\n");
		ret = select(s + 1, &rd_set, NULL, NULL, &timeout);

		if (ret == -1) {
			perror("--->rrc_listener : select()");
			continue;
		} else {
			if (!ret) {
				do_debug(LOG_LEVEL_LOW,
						"--->rrc_listener : No RRC messages. Reseting RRC state\n");
				strcpy(rrc_state, "unknown");
				continue;
			}
		}

		memset(buf, 0, EVENT_MSG_LEN);

		do_debug(LOG_LEVEL_LOW,
				"--->event_listener : Listening for rrc states\n");
		if ((nbytes = recvfrom(s, buf, EVENT_MSG_LEN, 0,
				(struct sockaddr *) &si_other, &slen)) == -1)
			perror("event_listener Error: recvfrom()");

		do_debug(LOG_LEVEL_LOW,
				"--->rrc_listener : Registering new RRC state : %s\n", buf);
		strcpy(rrc_state, buf);

		do_debug(LOG_LEVEL_LOW, "--->rrc_listener : RRC state registered.\n");
	}

	close(s);
}

void *statistics_updater(void *arg) {
	uid_node** top;
	int *traffic;

	db_connect("/sdcard/trinity-logs/stats.txt");

	while (1) {
		top = uid_retrieve_top_apps(10);
		traffic = get_traffic_stats();

		db_save_uid_states(top, 10, 0);
		db_save_traffic_states(traffic);
		db_save_system_states();
		db_save_cached_states(stats_con_cached, stats_con_not_cached,
				stats_uid_cached, stats_uid_not_cached, stats_broken_packets);

		free(top);
		free(traffic);

		//uid_print_top_apps(10);

		sleep(STATS_INTERVAL);
	}

	db_disconnect();
}

//Listens to ip changes from the monitor and state changes from dalvik apps (e.g screen state)
void *event_listener(void *arg) {
	struct sockaddr_in si_me, si_other;
	int s, i, slen = sizeof(si_other), k;
	int nbytes = 0, ret;
	char buf[EVENT_MSG_LEN];
	char interface[IFNAMSIZ + 1];
	char *buf_ptr;
	int aux_uid, aux_con, aux_proto, aux_dir, aux_screen, aux_action;

	int protocol;

	do_debug(LOG_LEVEL_LOW, "--->event_listener : Listener thread launched\n");

	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		perror("event_listener Error: socket");
	}

	bzero((char *) &si_me, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(EVENT_LISTEN_PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	do_debug(LOG_LEVEL_LOW,
			"--->event_listener : Listening for monitor updates on port %d\n",
			EVENT_LISTEN_PORT);

	if (bind(s, (struct sockaddr *) &si_me, sizeof(si_me)) == -1)
		perror("event_listener Error: bind");

	while (1) {
		fd_set rd_set;

		FD_ZERO(&rd_set);
		FD_SET(s, &rd_set);

		//do_debug(LOG_LEVEL_LOW,"tun_client : Waiting for traffic (select)\n");
		ret = select(s + 1, &rd_set, NULL, NULL, NULL);

		if (ret < 0 && errno == EINTR) {
			continue;
		}

		if (ret < 0) {
			perror("event_listener ERROR : select()");
		}

		memset(buf, 0, EVENT_MSG_LEN);

		do_debug(LOG_LEVEL_LOW,
				"--->event_listener : Listening for ip changes\n");
		if ((nbytes = recvfrom(s, buf, EVENT_MSG_LEN, 0,
				(struct sockaddr *) &si_other, &slen)) == -1)
			perror("event_listener Error: recvfrom()");

		buf[3] = '\0';
		protocol = atoi(buf);
		buf_ptr = buf + 4;

		switch (protocol) {
		case (PROTO_IPCHANGE):
			memset(interface, 0, IFNAMSIZ + 1);
			memcpy(interface, buf_ptr, nbytes - 4);
			do_debug(LOG_LEVEL_LOW,
					"--->event_listener : Change client interface : %s\n",
					interface);
			worker_notify_ip_change(interface);

			if (strncmp(interface, "eth", 3) == 0
					|| strncmp(interface, "wlan", 4) == 0)
				stats_con_type = STATS_CON_TYPE_WIFI;
			else
				stats_con_type = STATS_CON_TYPE_MOBILE;

			break;
		case (PROTO_SCREEN):
			stats_screen_state = atoi(buf_ptr);
			do_debug(LOG_LEVEL_LOW,
					"--->event_listener : Change screen state : %d\n",
					stats_screen_state);
			break;
		case (PROTO_RULE):

			do_debug(LOG_LEVEL_LOW,
					"--->event_listener : Received PROTO_RULE : %s\n", buf_ptr);

			for (i = 4, k = 4 + strlen(buf_ptr); i < k; i++)
				if (buf[i] == '.')
					buf[i] = '\0';

			aux_uid = atoi(buf_ptr);
			do_debug(LOG_LEVEL_LOW, "%s\n", buf_ptr);
			buf_ptr += strlen(buf_ptr) + 1;
			aux_con = atoi(buf_ptr);
			do_debug(LOG_LEVEL_LOW, "%s\n", buf_ptr);
			buf_ptr += strlen(buf_ptr) + 1;
			aux_proto = atoi(buf_ptr);
			do_debug(LOG_LEVEL_LOW, "%s\n", buf_ptr);
			buf_ptr += strlen(buf_ptr) + 1;
			aux_dir = atoi(buf_ptr);
			do_debug(LOG_LEVEL_LOW, "%s\n", buf_ptr);
			buf_ptr += strlen(buf_ptr) + 1;
			aux_screen = atoi(buf_ptr);
			do_debug(LOG_LEVEL_LOW, "%s\n", buf_ptr);
			buf_ptr += strlen(buf_ptr) + 1;
			aux_action = atoi(buf_ptr);
			do_debug(LOG_LEVEL_LOW, "%s\n", buf_ptr);

			do_debug(LOG_LEVEL_LOW,
					"--->event_listener : Received PROTO_RULE : (%d,%d,%d,%d,%d,%d)\n",
					aux_uid, aux_con, aux_proto, aux_dir, aux_screen,
					aux_action);

			add_new_rule(get_uid_node(0, aux_uid), aux_con, aux_proto, aux_dir,
					aux_screen, aux_action);
			break;
		}
	}
	close(s);
}

void monitor_notify(char *ip,int ctip) {
	struct sockaddr_in monitor_addr;
	int tun_client_sock_fd;
	int slen = sizeof(monitor_addr);

	if ((tun_client_sock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf("monitor_notify: socket creation failed");
		exit(1);
	}
	memset((char *) &monitor_addr, 0, sizeof(monitor_addr));
	monitor_addr.sin_family = AF_INET;
	monitor_addr.sin_port = htons(ROUTE_CONTROL_PORT);
	monitor_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	char buf[ROUTE_CONTROL_SIZE];

	memset(buf, 0, ROUTE_CONTROL_SIZE);
	sprintf(buf,"%s|10.0.1.%d",ip,ctip);
	do_debug(LOG_LEVEL_LOW, "Notifying monitor_master of worker address! %s\n",
			buf);
	//strcpy(buf, ip);

	if (sendto(tun_client_sock_fd, buf, ROUTE_CONTROL_SIZE, 0, &monitor_addr,
			sizeof(monitor_addr)) == -1) {
		perror("sendto()");
		exit(1);
	}
}

void init_tun() {
	do_debug(LOG_LEVEL_LOW, "Initializing tun interface\n");
	/* initialize tun/tap interface /dev/net/tun*/
	if ((tap_fd = tun_alloc(IF_PATH, IF_NAME, IFF_TUN | IFF_NO_PI)) < 0) {
		my_err("Error connecting to tun/tap interface %s!\n", IF_NAME);
		exit(1);
	}
	do_debug(LOG_LEVEL_LOW, "Successfully connected to interface %s\n",
			IF_NAME);
}

void init_sockets() {
	do_debug(LOG_LEVEL_LOW, "init_sockets : Creating traffic socket\n");
	if ((server_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		do_debug(LOG_LEVEL_LOW, "ERROR: UDP socket");
		exit(1);
	}

	do_debug(LOG_LEVEL_LOW, "init_sockets : Setting server to ip-%s p-%d\n",
			registered_worker->ip, registered_worker->traffic_port);
//bind socket to port
	bzero((char *) &server, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_port = htons(registered_worker->traffic_port);
	if (inet_aton(registered_worker->ip, &server.sin_addr) == 0) {
		do_debug(LOG_LEVEL_LOW, "init_sockets: UDP inet_aton() failed\n");
		exit(1);
	}

	bzero((char *) &client, sizeof(client));
	client.sin_family = AF_INET;
	client.sin_port = htons(registered_worker->traffic_port);
	client.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(server_fd, (struct sockaddr *) &client, sizeof(client)) < 0) {
		perror("bind()");
		exit(1);
	}

	do_debug(LOG_LEVEL_LOW, "init_sockets : Binded to port %d\n",
			registered_worker->traffic_port);
}

pthread_t ip_thr, stats_thr;

void init_ip_monitor() {
	do_debug(LOG_LEVEL_LOW, "init_ip_monitor : Launching monitor thread\n");
	if (pthread_create(&ip_thr, NULL, event_listener, NULL) != 0)
		perror("tun_client error: pthread");
}

void init_stats_updater() {
	do_debug(LOG_LEVEL_LOW, "init_ip_monitor : Launching Statistics thread\n");
	if (pthread_create(&stats_thr, NULL, statistics_updater, NULL) != 0)
		perror("tun_client error: pthread");
}

void init_scheduler(char *ip) {
//This is blocking
	do_debug(LOG_LEVEL_LOW,
			"init_scheduler : Retrieving a worker from the scheduler : %s\n", ip);
	scheduler_notify_register_client(ip);
	monitor_notify(registered_worker->ip,registered_worker->ctip);
}

void init_rrc() {
	pthread_t thr2;
	do_debug(LOG_LEVEL_LOW, "init_ip_monitor : Launching Statistics thread\n");
	if (pthread_create(&thr2, NULL, rrc_listener, NULL) != 0)
		perror("tun_client error: pthread");
}

void init_uid_monitor() {
	init_uid();
	sleep(2);
	do_debug(LOG_LEVEL_LOW, "Forcing package updates to server.\n");
	set_lock();
}

void init_modules(char *ip) {
	init_scheduler(ip);
	init_rrc();
	init_ip_monitor();
	init_uid_monitor();
	init_stats_updater();
	init_actions();
}

uid_node * get_uid_attributes(prg_node *node, int direction) {
	uid_node *uid = NULL;
	prg_node *cached;

	char tmp_ip[INET6_ADDRSTRLEN + 1];
	int tmp_port;

	if (node != NULL) {
		do_debug(LOG_LEVEL_LOW,
				"[!!!]get_uid_attributes : process packet returned a node!\n");

		if (direction == STATS_DIR_INCOMING) { //revert dst and src
			strcpy(tmp_ip, node->src);
			strcpy(node->src, node->dst);
			strcpy(node->dst, tmp_ip);

			tmp_port = node->src_port;
			node->src_port = node->dst_port;
			node->dst_port = tmp_port;
		}

		cached = get_info_from_node(node);

		if (cached != NULL) {
			stats_con_cached++; //Register cached event

			do_debug(LOG_LEVEL_LOW,
					"[!!!]get_uid_attributes : Retrieved node - ");
			print_prg_node(cached);
			uid = get_uid_node(0,cached->uid);
			if (uid != NULL) {
				stats_uid_cached++; //Register cached event

				do_debug(LOG_LEVEL_LOW,
						"[!!!]get_uid_attributes : Packet uid %d and name %s\n",
						uid->uid, uid->name);
			} else {
				stats_uid_not_cached++;
				do_debug(LOG_LEVEL_LOW,
						"[!!!]get_uid_attributes : Couldnt find uid for cached uid %d [XXX]\n",
						cached->uid);
			}
		} else {
			stats_con_not_cached++;
			do_debug(LOG_LEVEL_LOW,
					"[!!!]get_uid_attributes : Couldnt find proc net entry for node [XXX] - ");
			print_prg_node(node);
		}
	} else {
		stats_broken_packets++;
		do_debug(LOG_LEVEL_LOW,
				"[!!!]get_uid_attributes : process_packet returned NULL [XXX]\n");
	}

	print_counters();
	return uid;
}

char *read_properties() {
	FILE * fp;
	char * line = malloc(20);
	size_t len = 0;
	ssize_t read;

	fp = fopen("properties.config", "r");
	if (fp == NULL)
		exit(EXIT_FAILURE);

	fscanf(fp, "%s", line);

	fclose(fp);

	return line;
//if (line)
//    free(line); //TODO: release after running function
}

/*********************************2 Kings 2:23*************************************************
 From there Elisha went up to Bethel. As he was walking along the road,
 some boys came out of the town and jeered at him. “Get out of here, baldy!” they said.
 “Get out of here, baldy!” 24 He turned around, looked at them and called down a curse
 on them in the name of the Lord. Then two bears came out of the woods and mauled
 forty-two of the boys.
 **********************************************************************************************

 I am no Elisha but I am bald as well. Change this code knowing I am watching you closely :P
 */

int main(int argc, char *argv[]) {

	int maxfd, i, j;
	int nread, nwrite;
	char buffer[WORKER_TRAFFIC_MSG_LEN];
	char msg_buffer[PROTO_SIZE + 1 + WORKER_TRAFFIC_MSG_LEN + 64 + 1]; //TODO: Hardcoded maximum of package name
	char *msg;
	char *buffer_pointer = buffer;
	int pos = 0, len, action = ACTION_NONE;

	prg_node * node;
	uid_node *uid;

	char *sch_ip = malloc(20);
	strcpy(sch_ip, read_properties()); //TODO: read_properties return should be released

	memset(rrc_state, 0, RRC_STATE_LENGTH);
	strcpy(rrc_state, "unknown");

	init_tun();
	init_modules(sch_ip);
	sleep(3);
	init_sockets();

	/* use select() to handle two descriptors at once */
	maxfd = (tap_fd > server_fd) ? tap_fd : server_fd;

	while (1) {
		int ret;
		fd_set rd_set;

		FD_ZERO(&rd_set);
		FD_SET(tap_fd, &rd_set);
		FD_SET(server_fd, &rd_set);

		//do_debug(LOG_LEVEL_LOW,"tun_client : Waiting for traffic (select)\n");
		ret = select(maxfd + 1, &rd_set, NULL, NULL, NULL);

		if (ret < 0 && errno == EINTR) {
			continue;
		}

		if (ret < 0) {
			perror("select()");
			exit(1);
		}

		//OUTGOING TRAFFIC (APP->TUN->PROXY->PDP0->SERVER)
		if (FD_ISSET(tap_fd, &rd_set)) {

			do_debug(LOG_LEVEL_LOW, "########CLIENT->WEB#########\n");

			nread = cread(tap_fd, buffer, WORKER_TRAFFIC_MSG_LEN);
			//do_debug(LOG_LEVEL_LOW,"Read %d bytes\n", nread);
			if (nread <= 0) {
				stats_broken_packets++;
				continue;
			}

			do_debug(LOG_LEVEL_LOW, "Packet size: %d", nread);

			node = process_packet(buffer, nread);
			uid = get_uid_attributes(node, STATS_DIR_OUTGOING);

			if (uid != NULL)
				action = get_action_for_rule(uid->rules, uid->uid,
						stats_con_type, node->proto, STATS_DIR_OUTGOING,
						stats_screen_state);
			else
				action = ACTION_NONE;

			//Insert uid into message
			pos = 0;
			memset(msg_buffer, 0,
					PROTO_SIZE + 1 + WORKER_TRAFFIC_MSG_LEN + 64 + 1);
			msg = msg_buffer;
			if (uid != NULL) {
				len = strlen(uid->name);
				sprintf(msg_buffer, "%d|%d|%d|%s|", PROTO_TRAFFIC,
						stats_screen_state, uid->uid, rrc_state);
				pos = strlen(msg);
			} else {
				len = 7;
				sprintf(msg_buffer, "%d|%d|%d|%s|", PROTO_TRAFFIC,
						stats_screen_state, -1, rrc_state);
				pos = strlen(msg);
			}
			memcpy(msg + pos, buffer, nread);

			switch (action) //Check the Protocol and do accordingly...
			{
			case ACTION_BLOCK:
				do_debug(LOG_LEVEL_LOW, "blocking..");
				block_packet("");
				//-----------------------------------------------------------< EXIT POINT
				do_debug(LOG_LEVEL_HIGH,
						"CLIENT->WEB,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
						"ACTION_BLOCK",
						(stats_screen_state == STATS_SCREEN_STATE_OFF) ?
								"SCREEN_OFF" : "SCREEN_ON",
						(uid == NULL) ? -1 : uid->uid,
						(uid == NULL) ? "unknow_uid" : uid->name, node->proto,
						node->src, node->dst, node->src_port, node->dst_port, 0,
						(stats_con_type == STATS_CON_TYPE_MOBILE) ?
								"3G" : "WIFI", rrc_state);

				if (node != NULL)
					free(node);
				continue;
				break;
			case ACTION_ALLOW:
				break;
			case ACTION_DELAY:
				do_debug(LOG_LEVEL_LOW, "delaying..");
				delay_packet(msg_buffer, nread + pos, registered_worker->ip,
						registered_worker->traffic_port);
				//------------------------------------------------------------< EXIT POINT
				do_debug(LOG_LEVEL_HIGH,
						"CLIENT->WEB,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
						"ACTION_DELAY",
						(stats_screen_state == STATS_SCREEN_STATE_OFF) ?
								"SCREEN_OFF" : "SCREEN_ON",
						(uid == NULL) ? -1 : uid->uid,
						(uid == NULL) ? "unknow_uid" : uid->name, node->proto,
						node->src, node->dst, node->src_port, node->dst_port, 0,
						(stats_con_type == STATS_CON_TYPE_MOBILE) ?
								"3G" : "WIFI", rrc_state);

				if (node != NULL)
					free(node);
				continue;
				break;
			default:
				break;
			}

			send_if_new_packages();

			//Body of udp packet is the ip packet
			//server is the specific server
			nwrite = sendto(server_fd, msg_buffer, nread + pos, 0,
					(struct sockaddr*) &server, sizeof(server));

			//do_debug(LOG_LEVEL_LOW,"Written %d bytes\n", nwrite);

			if (nwrite == 0)
				perror("ERROR : sendto()");

			if (nwrite > 0 && uid != NULL) {
				uid->MB_sent += ((float) nwrite) / 1024; //in KB
			} else {
				if (nwrite <= 0)
					stats_broken_packets++;
			}

			//------------------------------------------------------------< EXIT POINT
			if (node != NULL) {
				do_debug(LOG_LEVEL_HIGH,
						"CLIENT->WEB,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
						"ACTION_ALLOW",
						(stats_screen_state == STATS_SCREEN_STATE_OFF) ?
								"SCREEN_OFF" : "SCREEN_ON",
						(uid == NULL) ? -1 : uid->uid,
						(uid == NULL) ? "unknow_uid" : uid->name, node->proto,
						node->src, node->dst, node->src_port, node->dst_port,
						nwrite,
						(stats_con_type == STATS_CON_TYPE_MOBILE) ?
								"3G" : "WIFI", rrc_state);

				free(node);
			}
			//direction, action, screen, uid, name, proto, src, dst, sp, dp, report, size, connection, rrc
		}

		//INCOMING TRAFFIC (SERVER->PDP0->PROXY->TUN->APP)
		if (FD_ISSET(server_fd, &rd_set)) {

			do_debug(LOG_LEVEL_LOW, "########WEB->CLIENT##########\n", IF_NAME);

			socklen_t slen = sizeof(struct sockaddr_in);
			//client is ANYADDR
			nread = recvfrom(server_fd, buffer, WORKER_TRAFFIC_MSG_LEN, 0,
					(struct sockaddr*) &client, &slen);

			if (nread <= 0) {
				stats_broken_packets++;
				continue;
			}

			do_debug(LOG_LEVEL_LOW, "Packet size: %d", nread);

			node = process_packet(buffer, nread);
			uid = get_uid_attributes(node, STATS_DIR_INCOMING);

			if (uid != NULL) {
				action = get_action_for_rule(uid->rules, uid->uid,
						stats_con_type, node->proto, STATS_DIR_INCOMING,
						stats_screen_state);
			} else
				action = ACTION_NONE;

			switch (action) //Check the Protocol and do accordingly...
			{
			case ACTION_BLOCK:
				do_debug(LOG_LEVEL_LOW, "Packet blocked!\n");
				block_packet("");
				//------------------------------------------------------------< EXIT POINT
				do_debug(LOG_LEVEL_HIGH,
						"WEB->CLIENT,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
						"ACTION_BLOCK",
						(stats_screen_state == STATS_SCREEN_STATE_OFF) ?
								"SCREEN_OFF" : "SCREEN_ON",
						(uid == NULL) ? -1 : uid->uid,
						(uid == NULL) ? "unknow_uid" : uid->name, node->proto,
						node->src, node->dst, node->src_port, node->dst_port, 0,
						(stats_con_type == STATS_CON_TYPE_MOBILE) ?
								"3G" : "WIFI", rrc_state);

				if (node != NULL)
					free(node);
				continue;
				break;
			case ACTION_ALLOW:
				break;
			case ACTION_DELAY:
				//delay_packet(buffer, nread); Non supported on incoming
				//continue;
				break;
			default:
				break;
			}

			send_if_new_packages(); //So that it server knows uids before message is sent

			nwrite = cwrite(tap_fd, buffer, nread);
			//do_debug(LOG_LEVEL_LOW,"Written %d bytes\n", nwrite);

			if (nwrite == 0)
				perror("ERROR : cwrite()");

			if (nwrite > 0 && uid != NULL)
				uid->MB_received += ((float) nwrite) / 1024; //in KB
			else {
				if (nwrite <= 0)
					stats_broken_packets++;
			}

			//------------------------------------------------------------< EXIT POINT
			if (node != NULL) {
				do_debug(LOG_LEVEL_HIGH,
						"WEB->CLIENT,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
						"ACTION_ALLOW",
						(stats_screen_state == STATS_SCREEN_STATE_OFF) ?
								"SCREEN_OFF" : "SCREEN_ON",
						(uid == NULL) ? -1 : uid->uid,
						(uid == NULL) ? "unknow_uid" : uid->name, node->proto,
						node->src, node->dst, node->src_port, node->dst_port,
						nwrite,
						(stats_con_type == STATS_CON_TYPE_MOBILE) ?
								"3G" : "WIFI", rrc_state);

				free(node);
			}
		}

		//uid_print_top_apps(5);
	}

	return (0);
}

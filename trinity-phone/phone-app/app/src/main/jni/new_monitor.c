#include "new_monitor.h"

#define TIMER_SET 5
#define TIMER_IFACE 5
#define LOG_VERSION "2.0"

//TODO: send new interfaces to client
//TODO: possibly remove the scheduled lock

//scheduled - locks the scheduling of rules; 
//xtimer    - locks the timer variable; 
//xcounter  - locks the allow_server counter
pthread_mutex_t scheduled, xtimer, xcounter;
int timer = 0;

int allow_server = 0;
//this is a flag that says if the worker ip was initialized (i.e. wait for it from the tun_client)
int initialized_flag = 0;
//this is a counter of worker iptable rule deletions in order to ignore demanded changes

/*When an IP change is found, the tun_client should be notified*/
struct sockaddr_in tun_client_address;
int tun_client_sock_fd;
struct sockaddr_in service_root_address;
int service_root_sock_fd;

char *global_interface_name = NULL;
char *global_previous_interface_name = NULL;
char global_worker_ip[34];
char global_tun_ip[34];
char global_src[34];
char global_via[34];

//Thread that implements the iptable changes.
//scheduled lock makes sure only one iptable change thread is run at each time 
//(although, thanksfully to the timer lock this should never happen).
void *schedule_implement_iptable_rules(){
	do_debug(LOG_LEVEL_LOW, "-->schedule_implement_iptable_rules : waiting to run\n");
	pthread_mutex_lock(&scheduled);
	do_debug(LOG_LEVEL_LOW, "-->schedule_implement_iptable_rules : ready to run\n");

	while(1){
		sleep(1);
		pthread_mutex_lock(&xtimer);
		do_debug(LOG_LEVEL_LOW,"-->Timer = %d\n",timer);
		if(timer>0)
			timer--;
		else 
			break;
		pthread_mutex_unlock(&xtimer); 
	}
	pthread_mutex_unlock(&xtimer);

	implement_iptable_rules();

	pthread_mutex_unlock(&scheduled);
}

//Function that launches a thread for setting new iptable rules
//xtimer lock makes sure that the timer is not changed as the same time as the thread is changing it
void try_scheduling_new_rules(){
	pthread_t thr;

		pthread_mutex_lock(&xtimer);
		if(timer>0){
			timer=TIMER_SET;
			pthread_mutex_unlock(&xtimer);
			return;
		}
		else{
			timer=TIMER_SET;
			pthread_create(&thr, NULL, schedule_implement_iptable_rules, NULL);
		}
		pthread_mutex_unlock(&xtimer);
	
}

void parse_link_messages(struct nlmsghdr *nlh){

	struct ifinfomsg *iface;
	unsigned int interface_flags;
	struct rtattr *attribute;
	int len, my_iface_index;
	char my_iface_name[IFNAMSIZ+1];
	 
	iface = NLMSG_DATA(nlh);
	len = nlh->nlmsg_len - NLMSG_LENGTH(sizeof(*iface));
	 
	/* loop over all attributes for the NEWLINK message */
	for (attribute = IFLA_RTA(iface); RTA_OK(attribute, len); attribute = RTA_NEXT(attribute, len))
	{
		switch(attribute->rta_type)
		{
			case IFLA_IFNAME:
				my_iface_index = iface->ifi_index;
				strcpy(my_iface_name,(char *) RTA_DATA(attribute));
				do_debug(LOG_LEVEL_LOW,"Interface %d : %s\n", my_iface_index, my_iface_name);
				break;
			case IFLA_STATS:
			default:
				break;
		}
	}

	interface_flags = iface->ifi_flags;

	if(strcmp(my_iface_name,"tun11")==0){
		do_debug(LOG_LEVEL_LOW,"Ignoring tun11\n");
		return;
	}

	if(!(interface_flags & IFF_UP)){
		do_debug(LOG_LEVEL_LOW,"-->Interface is down!\n");
		if(global_interface_name!=NULL){
			if(strcmp(my_iface_name,global_interface_name)==0){
				do_debug(LOG_LEVEL_LOW,"-->The previous if is dead! Scheduling new rules!\n");
				try_scheduling_new_rules();
			}
			else
				do_debug(LOG_LEVEL_LOW,"-->Someone died, not actual Registered interface! Ignore!\n");
		}
	}
	else{
		do_debug(LOG_LEVEL_LOW,"-->Interface is up!\n");
		if(global_interface_name!=NULL){
			if(strcmp(my_iface_name,global_interface_name)!=0){
				do_debug(LOG_LEVEL_LOW,"-->There is a new interface! Scheduling new rules!\n");
				try_scheduling_new_rules();
			}
			else{
				do_debug(LOG_LEVEL_LOW,"-->Some noob is putting up something that was already up, guess my middle finger! Ignore!\n");
			}
		}
	}

		
	if (nlh->nlmsg_type == RTM_NEWLINK) {
			//new_interface();
	}

	if (nlh->nlmsg_type == RTM_DELLINK) {
			//del_interface();
	}
}

void parse_addr_messages(struct nlmsghdr *nlh){

		if (nlh->nlmsg_type == RTM_NEWADDR) {
			xrtmnewaddr();
		}

		if (nlh->nlmsg_type == RTM_DELADDR) {
			xrtmdeladdr();
		}
}

void parse_route_messages(struct nlmsghdr *nlh){

	char destination_address[32] = "";
	char source_address[32] = "";
	char gateway_address[32] = "";
	char pref_address[32] = "";
	int iface_in = -1, iface_out = -1;

	struct rtmsg *route_entry; /* This struct represent a route entry \
                                    in the routing table */
	struct rtattr *route_attribute; /* This struct contain route \
                                            attributes (route type) */
	int route_attribute_len = 0;

	bzero(destination_address, sizeof(destination_address));
	bzero(gateway_address, sizeof(gateway_address));

	/* Get the route data */
		route_entry = (struct rtmsg *) NLMSG_DATA(nlh);

		/* We are just interested in main routing table */
		//if (route_entry->rtm_table != RT_TABLE_MAIN)
		//	continue;

		/* Get attributes of route_entry */
		route_attribute = (struct rtattr *) RTM_RTA(route_entry);

		/* Get the route atttibutes len */
		route_attribute_len = RTM_PAYLOAD(nlh);
		/* route_listener through all attributes */
		for (; RTA_OK(route_attribute, route_attribute_len); route_attribute =
				RTA_NEXT(route_attribute, route_attribute_len)) {

			/* Get the destination address */
			if (route_attribute->rta_type == RTA_DST) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute),
						destination_address, sizeof(destination_address));
			}
			/* Get the source address */
			if (route_attribute->rta_type == RTA_SRC) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute), source_address,
						sizeof(destination_address));
			}
			/* Get the gateway (Next hop) */
			if (route_attribute->rta_type == RTA_GATEWAY) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute), gateway_address,
						sizeof(gateway_address));
			}
			/* Get the interface in */
			if (route_attribute->rta_type == RTA_IIF) {
				memcpy(&iface_in, RTA_DATA(route_attribute), sizeof(int));
			}

			/* Get the interface out */
			if (route_attribute->rta_type == RTA_OIF) {
				memcpy(&iface_out, RTA_DATA(route_attribute), sizeof(int));
			}

			if (route_attribute->rta_type == RTA_PREFSRC) {
				inet_ntop(AF_INET, RTA_DATA(route_attribute), pref_address,
						sizeof(pref_address));
			}
		}

		if (nlh->nlmsg_type == RTM_DELROUTE) {
			del_route(pref_address, source_address, destination_address,
					gateway_address, iface_in, iface_out);
		}else
		if (nlh->nlmsg_type == RTM_NEWROUTE) {
			new_route(pref_address, source_address, destination_address,
					gateway_address, iface_in, iface_out);
		}
		
}

int route_listener(int sock, struct sockaddr_nl *addr) {
	int received_bytes = 0;
	struct nlmsghdr *nlh;

	char buffer[BUFFER_SIZE];
	bzero(buffer, sizeof(buffer));

	while (1) {
		received_bytes = recv(sock, buffer, sizeof(buffer), 0);
		if (received_bytes < 0)
			ERR_RET("recv");

		nlh = (struct nlmsghdr *) buffer;

		if (nlh->nlmsg_type == NLMSG_DONE)
			break;

		break;
	}

	for (; NLMSG_OK(nlh, received_bytes);
			nlh = NLMSG_NEXT(nlh, received_bytes)) {

		do_debug(LOG_LEVEL_LOW,"------nlh->nlmsg_type = %d------\n",nlh->nlmsg_type);
		if(nlh->nlmsg_type >= 16 && nlh->nlmsg_type <= 19){
			do_debug(LOG_LEVEL_LOW, "Received LINK MESSAGE\n");
			parse_link_messages(nlh);
		}else
		if(nlh->nlmsg_type >= 24 && nlh->nlmsg_type <= 26){
			do_debug(LOG_LEVEL_LOW, "Received ROUTE MESSAGE\n");
			parse_route_messages(nlh);
		}else
		if(nlh->nlmsg_type >= 20 && nlh->nlmsg_type <= 22){
			do_debug(LOG_LEVEL_LOW, "Received ADDR MESSAGE\n");
			//parse_addr_messages(nlh);
		}else
			do_debug(LOG_LEVEL_LOW,"Unrecognized message!\n");

		
		
	}
	return 0;
}

/*
1 (0001) - "ip route del default via %s dev %s\n",gateway_address, global_interface_name
2 (0010) - "ip route del %s dev %s\n",destination_address, global_interface_name
3 (0011) - "ip route del %s via %s dev %s\n",destination_address, gateway_address, global_interface_name
10 (1010) - "ip route del %s/24 src %s dev %s",destination_address, pref_address, global_interface_name
11 (1011) - "ip route del %s src %s via %s dev %s",destination_address, pref_address, gateway_address,global_interface_name
*/

int route_mask(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address) {
	int mask = 0;
	if (strcmp(pref_address, "") != 0)
		mask = mask | 8;
	if (strcmp(source_address, "") != 0)
		mask = mask | 4;
	if (strcmp(destination_address, "") != 0)
		mask = mask | 2;
	if (strcmp(gateway_address, "") != 0)
		mask = mask | 1;

	return mask;
}

void del_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out){

	int mask = route_mask(pref_address, source_address, destination_address,
			gateway_address);

	guess_rule("del", mask, pref_address, source_address, destination_address,
			gateway_address);

	if(strcmp(pref_address,global_worker_ip) == 0 || strcmp(source_address,global_worker_ip) == 0 ||
		strcmp(destination_address,global_worker_ip) == 0 || strcmp(gateway_address,global_worker_ip) == 0){

		do_debug(LOG_LEVEL_LOW,"-->del : someone deleted the server ip!\n");
		pthread_mutex_lock(&xcounter);

		if(allow_server>0){
			allow_server--;
			do_debug(LOG_LEVEL_LOW,"-->del : seems deletion allowed! Ignoring|\n");
		}
		else{
			try_scheduling_new_rules();
			do_debug(LOG_LEVEL_LOW,"-->del : seems deletion not allowed! I should check!\n");
		}

		pthread_mutex_unlock(&xcounter);
	}

	do_debug(LOG_LEVEL_LOW,"-->del : seems it is deleting something not worker_ip related! Lets if is tun related!\n");

	if(strncmp(pref_address,"10.0.1.",7) == 0 || strncmp(source_address,"10.0.1.",7) == 0 ||
		strncmp(destination_address,"10.0.1.",7) == 0 || strncmp(gateway_address,"10.0.1.",7) == 0){
		do_debug(LOG_LEVEL_LOW,"-->This rule was tun based, for now policy is ignore!!\n");
		return;
	}

	do_debug(LOG_LEVEL_LOW,"-->del : not tun, not worker, possibly and default interface deletion. Should check if interface is down!\n");

	if(mask == 1){
		//Sometimes the interface takes longer to be turned down
		do_debug(LOG_LEVEL_LOW,"-->del : Mask says this was a default route deletion. Ignore!\n");
		//pthread_t ptr;
		//if (pthread_create(&ptr, NULL, check_interface_state, NULL) != 0)
		//	perror("error: pthread");
	}
	else
		do_debug(LOG_LEVEL_LOW,"-->del : Mask says this was NOT a default route deletion. Ignoring!\n");
}

void *check_interface_state(){
	int t = TIMER_IFACE;

	while(t>0){
		do_debug(LOG_LEVEL_LOW, "-->Iface Timer = %d\n",t);
		sleep(1);
		t--;
	}
	do_debug(LOG_LEVEL_LOW,"-->delayed check_interface_state : verifying the iface state!\n");

	if(global_interface_name != NULL){
		do_debug(LOG_LEVEL_LOW,"-->del : checking interface - %s\n", global_interface_name);
		if(is_interface_online(global_interface_name)){
			do_debug(LOG_LEVEL_LOW,"-->del : interface is up! Probably not anything worth looking for! Ignoring!\n");
		}
		else{
			do_debug(LOG_LEVEL_LOW,"-->del : It seems the interface is down... Lets check this!\n");
			try_scheduling_new_rules();
		}
	}
	else{
		do_debug(LOG_LEVEL_LOW,"-->del : There was no interface yet\n");
	}
}

void new_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out){

	int mask = route_mask(pref_address, source_address, destination_address,
			gateway_address);

	guess_rule("add",mask,pref_address, source_address, destination_address,
			gateway_address);

	if(strncmp(pref_address,"10.0.1.",7) == 0 || strncmp(source_address,"10.0.1.",7) == 0 ||
		strncmp(destination_address,"10.0.1.",7) == 0 || strncmp(gateway_address,"10.0.1.",7) == 0){
		do_debug(LOG_LEVEL_LOW,"-->This rule was tun based, ignoring!\n");
		return;

	}

	if(strcmp(pref_address,global_worker_ip) == 0 || strcmp(source_address,global_worker_ip) == 0 ||
		strcmp(destination_address,global_worker_ip) == 0 || strcmp(gateway_address,global_worker_ip) == 0){
		do_debug(LOG_LEVEL_LOW,"-->This rule was worker_ip based, ignoring!\n");
		return;

	}

	do_debug(LOG_LEVEL_LOW,"-->There was a added rule (not tun related) that I should check!\n");
	try_scheduling_new_rules();
}

void new_interface(){

	//Sometimes the interface takes longer to be turned down
	do_debug(LOG_LEVEL_LOW,"-->[NEW EVENT!!!] NEW INTERFACE : Schedule verification for interface state!\n");
	//pthread_t ptr;
	//if (pthread_create(&ptr, NULL, check_interface_state, NULL) != 0)
	//	perror("error: pthread");
}

void del_interface(){

		//Sometimes the interface takes longer to be turned down
		do_debug(LOG_LEVEL_LOW,"-->[NEW EVENT!!!] DEL INTERFACE : Schedule verification for interface state!\n");
		//pthread_t ptr;
		//if (pthread_create(&ptr, NULL, check_interface_state, NULL) != 0)
		//	perror("error: pthread");
}

void xrtmdeladdr(){
	
	do_debug(LOG_LEVEL_LOW,"-->[NEW EVENT!!!]  DELADDR : Schedule verification for interface state!\n");

}

void xrtmnewaddr(){

	do_debug(LOG_LEVEL_LOW,"-->[NEW EVENT!!!]  NEWADDR : Schedule verification for interface state!\n");
	
}

void guess_rule(char *action, int mask, char * pref_address, char *source_address,
		char *destination_address, char *gateway_address){
	switch (mask) {
		case (1): //0001 - Default route
			do_debug(LOG_LEVEL_LOW, "Guess : ip route %s default via %s dev ..\n",
					action, gateway_address);

			break;
		case (2): //0010 - Map address (e.g Server rule)
			do_debug(LOG_LEVEL_LOW, "Guess : ip route %s %s dev ..\n",
					action, destination_address);

			break;
		case (3): //0011 - Complete route
			do_debug(LOG_LEVEL_LOW, "Guess : ip route %s %s via %s dev ..\n",
					action, destination_address, gateway_address);

			break;
		case (10): //1010 - Set pref_source!
			do_debug(LOG_LEVEL_LOW, "Guess : ip route %s %s/24 src %s dev ..\n",
					action, destination_address, pref_address);

			break;
		case (11): //1011
			do_debug(LOG_LEVEL_LOW, "Guess : ip route %s %s src %s via %s dev ..\n",
					action, destination_address, pref_address, gateway_address);

			break;
		default:
			do_debug(LOG_LEVEL_LOW, "Guess : Ignore! (%s,src-%s,via-%s,unk-%s)\n",
				destination_address, pref_address, gateway_address, source_address);
			break;
	}
}

int get_info(int (*f)(int, char*, char*, char*, int)) {
	struct nlmsghdr *nl_msg;
	struct rtmsg *rt_msg;
	struct route_info route_info;
	char msgBuf[BUFFER_SIZE]; // pretty large buffer
	char prefsource[33 + 1];
	char destination[33 + 1];
	char gateway[33 + 1];

	int ret;

	int sock, len, msgSeq = 0;

	/* Create Socket */
	if ((sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE)) < 0)
		perror("Socket Creation: ");

	/* Initialize the buffer */
	memset(msgBuf, 0, sizeof(msgBuf));

	/* point the header and the msg structure pointers into the buffer */
	nl_msg = (struct nlmsghdr *) msgBuf;
	rt_msg = (struct rtmsg *) NLMSG_DATA(nl_msg);

	/* Fill in the nlmsg header*/
	nl_msg->nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg)); // Length of message.
	nl_msg->nlmsg_type = RTM_GETROUTE; // Get the routes from kernel routing table .

	nl_msg->nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST; // The message is a request for dump.
	nl_msg->nlmsg_seq = msgSeq++; // Sequence of the message packet.
	nl_msg->nlmsg_pid = getpid(); // PID of process sending the request.

	/* Send the request */
	if (send(sock, nl_msg, nl_msg->nlmsg_len, 0) < 0) {
		fprintf(stderr, "Write To Socket Failed...\n");
		return;
	}

	/* Read the response */
	if ((len = read_netlink_sockets(sock, msgBuf, sizeof(msgBuf), msgSeq, getpid()))
			< 0) {
		fprintf(stderr, "Read From Socket Failed...\n");
		return;
	}

	(*f)(1, NULL, NULL, NULL, -1); //reset counters (if any)

	/* Parse and print the response */
	for (; NLMSG_OK(nl_msg, len); nl_msg = NLMSG_NEXT(nl_msg, len)) {
		memset(&route_info, 0, sizeof(route_info));
		if (parse_routes(nl_msg, &route_info) < 0)
			continue; // don't check route_info if it has not been set up

		memset(prefsource, 0, 33 + 1);
		memset(destination, 0, 33 + 1);
		memset(gateway, 0, 33 + 1);
		strcpy(prefsource, (char *) inet_ntoa(route_info.src_addr));
		strcpy(destination, (char *) inet_ntoa(route_info.dst_addr));
		strcpy(gateway, (char *) inet_ntoa(route_info.gateway));

		do_debug(LOG_LEVEL_LOW,
				"FOUND: [prefsource %s destination %s gateway %s if_out %d]\n",
				prefsource, destination, gateway, route_info.if_index);

		ret = (*f)(0, prefsource, destination, gateway, route_info.if_index);

		if (ret != UNFINISHED && ret != ALL_OK) {
			do_debug(LOG_LEVEL_LOW, "get_info : BREAK!\n");
			break;
		}
	}

	close(sock);
	return ret;
}

//ioctl is a synchronous run_as_root call.
char *find_if_name(unsigned int index) {
	int fd;
	char *ret = (char *) malloc(IFNAMSIZ + 1);
	memset(ret, 0, IFNAMSIZ + 1);
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	struct ifreq ifr;
	ifr.ifr_ifindex = index;
	ioctl(fd, SIOCGIFNAME, (void*) &ifr);
	memcpy(ret, ifr.ifr_name, IFNAMSIZ);
	do_debug(LOG_LEVEL_LOW, "if name : %s\n", ret);
	close(fd);

	return ret;
}

int is_interface_online(char *itface) {
    struct ifreq ifr;
    int sock = socket(PF_INET6, SOCK_DGRAM, IPPROTO_IP);
    memset(&ifr, 0, sizeof(ifr));
    strcpy(ifr.ifr_name, itface);
    if (ioctl(sock, SIOCGIFFLAGS, &ifr) < 0) {
            perror("SIOCGIFFLAGS");
    }
    close(sock);
    return !!(ifr.ifr_flags & IFF_UP);
}

int get_gateway_info(int reset, char *src, char* noop,
		char* via, int dev) {

	if (reset)
		return UNFINISHED;

	char *interface_name, *aux;

	do_debug(LOG_LEVEL_LOW,"retrieving iptables : Always ignored destination - %s\n", noop);

	interface_name = find_if_name(dev);

	if(strcmp(interface_name,TUN_NAME)==0){
			do_debug(LOG_LEVEL_LOW,"retrieving iptables : (IGNORE RULE) found tun interface- %s\n",interface_name);
			free(interface_name); //not needed
			return;
	}
	else{
			do_debug(LOG_LEVEL_LOW,"retrieving iptables : found possibily new interface - %s\n",interface_name);

			//Store new interface
			aux = global_previous_interface_name;
			global_previous_interface_name = global_interface_name;
			global_interface_name = interface_name;
			if(aux != NULL)
				free(aux);
	}

	if(strncmp(src, "10.0.1.",7)==0 || strncmp(noop, "10.0.1.",7)==0 || strncmp(via, "10.0.1.",7)==0){
		do_debug(LOG_LEVEL_LOW,"retrieving iptables : detected the presence of 10.0.1. Ignoring!\n");
		return;
	}

	if(strcmp(via, "0.0.0.0") != 0){
		do_debug(LOG_LEVEL_LOW,"retrieving iptables : found via : %s\n",via);
		strcpy(global_via,via);
	}

	if(strcmp(src, "0.0.0.0") != 0){
		do_debug(LOG_LEVEL_LOW,"retrieving iptables : found src : %s\n",src);
		strcpy(global_src,src);
	}

	return UNFINISHED;
}

int read_netlink_sockets(int sockFd, char *bufPtr, size_t buf_size, int seqNum, int pId) {
	struct nlmsghdr *nl_hdr;
	int readLen = 0, msgLen = 0;

	do {
		/* Recieve response from the kernel */
		if ((readLen = recv(sockFd, bufPtr, buf_size - msgLen, 0)) < 0)
			ERR_RET("SOCK READ: ");

		nl_hdr = (struct nlmsghdr *) bufPtr;

		/* Check if the header is valid */
		if ((NLMSG_OK(nl_hdr, readLen) == 0)
				|| (nl_hdr->nlmsg_type == NLMSG_ERROR))
			ERR_RET("Error in recieved packet");

		/* Check if the its the last message */
		if (nl_hdr->nlmsg_type == NLMSG_DONE) {
			break;
		} else {
			/* Else move the pointer to buffer appropriately */
			bufPtr += readLen;
			msgLen += readLen;
		}

		/* Check if its a multi part message */
		if ((nl_hdr->nlmsg_flags & NLM_F_MULTI) == 0) {
			/* return if its not */
			break;
		}
	} while ((nl_hdr->nlmsg_seq != seqNum) || (nl_hdr->nlmsg_pid != pId));

	return msgLen;
}


/* parse the route info returned */
int parse_routes(struct nlmsghdr *nl_hdr, struct route_info *rtInfo) {
	struct rtmsg *rt_msg;
	struct rtattr *rt_attr;
	int rt_len;

	rt_msg = (struct rtmsg *) NLMSG_DATA(nl_hdr);

	/* If the route is not for AF_INET or does not belong to main routing table then return. */
	if ((rt_msg->rtm_family != AF_INET) || (rt_msg->rtm_table != RT_TABLE_MAIN))
		return -1;

	/* get the rtattr field */
	rt_attr = (struct rtattr *) RTM_RTA(rt_msg);
	rt_len = RTM_PAYLOAD(nl_hdr);

	for (; RTA_OK(rt_attr, rt_len); rt_attr = RTA_NEXT(rt_attr, rt_len)) {
		switch (rt_attr->rta_type) {
		case RTA_OIF:
			memcpy(&rtInfo->if_index, RTA_DATA(rt_attr),
					sizeof(rtInfo->if_index));
			break;

		case RTA_GATEWAY:
			memcpy(&rtInfo->gateway, RTA_DATA(rt_attr),
					sizeof(rtInfo->gateway));
			break;

		case RTA_PREFSRC:
			memcpy(&rtInfo->src_addr, RTA_DATA(rt_attr),
					sizeof(rtInfo->src_addr));
			break;

		case RTA_DST:
			memcpy(&rtInfo->dst_addr, RTA_DATA(rt_attr),
					sizeof(rtInfo->dst_addr));
			break;
		}
	}
	return 0;
}

void set_dns() {
	//Set dns to google dns
	do_debug(LOG_LEVEL_LOW, "set dns\n");
	run_as_root("setprop net.rmnet0.dns1 8.8.8.8");
	run_as_root("setprop net.rmnet0.dns2 8.8.4.4");

	run_as_root("setprop net.dns1 8.8.8.8");
	run_as_root("setprop net.dns2 8.8.4.4");

	run_as_root("setprop net.pdp0.dns1 8.8.8.8");
	run_as_root("setprop net.pdp0.dns2 8.8.4.4");

	run_as_root("setprop dhcp.eth0.dns1 8.8.8.8");
	run_as_root("setprop dhcp.eth0.dns2 8.8.4.4");

	run_as_root("setprop dhcp.wlan0.dns1 8.8.8.8");
	run_as_root("setprop dhcp.wlan0.dns2 8.8.4.4");

	run_as_root("iptables -t nat -I OUTPUT -p udp --dport 53 -j DNAT --to-destination 8.8.8.8:53");

	//TODO : DISABLE APN PROXY
}

void run_as_root(char *cmd) {
	do_debug(LOG_LEVEL_LOW, "Running command as root! %s\n", cmd);
	int len = strlen(cmd);
	int msg_size = 3 + 1 + len + 1; //Assumes max of strlen of 3 digits
	char msg[msg_size];

	memset(msg, 0, msg_size);
	sprintf(msg, "%3d|%s", len, cmd); //forces 3 digit representation of len

	if (sendto(service_root_sock_fd, msg, strlen(msg), 0, &service_root_address,
			sizeof(service_root_address)) == -1)
		perror("sendto()");
}



void implement_iptable_rules(){
	char command[512];
	memset(command,0,512);

	strcpy(global_via,"");
	strcpy(global_src,"");

	if(global_interface_name != NULL){
		free(global_interface_name);
		global_interface_name = NULL;
	}

	do_debug(LOG_LEVEL_LOW,"####Started implement_iptable_rules######\n");

	get_info(get_gateway_info);
	int gotvia = (strcmp(global_via,"")==0)?0:1, gotsrc = (strcmp(global_src,"")==0)?0:1;

	run_as_root(REMOVE_DEFAULT);
	run_as_root(REMOVE_DEFAULT);
	run_as_root(REMOVE_DEFAULT);

	if(global_interface_name == NULL){
		do_debug(LOG_LEVEL_LOW,"No available interfaces to the outside web. Avoiding tun!\n");
		return;	
	}

	char adt[64]; //ADD_DEFAULT_TUN
	memset(adt,0,sizeof(adt));
	sprintf(adt, "ip route add default via %s dev tun11",global_tun_ip);
	run_as_root(adt);
	//run_as_root(ADD_DEFAULT_TUN);

	pthread_mutex_lock(&xcounter);
		sprintf(command,"ip route del %s", global_worker_ip);
		run_as_root(command);
		allow_server++;
	pthread_mutex_unlock(&xcounter);

	memset(command,0,512);
	sprintf(command, "ip route add %s %s %s %s %s dev %s",global_worker_ip,
		(gotvia)?"via":"",
		(gotvia)?global_via:"",
		(gotsrc)?"src":"",
		(gotsrc)?global_src:"",
		global_interface_name);
	run_as_root(command);

	char local_network[32];

	if(gotsrc)
		strcpy(local_network, global_src);
	else {
		if(gotvia)
			strcpy(local_network, global_via);
	}

	if(gotsrc || gotvia){
		int kk;

		for(kk=strlen(local_network)-1; local_network[kk]!='.'; kk--);

		//Kind of lame that i am assuming /24 addresses but yolo
		local_network[kk+1]='0';
		local_network[kk+2]='/';
		local_network[kk+3]='2';
		local_network[kk+4]='4';
		local_network[kk+5]='\0';

		do_debug(LOG_LEVEL_LOW, "deleting local network from iptables : %s and %s", local_network, global_via);

		memset(command,0,512);
		sprintf(command, "ip route del %s", local_network);
		run_as_root(command);
		run_as_root(command);
		run_as_root(command);

		if(gotvia){
			memset(command,0,512);
			sprintf(command, "ip route del %s", global_via);
			run_as_root(command);
			run_as_root(command);
			run_as_root(command);
		}

	} else {
		do_debug(LOG_LEVEL_LOW, "There was no src or via.");
	}

	set_dns();

	notify_change(global_interface_name);

	do_debug(LOG_LEVEL_LOW,"####Finished implement_iptable_rules######\n");

}

void termination_handler(int signum) {
	char command[512];
	memset(command,0, 512);
	do_debug(LOG_LEVEL_LOW,"##########Quitting like a sir###########\n");
	
	char rdt[64]; //REMOVE_DEFAULT_TUN
	memset(rdt,0,sizeof(rdt));
	sprintf(rdt, "ip route del default via %s dev tun11",global_tun_ip);
	run_as_root(rdt);

	//run_as_root(REMOVE_DEFAULT_TUN);

	sprintf(command,"ip route del %s", global_worker_ip);
	run_as_root(command);

	memset(command,0,512);
	if(strcmp(global_via, "")==0)
		sprintf(command,"ip route add default dev %s", global_interface_name );
	else
		sprintf(command,"ip route add default via %s dev %s", global_via, global_interface_name );

	run_as_root(command);


	close(tun_client_sock_fd);
	close(service_root_sock_fd);
	exit(0);
}

//Listens to tunnel destination changes
void *event_listener() {
	struct sockaddr_in si_me, si_other;
	int s, i, slen = sizeof(si_other);
	int nbytes = 0, ret;
	char buf[ROUTE_CONTROL_SIZE];
	char interface[IFNAMSIZ + 1];
	char *buf_ptr;
	int len = 0;
	int protocol;

	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		perror("event_listener Error: socket");
	}

	bzero((char *) &si_me, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(ROUTE_CONTROL_PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	do_debug(LOG_LEVEL_LOW,
			"--->event_listener : Listening for route control messages on port %d\n",
			ROUTE_CONTROL_PORT);

	if (bind(s, (struct sockaddr *) &si_me, sizeof(si_me)) == -1)
		perror("event_listener Error: bind");

	while (1) {
		fd_set rd_set;

		FD_ZERO(&rd_set);
		FD_SET(s, &rd_set);

		//do_debug(LOG_LEVEL_LOW,"tun_client : Waiting for traffic (select)\n");
		ret = select(s + 1, &rd_set, NULL, NULL, NULL);

		if (ret < 0 && errno == EINTR) {
			continue;
		}

		if (ret < 0) {
			perror("select()");
			exit(1);
		}

		memset(buf, 0, ROUTE_CONTROL_SIZE);

		if ((nbytes = recvfrom(s, buf, ROUTE_CONTROL_SIZE, 0,
				(struct sockaddr *) &si_other, &slen)) == -1)
			perror("event_listener Error: recvfrom()");
		
		for (len = strlen(buf), i = 0; i < len; i++) {
			if (buf[i] == '|')
				buf[i] = '\0';
		}
	
		//wk->id, wk->ip, wk->traffic_port, wk->control_port, 0);
		strncpy(global_worker_ip,buf,strlen(buf));
		len = strlen(buf) + 1;

		do_debug(LOG_LEVEL_LOW, "Registered worker ip %s\n",
				global_worker_ip);

		strncpy(global_tun_ip,buf+len,strlen(buf+len));

		do_debug(LOG_LEVEL_LOW, "Assigned Tun ip %s\n",
				global_tun_ip);

		initialized_flag = 1;
	}
	close(s);
}

void notify_change(char *itface) {
	do_debug(LOG_LEVEL_LOW, "Notifying tun_client of interface change! %s\n",
			itface);
	char buf[strlen(itface) + 3 + 1 + 1];

	memset(buf, 0, strlen(itface) + 3 + 1 + 1);
	sprintf(buf, "%3d|%s", PROTO_IPCHANGE, itface);

	if (sendto(tun_client_sock_fd, buf, strlen(buf), 0, &tun_client_address,
			sizeof(tun_client_address)) == -1)
		perror("sendto()");
}

void client_notify_init() {
	int slen = sizeof(tun_client_address);
	if ((tun_client_sock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf("client_notify_init: socket creation failed");
		exit(1);
	}
	memset((char *) &tun_client_address, 0, sizeof(tun_client_address));
	tun_client_address.sin_family = AF_INET;
	tun_client_address.sin_port = htons(TUN_CLIENT_PORT);
	tun_client_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
}

void service_root_init() {
	int slen = sizeof(service_root_address);
	if ((service_root_sock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
			== -1) {
		printf("service_root_init: socket creation failed");
		exit(1);
	}
	memset((char *) &service_root_address, 0, sizeof(service_root_address));
	service_root_address.sin_family = AF_INET;
	service_root_address.sin_port = htons(SERVICE_ROOT_PORT);
	service_root_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
}

int iptable_change_listener(){
	int sock = -1;
	struct sockaddr_nl addr;
	
	//The main thread will do the detection and attempt to schedule implementation of iptables
	bzero(&addr, sizeof(addr));
	if ((sock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0)
		ERR_RET("socket");
	addr.nl_family = AF_NETLINK;
	addr.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV4_ROUTE;

	if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0)
		ERR_RET("bind");

	while(1){
		route_listener(sock, &addr);
	}

	return;
}

int main(){
	pthread_t route_tr;

	do_debug(LOG_LEVEL_LOW,"Monitor_master - Version %s\n",LOG_VERSION);

	do_debug(LOG_LEVEL_LOW, "Preparing device\n");

	//event_listener receives messages from the client regarding new workers
	if (pthread_create(&route_tr, NULL, event_listener, NULL) != 0)
		perror("error: pthread");

	//Avoids starting without a defined worker ip
	while (!initialized_flag) {
		//I know this should be a conditional wait but this only happens for the
		//first execution while waiting for the client to start
		do_debug(LOG_LEVEL_LOW, "Waiting for worker ip address\n");
		sleep(1);
	}

	//FOR DEBUG:
	//strcpy(global_worker_ip, "172.168.1.1");

	//establish sockets to send data to client and rootService 
	client_notify_init();
	service_root_init();

	run_as_root(CREATE_TUN);
	run_as_root(SET_TUN_UP);
	
	char cvn[64]; //CREATE_VIRTUAL_NETWORK
	memset(cvn,0,sizeof(cvn));
	sprintf(cvn, "ip addr add %s/24 dev tun11",global_tun_ip);
	run_as_root(cvn);
	
	//run_as_root(CREATE_VIRTUAL_NETWORK);

	//this is the only way to apply rules. better to make it unique.
	try_scheduling_new_rules();

	//catch signalling to allow reverting the iptable changes
	if (signal(SIGINT, termination_handler) == SIG_IGN)
		signal(SIGINT, SIG_IGN);
	if (signal(SIGHUP, termination_handler) == SIG_IGN)
		signal(SIGHUP, SIG_IGN);
	if (signal(SIGTERM, termination_handler) == SIG_IGN)
		signal(SIGTERM, SIG_IGN);
	
	//This is the listener for iptables. it runs eternally (ofc with select API).
	return iptable_change_listener();
}

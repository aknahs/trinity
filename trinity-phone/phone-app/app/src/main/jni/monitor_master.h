#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdarg.h>
//#include <linux/if.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "utils.h"

#define ERR_RET(x) do { perror(x); return EXIT_FAILURE; } while (0);

//size for the netlink messages
#define BUFFER_SIZE 4095 //8192
#define BOOT_SLEEP 2
//#define DEBUG 1

//Tun_client notification macros
#define TUN_CLIENT_PORT 55559

//Change tunnel route port
#define ROUTE_CONTROL_PORT 55666
#define ROUTE_CONTROL_SIZE 34

//Root runner
#define SERVICE_ROOT_PORT 55570

//maximum ip route command length
#define COMMAND_LEN 256
//ip route static commands
#define TUN_IP "10.0.1.1"
#define CREATE_TUN "openvpn --mktun --dev tun11"
//#define TUN_ADDR "ip addr add 10.0.1.1/24 dev tun11"

#define DEL_TUN_ADDR "ip addr del 10.0.1.1/24 dev tun11"
#define ADD_TUN_ADDR "ip addr add 10.0.1.1/24 dev tun11"

#define TUN_UP "ip link set tun11 up"
#define TUN_DEFAULT "ip route add default via 10.0.1.1 dev tun11"
//#define SERVER "176.34.202.100"
//#define REMOVE_SERVER "ip route del 176.34.202.100"
//#define REMOVE_TUN "ip route del dev tun11"

struct route_info {
	struct in_addr dst_addr;
	struct in_addr src_addr;
	struct in_addr gateway;
	int if_index;
};

enum routing_return {
	ALL_OK, //All routes verified
	WRONG_SERVER, //wrong server route
	WRONG_DEFAULT, //not tun
	ITS_SUNNY_OUTSIDE, //wish i had swimwear now
	UNFINISHED, //nop
};

void clean_rules();

//void do_debug(char *msg, ...);

void notify_change();

void run_as_root(char *cmd);

int readNlSock(int sockFd, char *bufPtr, size_t buf_size, int seqNum, int pId);

/* parse the route info returned */
int parseRoutes(struct nlmsghdr *nl_hdr, struct route_info *rtInfo);

int get_gateway_info(int reset,char *prefsource, char* destination, char* gateway,
		int index);

int confirm_routing_rules(int reset, char *prefsource, char* destination,
		char* gateway, int index);

int get_info(int (*f)(int,char*, char*, char*, int));

//ioctl is a synchronous run_as_root call.
void find_if_name(unsigned int index);

//ioctl is a synchronous run_as_root call. (not used!)
char * find_if_ip(unsigned int index);

void set_dns();

void set_routes(char *server, char *src, char *gateway, char *iface);

void set_virtual_network();

void set_server_route(char *server, char *src, char *gateway, char *iface);

void set_default_route();

void client_notify_init();

void service_root_init();

void route_init();

void new_interface(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out);

void del_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out);

int route_mask(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address);
void new_route(char * pref_address, char *source_address,
		char *destination_address, char *gateway_address, int iface_in,
		int iface_out);

int route_listener(int sock, struct sockaddr_nl *addr);

void termination_handler(int signum);

void *apply_route(void *arg);

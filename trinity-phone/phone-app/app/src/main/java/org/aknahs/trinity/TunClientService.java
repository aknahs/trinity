package org.aknahs.trinity;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class TunClientService extends Service{
	private final static String TAG = "TunClientService";
	private final IBinder binder = new TunClientLocalBinder();
	private boolean isRunning = false;

    public class TunClientLocalBinder extends Binder {
    	TunClientService getService() {
            return TunClientService.this;
        }
    }
    
    private void stop() {
		if (isRunning) {
			Log.d(TAG, "Got to stop()!");
			isRunning = false;
			stopForeground(true);
		}
	}

	private void run() {
		if (!isRunning) {
			Log.d(TAG, "Got to run()!");
			isRunning = true;

			startForeground(TrinityNotification.notifId, TrinityNotification.getNotification(getApplicationContext()));
		}
	}
    
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

    }
    

	public int onStartCommand(Intent intent, int flags, int startId){
		Log.d(TAG, "onStartCommand");
		run();
		/*
		 *  -START_STICKY is used for services that are 
		 *  	explicitly started and stopped as needed. 
		 *  -START_NOT_STICKY or START_REDELIVER_INTENT are
		 *  	used for services that should only remain
		 *  	running while processing any commands
		 *  	sent to them. 
		 */
		return(START_STICKY);
	}

}

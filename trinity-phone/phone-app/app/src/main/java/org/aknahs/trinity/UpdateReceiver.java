package org.aknahs.trinity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by mb on 8/05/15.
 */
public class UpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean isConnected = Services.isNetworkConnected(context);

        if (isConnected) {
            try {
                RootService.ownRef.startServices();
            }catch(InterruptedException ex){
                Log.v("Trinity: ", "Interrupted Exception occurred, whatever it means");
                ex.printStackTrace();
            }catch(NullPointerException ex){
                Log.v("Trinity: ", "Connectivity change notification, root service uninitialized");
                ex.printStackTrace();
            }
        }
        else Log.i("NET", "not connected" + isConnected);
    }

}

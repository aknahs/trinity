package org.aknahs.trinity;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.Command;


//Could use reflection to infer the _rtServ type
public class CallbackCommandCapture extends Command {
	private StringBuilder sb = new StringBuilder();
	private RootService _rtServ = null;
	private FeedbackType _feedbackType;

	//If you want to add new calls add the feedback type here
	public static enum FeedbackType {
		CLIENT, MONITOR
	}
	
	public void setCallBack(RootService rtServ, FeedbackType feedbackType) {
		_rtServ = rtServ;
		_feedbackType = feedbackType;
	}

	public CallbackCommandCapture(int id, String... command) {
		super(id, command);
	}

	@Override
	public void output(int id, String line) {
		if (_rtServ == null) {
			sb.append(line).append('\n');
			RootTools.log("Special", "ID: " + id + ", " + line);
		} else {
			if (line.length() >= 0) {
				RootTools.log("Special", "Result " + line);
				if (line.compareTo("0") == 0) {
					RootTools.log("Special", "Dead! feedbackType "
							+ _feedbackType);
					_rtServ.report(_feedbackType, false);
				} else {
					RootTools.log("Special", "Alive! feedbackType "
							+ _feedbackType);
					_rtServ.report(_feedbackType, true);
				}
			} else {
				RootTools.log("Special", "Line was empty feedbackType "
						+ _feedbackType);
				_rtServ.report(_feedbackType, false);
			}
		}
	}

	@Override
	public String toString() {
		return sb.toString();
	}
}
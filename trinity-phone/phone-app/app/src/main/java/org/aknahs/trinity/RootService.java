package org.aknahs.trinity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.aknahs.trinity.CallbackCommandCapture.FeedbackType;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;

public class RootService extends Service {
	private boolean isRunning = false;
	private final static String TAG = "MonitorService";
	private final IBinder binder = new MonitorLocalBinder();
	private volatile boolean isReady = false;

	private volatile boolean runningClient = false;
	private volatile boolean runningMonitor = false;

	private int recoveredClient = 0;
	private int recoveredMonitor = 0;

	private Handler recoverHandler = new Handler();

    public static ConnectivityCheck ck= null;

    public static RootService ownRef;
    public RootService(){
        super();
        ownRef = this;
    }
    /*
	private Runnable recover = new Runnable() {
		public void run() {
			updateStates();
		}
	};*/

	public void send_state_to_client(boolean wasScreenOn) {
		(new sendStateToClient()).execute(wasScreenOn ? "1" : "2");
	}

	public void send_state_to_client(String rule) {
		(new sendStateToClient()).execute(rule);
	}

	private Runnable recoverClient = new Runnable() {
		public void run() {
			resumeOutput("ClientError-"
					+ (new SimpleDateFormat("MM-dd-HH-mm")).format(new Date())
					+ ".txt");
			startClient();
			recoveredClient++;
		}
	};

	private Runnable recoverMonitor = new Runnable() {
		public void run() {
			startMonitor();
			recoveredMonitor++;
		}
	};

	private Runnable statusClient = new Runnable() {
		public void run() {
			clientStatus(); // This async calls report, that handles the
							// recovery
		}
	};

    /*
	public void updateStates() {
		RootTools.log("Special", "Running updateStates");

		rootStatus();

		monitorStatus(); // This async calls report, that handles the recovery

		recoverHandler.postDelayed(statusClient, 1000);

		recoverHandler.postDelayed(recover, 30000); // 3000ms delay
	}*/

	private RootRunnable rootRunnable = new RootRunnable();

	public boolean isClientRunning() {
		return runningClient;
	}

	public boolean isMonitorRunning() {
		return runningMonitor;
	}

	public boolean isRootRunning() {
		return isRunning;
	}

	private class sendStateToClient extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... arg0) {
			String state = arg0[0];
			DatagramSocket serverSocket;
			byte[] sendData;
			String msg;

			Log.d("Screen Receiver", "screen state " + state);

			try {
				serverSocket = new DatagramSocket();

				Log.d("Screen Receiver", "socket created");

				InetAddress address;

				// Write state to sendData!
				msg = state;// "003|" + state;//(state ? 1 : 2);

				sendData = msg.getBytes();

				Log.d("Screen Receiver", "sendData prepared - " + msg);

				// Get the loopback interface
				address = InetAddress.getByName("127.0.0.1");

				Log.d("Screen Receiver", "Got loopback!");

				DatagramPacket packet = new DatagramPacket(sendData,
						sendData.length, address, 55559);

				Log.d("Screen Receiver", "packet prepared!");

				serverSocket.send(packet);

				Log.d("Screen Receiver", "sendData sent!");

				serverSocket.close();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
	}

	public class RootRunnable implements Runnable {
		private DatagramSocket serverSocket;
		private volatile boolean running = false;

		@Override
		public void run() {
			Log.d(TAG, "socket thread launched");
			running = true;
			byte[] receiveData = new byte[100];

			getRawResources(R.raw.new_monitor, "new_monitor");
			getRawResources(R.raw.tun_client, "tun_client");

			try {
				serverSocket = new DatagramSocket(55570);
				Log.d(TAG, "socket created");
				for (int i = 0; i < 100; i++)
					receiveData[i] = 0;

			} catch (SocketException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				running = false;
			}

			isReady = true;

			try {

				while (running) {
					DatagramPacket receivePacket = new DatagramPacket(
							receiveData, receiveData.length);

					serverSocket.receive(receivePacket);

					String sentence = new String(receivePacket.getData());
					Log.d(TAG, "msg [" + sentence + "]");
					String len = sentence.substring(0, 3).trim();
					Log.d(TAG, "len [" + len + "]");
					String msg = sentence
							.substring(4, 4 + Integer.valueOf(len));
					Log.d(TAG, "Running: [" + msg + "]");

					CallbackCommandCapture command = new CallbackCommandCapture(
							0, msg);

					RootTools.getShell(true).add(command);
				}

				serverSocket.close();
			} catch (IOException e) {
				if (!running)
					Log.d(TAG, "Socket close");
				else
					e.printStackTrace();
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RootDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			isReady = false;
		}

		public void terminate() {
			running = false;
			serverSocket.close(); // this breaks the receive
		}

	}

	public final Object lock = new Object();
	public static Boolean isSet = false;

	public void schedulerIp(final String ip) {

		synchronized (lock) {
			isSet = false;
		}

		//Network translation cannot be achieved on Main thread
		(new Thread(new Runnable() {

			@Override
			public void run() {
				InetAddress address = null;
				CallbackCommandCapture command;

				try {
					address = InetAddress.getByName(ip);

					Log.v(TAG, "Translated ip address : " + address.getHostAddress());

					command = new CallbackCommandCapture(0, "echo \""
							+ address.getHostAddress() + "\" > "
							+ "/data/local/tmp/properties.config");

				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();

					Log.v(TAG, "Could not translate ip address : " + ip);

					command = new CallbackCommandCapture(0, "echo \"" + ip
							+ "\" > " + "/data/local/tmp/properties.config");
				}

				try {
					RootTools.getShell(true).add(command);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RootDeniedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				synchronized (lock) {
					isSet = true;
					lock.notifyAll();
				}

			}
		})).start();

		synchronized (lock) {
			try {
				if (!isSet)
					lock.wait();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void resumeOutput(String output) {
		CallbackCommandCapture command = new CallbackCommandCapture(0,
				"tail -n 30 " + Environment.getExternalStorageDirectory()
						+ "/trinity-logs/client.txt > "
						+ Environment.getExternalStorageDirectory()
						+ "/trinity-logs/" + output);

		try {
			RootTools.getShell(true).add(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (RootDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}

	public void report(FeedbackType feedbackType, boolean result) {
		switch (feedbackType) {
		case CLIENT:
			if (!result) {
				if (runningClient) {
					// Toast.makeText(getApplicationContext(),
					// "tun_client stopped!", Toast.LENGTH_SHORT).show();
					stopClient();
					// Lets make this bastard resillient
					recoverHandler.postDelayed(recoverClient, 1000); // to make
																		// sure
																		// it
																		// was
																		// stopped
				}
			}
			break;
		case MONITOR:
			if (!result) {
				// Toast.makeText(getApplicationContext(), "monitor stopped!",
				// Toast.LENGTH_SHORT).show();
				stopMonitor();
				// Lets make it resillient
				recoverHandler.postDelayed(recoverMonitor, 1000); // to make
																	// sure it
																	// was
																	// stopped
			}
			break;
		}
	}

	public int getRecoveredMonitor() {
		return recoveredMonitor;
	}

	public int getRecoveredClient() {
		return recoveredClient;
	}

	public static enum ServiceStatus {
		SERVICE_SUCCESSFUL, SERVICE_WAS_RUNNING, SERVICE_WAS_NOT_RUNNING, NO_ACCESS_GIVEN, NO_ROOT_AVAILABLE, NO_BUSYBOX_AVAILABLE, THROWED_EXCEPTION
	}

	public class MonitorLocalBinder extends Binder {
		RootService getService() {
			return RootService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public void onCreate() {
		// initialize receiver
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		BroadcastReceiver mReceiver = new ScreenReceiver(this);
		registerReceiver(mReceiver, filter);

		// NEW
		// PowerManager pm = (PowerManager)
		// getSystemService(Context.POWER_SERVICE);

		super.onCreate();
		Log.d(TAG, "onCreate");
		//recoverHandler.postDelayed(recover, 30000); // 3000ms delay
	}

	/*
	 * @Override protected void onPause() { // when the screen is about to turn
	 * off // Use the PowerManager to see if the screen is turning off if
	 * (pm.isScreenOn() == false) { // this is the case when onPause() is called
	 * by the system due to the screen turning off System.out.println(“SCREEN
	 * TURNED OFF”); } else { // this is when onPause() is called when the
	 * screen has not turned off } super.onPause(); }
	 */
	public ServiceStatus stop() {
		if (isRunning) {
			Log.d(TAG, "Got to stop()!");
			RootTools.log("OurTag", "this is a log message");
			isRunning = false;
			stopForeground(true);
			while (!isReady) {/* nop */
			}
			rootRunnable.terminate();
            stopServices();
            return ServiceStatus.SERVICE_WAS_RUNNING;
		}
		return ServiceStatus.SERVICE_WAS_NOT_RUNNING;
	}

	// getRawResources(R.raw.new_monitor,"new_monitor");
	public void getRawResources(int resource, String name) {
		Log.d(TAG, "Acessing raw resources!");
		InputStream inputStream = getResources().openRawResource(resource);

		// Create new file to copy into.
		File file = new File(Environment.getExternalStorageDirectory()
				+ java.io.File.separator + name);
		try {
			file.createNewFile();

			Log.d(TAG, "Created " + name);

			int size;
			FileOutputStream fout = new FileOutputStream(file);
			byte[] buffer = new byte[1024];
			while ((size = inputStream.read(buffer, 0, 1024)) >= 0) {
				fout.write(buffer, 0, size);
			}
			Log.d(TAG, "Rebuilding " + name);
			fout.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		CallbackCommandCapture command = new CallbackCommandCapture(0, "mkdir "
				+ Environment.getExternalStorageDirectory() + "/trinity-logs",
				"rm /data/local/tmp/" + name, "cp "
						+ Environment.getExternalStorageDirectory()
						+ java.io.File.separator + name + " /data/local/tmp/"
						+ name, "chmod 777 /data/local/tmp/" + name);

		try {
			RootTools.getShell(true).add(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RootDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean startClient() {
		if (isReady) {
			CallbackCommandCapture command = new CallbackCommandCapture(
					0,
					"rm /data/local/tmp/client.*",
					"cd /data/local/tmp/ && chmod 777 tun_client && nohup ./tun_client > client.out 2> "
							+ Environment.getExternalStorageDirectory()
							+ "/trinity-logs/client.txt < /dev/null &");

			try {
				RootTools.getShell(true).add(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (RootDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}

			runningClient = true;
			return true;
		} else
			return false;
	}

	public boolean startMonitor() {
		if (isReady) {
			CallbackCommandCapture command = new CallbackCommandCapture(
					0,
					"rm /data/local/tmp/monitor.*",
					"cd /data/local/tmp/ && chmod 777 new_monitor && nohup ./new_monitor > monitor.out 2> "
							+ Environment.getExternalStorageDirectory()
							+ "/trinity-logs/monitor.txt < /dev/null &");

			try {
				RootTools.getShell(true).add(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (RootDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			runningMonitor = true;
			return true;
		} else
			return false;
	}

	public boolean stopClient() {
		CallbackCommandCapture command = new CallbackCommandCapture(0,
				"kill $(pgrep tun_client)");

		try {
			RootTools.getShell(true).add(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (RootDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		runningClient = false;
		return true;
	}

	public boolean stopMonitor() {
		CallbackCommandCapture command = new CallbackCommandCapture(0,
				"kill $(pgrep new_monitor)");
		try {
			RootTools.getShell(true).add(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (RootDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		runningMonitor = false;
		return true;
	}

	private ServiceStatus rootStatus() {
		if (isRunning) {
			return ServiceStatus.SERVICE_WAS_RUNNING;
		} else
			return ServiceStatus.SERVICE_WAS_NOT_RUNNING;
	}

	private ServiceStatus monitorStatus() {
		if (runningMonitor) {

			CallbackCommandCapture command = new CallbackCommandCapture(0,
					"pgrep new_monitor | wc -c");

			// Tell it to call report with the following feedback type
			command.setCallBack(this, FeedbackType.MONITOR);
			try {
				RootTools.getShell(true).add(command);
			} catch (IOException e) {
				e.printStackTrace();
				return ServiceStatus.THROWED_EXCEPTION;
			} catch (TimeoutException e) {
				e.printStackTrace();
				return ServiceStatus.THROWED_EXCEPTION;
			} catch (RootDeniedException e) {
				e.printStackTrace();
				return ServiceStatus.THROWED_EXCEPTION;
			}

			// Notice that at this moment the possible death of the service is
			// not found
			// But next execution it will since report sets the running flags
			return ServiceStatus.SERVICE_WAS_RUNNING;

		} else
			return ServiceStatus.SERVICE_WAS_NOT_RUNNING;
	}

	private ServiceStatus clientStatus() {
		if (runningClient) {
			CallbackCommandCapture command = new CallbackCommandCapture(0,
					"pgrep tun_client | wc -c");

			// Tell it to call report with the following feedback type
			command.setCallBack(this, FeedbackType.CLIENT);
			try {
				RootTools.getShell(true).add(command);
			} catch (IOException e) {
				e.printStackTrace();
				return ServiceStatus.THROWED_EXCEPTION;
			} catch (TimeoutException e) {
				e.printStackTrace();
				return ServiceStatus.THROWED_EXCEPTION;
			} catch (RootDeniedException e) {
				e.printStackTrace();
				return ServiceStatus.THROWED_EXCEPTION;
			}

			// Notice that at this moment the possible death of the service is
			// not found
			// But next execution it will since report sets the running flags

			return ServiceStatus.SERVICE_WAS_RUNNING;
		} else
			return ServiceStatus.SERVICE_WAS_NOT_RUNNING;
	}

	public ServiceStatus run() throws InterruptedException, IOException,
			TimeoutException, RootDeniedException {
		if (!isRunning) {
			if (RootTools.isBusyboxAvailable()) {
				// busybox exists, do something
				if (RootTools.isRootAvailable()) {
					// su exists, do something
					if (RootTools.isAccessGiven()) {
						// your app has been granted root access

						Log.d(TAG, "Got to run()!");
						isRunning = true;

						startForeground(
								TrinityNotification.notifId,
								TrinityNotification
										.getNotification(getApplicationContext()));

						new Thread(rootRunnable).start();
                        startServices();
						return ServiceStatus.SERVICE_SUCCESSFUL;
					} else {
						return ServiceStatus.NO_ACCESS_GIVEN;
					}
				} else {
					// do something else
					return ServiceStatus.NO_ROOT_AVAILABLE;
				}
			} else {
				// do something else
				return ServiceStatus.NO_BUSYBOX_AVAILABLE;
			}
		}
		return ServiceStatus.SERVICE_WAS_RUNNING;
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand");
		RootTools.debugMode = true; // ON
		// run();
		/*
		 * -START_STICKY is used for services that are explicitly started and
		 * stopped as needed. -START_NOT_STICKY or START_REDELIVER_INTENT are
		 * used for services that should only remain running while processing
		 * any commands sent to them.
		 */
		return (START_STICKY);
	}
    Object serviceLock = new Object();

    public boolean startServices() throws InterruptedException {
        synchronized (serviceLock) {
            if (Services.isNetworkConnected(getApplicationContext()) && !areServicesRunning()
                    && isRootRunning()) {
                boolean res;
                Thread.sleep(1000);
                res = startMonitor();
                Thread.sleep(1000);
                res = res && startClient();
                Thread.sleep(500);
                if (res) {
                    if (ck != null) {
                        ck.stop();
                    }
                    ck = new ConnectivityCheck(getApplicationContext());
                    Thread th = new Thread(ck);
                    th.start();
                }
                String msg = "Launching this shit";
                Services.showNotification(getApplicationContext(), msg);
                return res;
            } else {
                if (isRootRunning()) {
                    String msg = "Oops, someone forgot to connect to internet before turning on trinity.";
                    Services.showNotification(getApplicationContext(), msg);
                }
                return false;
            }
        }
    }

    public boolean stopServices(){
        synchronized (serviceLock) {
            try {
                boolean res;
                Thread.sleep(1000);
                res = stopMonitor();
                Thread.sleep(1000);
                res = res && stopClient();
                Thread.sleep(500);
                Services.restartwifi(getApplicationContext());
                ck.stop();
                return res;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    private boolean areServicesRunning(){
        if(isClientRunning()&&isMonitorRunning()&&isRootRunning()){
            return true;
        }else{
            return false;
        }
    }

}

package org.aknahs.trinity;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;


/**
 * Created by mb on 4/05/15.
 */
public class ConnectivityCheck implements Runnable {


    private volatile boolean stop = false;
    private static Context context;
    private static final long DEFAULT_SLEEP = 10000;
    private static long sleepTime = DEFAULT_SLEEP;
    private static final long MAX_SLEEP_TIME = 600000;
    private static int disconnectCount = 0;
    private static final int MAX_COUNT = 2;
    private static boolean conFlag = true;
    private static boolean connected = true;
    public ConnectivityCheck(Context context){
        this.context = context;
    }
    @Override
    public void run() {
        while (!stop) {
            try {
                Log.v("Trinity Connection check","Sleep time "+sleepTime+"Count "+disconnectCount);
                Thread.sleep(sleepTime);
                connected = isOnline();
                Log.v("Trinity Connection check","Is online?"+connected);
                if (connected&&conFlag) {
                    disconnectCount=0;
                    if(sleepTime< MAX_SLEEP_TIME) {
                        sleepTime = 2 * sleepTime;
                    }else{
                        sleepTime = MAX_SLEEP_TIME;
                    }
                }else if(connected&&!conFlag){
                    conFlag=true;
                    disconnectCount=0;
                    sleepTime = DEFAULT_SLEEP;
                }else if(!connected&&conFlag){
                    conFlag=false;
                    disconnectCount=0;
                    sleepTime=DEFAULT_SLEEP;
                }
                else {
                    if(disconnectCount < MAX_COUNT){
                        sleepTime +=DEFAULT_SLEEP;
                        disconnectCount++;
                    }else{
                        noConnection();
                    }

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void noConnection(){
        showNotification();
        try {
            RootService.ownRef.stop();

            SharedPreferences.Editor editor = context.getSharedPreferences("pref",Context.MODE_PRIVATE).edit();
            editor.putBoolean("buttonSave", false); // value
            editor.commit();
        }catch(NullPointerException ex){
            Log.v("Trinity: ", "Root service uninitialized");
            ex.printStackTrace();
        }
    }
    public void stop(){
        stop = true;
    }

    public Boolean isOnline() {

        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 3 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;

        //return true;
    }

    @TargetApi(16)
    public static void showNotification(){

        // define sound URI, the sound to be played when there's a notification
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // intent triggered, you can add other intent for other actions
        Intent intent = new Intent(context, TrinityDeploy.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        // this is it, we'll build the notification!
        // in the addAction method, if you don't want any icon, just set the first param to 0
        Notification mNotification = new Notification.Builder(context)

                .setContentTitle("Trinity Notification!")
                .setContentText("Failed to establish tunnel. Trinity disabled")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .setSound(soundUri)

                //.addAction(R.drawable.ic_launcher, "Stop trinity", pIntent)
                //.addAction(0, "Remind", pIntent)

                .build();

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        // If you want to hide the notification after it was selected, do the code below
        // myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, mNotification);
    }
}

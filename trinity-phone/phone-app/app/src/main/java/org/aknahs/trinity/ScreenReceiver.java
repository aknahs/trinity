package org.aknahs.trinity;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScreenReceiver extends BroadcastReceiver {

	public boolean wasScreenOn = true;
	private RootService _rt;

	public ScreenReceiver(RootService rt) {
		super();
		_rt = rt;
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			// do whatever you need to do here
			wasScreenOn = false;
		} else {
			if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
				// and do whatever you need to do here
				wasScreenOn = true;
			} else
				return;
		}
		
		_rt.send_state_to_client("003|" + (wasScreenOn?"1":"2"));

	}

}

package org.aknahs.trinity;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.aknahs.trinity.RootService.MonitorLocalBinder;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;

public class TrinityDeploy extends Activity implements OnClickListener {

    private RootService rootService;
    private boolean bound;
    private Context context;
    boolean appState = false;
    SharedPreferences preferences;
    ToggleButton tgclient;
    ToggleButton tgmonitor;
    ToggleButton tgroot;
    ToggleButton tgrrc;
    Button butLog;
    ToggleButton butSave;
    Button butSaveRule;
    EditText schedIp;
    EditText ruleText;
    ImageView icon;

    private Handler updateHandler;

    private Runnable updater = new Runnable() {
        public void run() {
            updateStates();
        }
    };

    @Override
    public void onResume(){
        super.onResume();
        SharedPreferences preferences = getSharedPreferences("pref", MODE_PRIVATE);
        butSave.setChecked(preferences.getBoolean("buttonSave",false));
        if(preferences.getBoolean("buttonSave",false))
            icon.setImageResource(R.drawable.ic_launcher);
        else
            icon.setImageResource(R.drawable.ic_launcher_deactive);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_new);
        preferences = getSharedPreferences("pref",MODE_PRIVATE);
        // if the phone reboots while trinity was on
        if(RootService.ownRef==null){
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("buttonSave", false); // value
            editor.commit();
        }

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tgclient = (ToggleButton) findViewById(R.id.toggleClient);
        tgmonitor = (ToggleButton) findViewById(R.id.toggleMonitor);
        tgroot = (ToggleButton) findViewById(R.id.toggleRootService);
        butLog = (Button) findViewById(R.id.butLog);
        butSave = (ToggleButton) findViewById(R.id.saveButton);
        butSaveRule = (Button) findViewById(R.id.ruleButton);
        tgrrc = (ToggleButton) findViewById(R.id.toggleButton_RRC);
        schedIp = (EditText) findViewById(R.id.schedulerip);
        ruleText = (EditText) findViewById(R.id.rule);
        icon = (ImageView) findViewById(R.id.imageView1);

        tgclient.setOnClickListener(this);
        tgmonitor.setOnClickListener(this);
        tgroot.setOnClickListener(this);
        butLog.setOnClickListener(this);
        tgrrc.setOnClickListener(this);
        butSave.setOnClickListener(this);
        butSaveRule.setOnClickListener(this);

        tgclient.setChecked(preferences.getBoolean("tgclient", false));
        tgmonitor.setChecked(preferences.getBoolean("tgmonitor", false));
        tgroot.setChecked(preferences.getBoolean("tgroot", false));
        butSave.setChecked(preferences.getBoolean("buttonSave",false));
        if(preferences.getBoolean("savedip", false)){
            //tgclient.setVisibility(View.VISIBLE);
            //tgmonitor.setVisibility(View.VISIBLE);
            //tgroot.setVisibility(View.VISIBLE);
            //butLog.setVisibility(View.VISIBLE);
            //tgrrc.setVisibility(View.VISIBLE);
        }
        else{
            tgclient.setVisibility(View.INVISIBLE);
            tgmonitor.setVisibility(View.INVISIBLE);
            tgroot.setVisibility(View.INVISIBLE);
            butLog.setVisibility(View.INVISIBLE);
            tgrrc.setVisibility(View.INVISIBLE);
        }

        Intent serviceIntent = new Intent(getApplicationContext(),
                RootService.class);
        startService(serviceIntent);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);

        context = getApplicationContext();
        updateHandler = new Handler();
    }

    private boolean runClient() {
        if (bound) {
            if (rootService.startClient()) {
                Toast.makeText(context, "Full fleet launched!",
                        Toast.LENGTH_SHORT).show();

                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("tgclient", tgclient.isChecked()); // value to
                // store
                editor.commit();

                return true;
            }
        }
        Toast.makeText(context, "Not bound.", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean runMonitor() {
        if (bound) {
            if (rootService.startMonitor()) {
                tgclient.setEnabled(true);

                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("tgmonitor", tgmonitor.isChecked()); // value
                // to
                // store
                editor.commit();

                return true;
            } else
                return false;
        }
        Toast.makeText(context, "Not bound.", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean startRootService() {
        Context context = getApplicationContext();
        CharSequence text = "";
        boolean printMessage = true;
        boolean ret = false;

        if (bound) {
            RootService.ServiceStatus status;
            try {
                status = rootService.run();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            } catch (TimeoutException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            } catch (RootDeniedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            }

            switch (status) {
                case NO_ACCESS_GIVEN:
                    text = new String("Couldn't grant root access!");
                    break;

                case NO_BUSYBOX_AVAILABLE:
                    text = new String("Please install busy box!");
                    break;

                case NO_ROOT_AVAILABLE:
                    text = new String("Please install superuser!");
                    break;

                case SERVICE_SUCCESSFUL:
                    printMessage = false;
                    tgmonitor.setEnabled(true);
                    ret = true;
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("tgroot", tgmonitor.isChecked()); // value to
                    // store
                    editor.commit();
                    break;

                case SERVICE_WAS_RUNNING:
                    text = new String("Service was already running!");
                    ret = true;
                    break;
                default:
                    text = new String("Throwed Exception!");
                    break;
            }

        } else
            text = new String("Binding to Service!");

        if (printMessage)
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();

        return ret;
    }

    private boolean stopClient() {
        if (bound) {
            return rootService.stopClient();
        }
        Toast.makeText(context, "Not bound.", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean stopMonitor() {
        if (bound) {
            stopClient();
            tgclient.setEnabled(false);
            tgclient.setChecked(false);
            return rootService.stopMonitor();
        }
        Toast.makeText(context, "Not bound.", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean stopRootService() {
        CharSequence text = "";
        boolean ret = false;

        if (bound) {
            stopClient();
            stopMonitor();

            tgmonitor.setEnabled(false);
            tgmonitor.setChecked(false);

            tgclient.setEnabled(false);
            tgclient.setChecked(false);
            RootService.ServiceStatus status = rootService.stop();

            switch (status) {
                case SERVICE_WAS_RUNNING:
                    text = new String("Service stopped!");
                    ret = true;
                    break;
                case SERVICE_WAS_NOT_RUNNING:
                    text = new String("Service was not running!");
                    ret = false;
                    break;
                default:
                    break;
            }

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("savedip", false); // value to										// store
            editor.commit();

        } else
            text = new String("Binding to Service!");

        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        return ret;
    }

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        boolean res = true;

        switch (v.getId()) {

            case R.id.toggleButton_RRC:
                if (tgrrc.isChecked()) {
                    rrcCommunicatorStart();
                } else {
                    rrcCommunicatorStop();
                }
                break;

            case R.id.toggleMonitor:
                if (tgmonitor.isChecked()) {
                    res = runMonitor();
                } else {
                    res = stopMonitor();
                }
                if (!bound || !res)
                    tgmonitor.toggle();
                break;

            case R.id.saveButton:
                if(writeToProperties()&& butSave.isChecked()){
                    /*
                    tgclient.setVisibility(View.VISIBLE);
                    tgmonitor.setVisibility(View.VISIBLE);
                    tgroot.setVisibility(View.VISIBLE);*/
                   // butLog.setVisibility(View.VISIBLE);
                    //tgrrc.setVisibility(View.VISIBLE);
                    //butSave.setVisibility(View.VISIBLE);
                    SharedPreferences.Editor editor = preferences.edit();
                    //Disabled other buttons
                    editor.putBoolean("savedip", true); // value to										// stor
                    editor.commit();

                    //Extra
                    res = startRootService();
                    butLog.setVisibility(View.VISIBLE);
                    icon.setImageResource(R.drawable.ic_launcher);
                    appState = true;
                }else if(!butSave.isChecked()){

                    try {
                        res=stopRootService();
                        Thread.sleep(1000);
                        butLog.setVisibility(View.INVISIBLE);
                        icon.setImageResource(R.drawable.ic_launcher_deactive);
                        appState=false;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                if (!bound || !res) {
                    butSave.toggle();
                }
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("buttonSave",butSave.isChecked());
                editor.commit();
                break;

            case R.id.ruleButton:
                if(saveRule()){
                    Toast.makeText(context, "Rule saved!", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(context, "Could not save!", Toast.LENGTH_SHORT).show();

                break;

            case R.id.toggleClient:
                if (tgclient.isChecked()) {
                    res = runClient();
                } else {
                    res = stopClient();
                }
                if (!bound || !res)
                    tgclient.toggle();
                break;

            case R.id.toggleRootService:
                if (tgroot.isChecked()) {
                    res = startRootService();
                } else {
                    res = stopRootService();
                }
                if (!bound || !res)
                    tgroot.toggle();
                break;

            case R.id.butLog:
                if (bound) {
                    rootService.resumeOutput("client_tail.txt"); // tail -n

                    // too make sure the resume output executes before
                    updateHandler.postDelayed(new Runnable() {

                        public void run() {
                            Intent intent = new Intent(Intent.ACTION_EDIT);
                            RootTools.log(
                                    "Special",
                                    "file:///"
                                            + Environment
                                            .getExternalStorageDirectory()
                                            + "/client_tail.txt");
                            Uri uri = Uri.parse("file:///"
                                    + Environment.getExternalStorageDirectory()
                                    + "/trinity-logs/client_tail.txt");
                            intent.setDataAndType(uri, "text/plain");
                            startActivity(intent);
                        }

                    }, 1000); // 1000ms delay

                    Toast.makeText(
                            context,
                            "Recovered: c = " + rootService.getRecoveredClient()
                                    + " m = " + rootService.getRecoveredMonitor(),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    private boolean writeToProperties(){
        if (bound) {
            rootService.schedulerIp(schedIp.getText().toString());
            Toast.makeText(context, "Saved : " + schedIp.getText().toString(), Toast.LENGTH_SHORT).show();
            return true;
        }
        else
            Toast.makeText(context, "Not bound.", Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean saveRule(){
        if(bound){
            String rule = ruleText.getText().toString();
            rootService.send_state_to_client("005|" + rule);
            return true;
        }
        return false;
    }

    public void rrcCommunicatorStart() {
        (new rrcCommunicator()).execute(true);
    }

    public void rrcCommunicatorStop() {
        (new rrcCommunicator()).execute(false);
    }

    private class rrcCommunicator extends AsyncTask<Boolean, Void, Void> {

        @Override
        protected Void doInBackground(Boolean... arg0) {
            boolean state = arg0[0];
            DatagramSocket serverSocket;
            byte[] sendData;
            String msg = state ? "1" : "0";

            RootTools.log("Special", "Sending to SamsungServiceMode : " + msg);
            try {
                serverSocket = new DatagramSocket();
                InetAddress address;
                sendData = msg.getBytes();

                // Get the loopback interface
                address = InetAddress.getByName("127.0.0.1");

                DatagramPacket packet = new DatagramPacket(sendData,
                        sendData.length, address, 9940);

                serverSocket.send(packet);
                serverSocket.close();

            } catch (SocketException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }
    }

    public void updateStates() {
        RootTools.log("Special", "Running updateStates");

        if (rootService.isRootRunning()) {
            tgroot.setChecked(true);
            tgmonitor.setEnabled(true);
            updateHandler.postDelayed(updater, 30000); // 3000ms delay
        } else {
            tgroot.setChecked(false);

            tgmonitor.setEnabled(false);
            tgmonitor.setChecked(false);

            tgclient.setEnabled(false);
            tgclient.setChecked(false);
        }
        if (rootService.isMonitorRunning()) {
            tgmonitor.setChecked(true);

            tgclient.setEnabled(true);
            tgclient.setChecked(false);
        } else {
            tgmonitor.setChecked(false);

            tgclient.setEnabled(false);
            tgclient.setChecked(false);
        }
        if (rootService.isClientRunning()) {
            tgclient.setChecked(true);
        } else
            tgclient.setChecked(false);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            updateHandler.removeCallbacks(updater); // removes post delayeds
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ToggleButton t;
            MonitorLocalBinder binder = (MonitorLocalBinder) service;
            rootService = binder.getService();
            bound = true;

            updateStates();
        }
    };

    private ServiceConnection connectionCheck = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

        }
    };

}
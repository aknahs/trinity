
package org.aknahs.trinity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import org.aknahs.trinity.R;

public class TrinityNotification {

    public static int notifId = 654654;

    public static Notification getNotification(Context context) {

        Notification n = new Notification(
                R.drawable.ic_launcher,
                "Trinity Service!",
                System.currentTimeMillis());

        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                new Intent(context, Trinity.class),
                0);
        n.setLatestEventInfo(context, "Trinity", "All your bases are belong to us!", pendingIntent);

        return n;
    }

    public static void cancel(Context context) {
        NotificationManager nm =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        nm.cancel(notifId);
    }

}

package com.cyanogenmod.samsungservicemode;

//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.DialogInterface;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.telephony.gsm.GsmCellLocation;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.EditText;
//import android.widget.ListView;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;


// AA535 modified imports for the service
import android.app.Service;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.os.AsyncTask;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.util.Arrays;
import java.lang.String;
import java.io.*;
import java.net.*;
import java.net.UnknownHostException;

//Logging
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.BufferedWriter;


// Change signature to Service
public class SamsungServiceModeActivity extends Service {

    private static final String TAG = "SPC";
    private static String PREV_RIL_STATE = "-1";
    private static String prevReport = "-1";
    private static boolean PREV_SCREEN_STATE = false;

    //Logging - does not deppend on logger.c 
    private PrintWriter logWriter = null;
    private static String logfile = "/sdcard/ril_log.txt";


    public static final String EXTRA_SECRET_CODE = "secret_code";

    private static final int ID_SERVICE_MODE_REFRESH = 1001;
    private static final int ID_SERVICE_MODE_REQUEST = 1008;
    private static final int ID_SERVICE_MODE_END = 1009;

    private static final int DIALOG_INPUT = 0;

    private static final int CHARS_PER_LINE = 34;
    private static final int LINES = 16; // 11 on gb and ics, 16 on jb

    private String[] mDisplay = new String[LINES];

    private int mCurrentSvcMode;
    private int mCurrentModeType;

    private boolean poolingRRC = false;

    // AA535: 3 lines, Narseo's changes
    private boolean DEBUG = true;	
    //Used to get type of network connectivity (GSM/3GPP Standard)
    private TelephonyManager mTelephonyManager;

    // Disable back when initialized with certain commands due to crash
    private boolean mAllowBack;
    private boolean mFirstRun = true;
    private String mFirstPageHead;


    // AA535: the next 145 lines custom
    private final IBinder mBinder = new MyBinder();

    // Used to communicate with the logger
    private DatagramSocket loggerClientSocket;
    private byte[] loggerBuffer = new byte[1024];
    private InetAddress loggerIPAddress;
    private static final int LOGGER_UDP_PORT = 9930;


    /*
     * Broadcast receiver for a 1sec granularity info about the state of the screen
     * 
     * Instead of having events, we will consider that with the RNC states
     * and will be sent together to the logger
     */
    public class ScreenReceiver extends BroadcastReceiver { 

        public boolean wasScreenOn = true;

        @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    wasScreenOn = false;
                } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                    wasScreenOn = true;
                }
            }

        public int getScreenState(){
            if (wasScreenOn){
                return 1;
            }
            return 0;
        }
    }

    private ScreenReceiver mScreenReceiver = null;
    private static boolean radioIsOff = false;
    private static String prevRRC = "INITIAL";
    /*
     * Sends UDP socket to logger server on a given port defined by LOGGER_UDP_PORT
     */
    private class RncLogger extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... messages) {
            String data = messages[0];
            try{		
                loggerBuffer = data.getBytes();
                Log.d(TAG,"RRC Sending...");
                DatagramPacket sendPacket = new DatagramPacket(loggerBuffer, loggerBuffer.length, loggerIPAddress, LOGGER_UDP_PORT);
                loggerClientSocket.send(sendPacket);
                Log.d(TAG,"RRC Message sent!");
                return "Success";
            }
            catch(IOException e){
                String msg = "Error IOException sending log event: "+data; 
                Log.e(TAG, msg, e);
                return msg;
            }
            catch(Exception e){
                String msg = "Error sending log event: "+data;
                Log.e(TAG, msg, e);
                return msg;
            }
        }
        @Override
        protected void onPostExecute(String result) {
            return;
        }
    }

    /*
     * Logger. writing on file directly. 
     */
    private class RncLoggerFile extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... messages) {
            String data = messages[0];		
            if(logWriter == null) {
              if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                try {
                  logWriter = new PrintWriter(new BufferedWriter(new FileWriter(logfile, true)), true);
                } 
                catch(final Exception e) {
                  Log.e(TAG, "Exception opening logfile [" + logfile +"]", e);
                  return "Exception opening logfile [" + logfile +"]";
                }
              } else {
                Log.e(TAG, "External storage " + logfile + " not available");
              }
            }
            // log entry to logfile
            if(logWriter != null) {
              logWriter.print(data);
              logWriter.flush();
              return "Success";
            }
            return "Failure";
        }
        @Override
        protected void onPostExecute(String result) {
            return;
        }
    }

    public class RRCController implements Runnable {
        private DatagramSocket serverSocket;
        private volatile boolean running = false;

        @Override
        public void run() {
            Log.d(TAG, "RRCController : socket thread launched");
            running = true;
            byte[] receiveData = new byte[100];

            try {
                serverSocket = new DatagramSocket(9940);
                Log.d(TAG, "RRCController :socket created");
                for (int i = 0; i < 100; i++)
                    receiveData[i] = 0;

            } catch (SocketException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                running = false;
            }

            try {

                while (running) {

                    DatagramPacket receivePacket = new DatagramPacket(
                            receiveData, receiveData.length);

                    Log.d(TAG, "RRCController : waiting for message");
                    serverSocket.receive(receivePacket);

                    String sentence = new String(receivePacket.getData());
                    Log.d(TAG, "RRCController : message - "+sentence);

                    if(sentence.charAt(0) == '1'){
                        Log.d(TAG, "RRCController : starting service");
                        completeStart();
                    }
                    else{
                        Log.d(TAG, "RRCController : stopping service");
                        endServiceMode();
                    }
                }

                serverSocket.close();
            } catch (IOException e) {
                if (!running)
                    Log.d(TAG, "Socket close");
                else
                    e.printStackTrace();
            }
        }

        public void terminate() {
            running = false;
            serverSocket.close(); // this breaks the receive
        }

    }


    private Phone mPhone;
    private Handler mHandler = new Handler() {

        @Override
            public void handleMessage(Message msg) {
                if (DEBUG) Log.i(TAG, "HANDLE MESSAGE: "+msg.what);

                int modeType = OemCommands.OEM_SM_TYPE_MONITOR;
                int subType = OemCommands.OEM_SM_TYPE_SUB_ENTER;
                byte[] dataToSend = OemCommands.getEnterServiceModeData(modeType, subType, OemCommands.OEM_SM_ACTION);


                switch(msg.what) {
                    case ID_SERVICE_MODE_REFRESH:
                        // AA535 the whole case custom
                        // mCurrentSvcMode forced to b OEM_SM_ENTER_MODE_MESSAGE (but should be this anyway)
                        // want to keep polling the same "menu", so don't need the other cases
                        Log.i(TAG, "Type: ID_SERVICE_MODE_REFRESH. Maybe no network. Wait until there's network");
                        mCurrentSvcMode = OemCommands.OEM_SM_ENTER_MODE_MESSAGE;
                        mCurrentModeType = modeType;
                        if (isAirplaneModeOn()){
                            Log.i(TAG, "*****AIRPLANE MODE*****\tDelayed Message");
                            mHandler.sendEmptyMessageDelayed(ID_SERVICE_MODE_REFRESH, 30000);
                        }								
                        else{	
                            sendRequest(dataToSend, ID_SERVICE_MODE_REQUEST);
                        }
                        break;
                        //Log.v(TAG, "Tick");
                        //byte[] data = null;
                        //switch(mCurrentSvcMode) {
                        //    case OemCommands.OEM_SM_ENTER_MODE_MESSAGE:
                        //        data = OemCommands.getEnterServiceModeData(0, 0, OemCommands.OEM_SM_QUERY);
                        //        break;
                        //    case OemCommands.OEM_SM_PROCESS_KEY_MESSAGE:
                        //        data = OemCommands.getPressKeyData('\0', OemCommands.OEM_SM_QUERY);
                        //        break;
                        //    default:
                        //        Log.e(TAG, "Unknown mode: " + mCurrentSvcMode);
                        //        break;
                        //}

                        //if (data != null) {
                        //    sendRequest(data, ID_SERVICE_MODE_REQUEST);
                        //}
                        //break;
                    case ID_SERVICE_MODE_REQUEST:
                        // AA535 wrap async result retrieval in a try-catch block, exception thrown when in Airplane mode
                        AsyncResult result;
                        try {
                            result = (AsyncResult)msg.obj;
                        } catch (Exception e) {
						    Log.e(TAG, "Something bad happened when getting async result: "+e.getMessage());
						    Log.i(TAG, "*****ERROR*****\tDelayed Message");
						    mHandler.sendEmptyMessageDelayed(ID_SERVICE_MODE_REFRESH, 10000);
                            break;
                        }
                        if (result.exception != null) {
                            // AA535 the whole clause different - if exception in async result, retry after 30s
                            Log.e(TAG, "Excepction in result", result.exception);
							Log.i(TAG, "*****Delayed Message");
							mHandler.sendEmptyMessageDelayed(ID_SERVICE_MODE_REFRESH, 30000);		
                            break;
                            //return;
                        }
                        if (result.result == null) {
                            Log.v(TAG, "No need to refresh.");
                            return;
                        }
                        byte[] aob = (byte[])result.result;

                        if (aob.length == 0) {
                            Log.v(TAG, "Length = 0");
                            return;
                        }

                        int lines = aob.length / CHARS_PER_LINE;
                        if (lines > LINES) {
                            Log.e(TAG, "Datasize " + aob.length + " larger than expected");
                            return;
                        }

                        // AA535 NV's default settings
                        //Send the logger the data: RNC State, HSPA Channels, RSCP data
						String RRC = "-1"; //Default in case there's an error
						String HSPA = "-1";
						String RSCP = "-1,-1,-1";
						//Kind of slow but that's all that can be done
                        for (int i = 0; i < lines; i++) {
                            StringBuilder strb = new StringBuilder(CHARS_PER_LINE);
                            for (int j = 2; i < CHARS_PER_LINE; j++) {
                                int pos = i * CHARS_PER_LINE + j;
                                if (pos >= aob.length) {
                                    Log.e(TAG, "Unexpected EOF");
                                    break;
                                }
                                if (aob[pos] == 0) {
                                    break;
                                }
                                strb.append((char)aob[pos]);
                            }
                            mDisplay[i] = strb.toString();

                            // AA535 the rest of the statements inside the for loop custom
                            //TODO: Need to parse. Try to get RRC state and HSPA INFO
                            //Removed strb.toString().contains("LAC") ||
                            try{
                                String currentLine = strb.toString();
                                if (currentLine.contains("RRC")){
                                    String []  items = currentLine.split(": ");
                                    RRC = items[1];
                                    if (DEBUG) Log.i(TAG, "RRC,"+RRC);
                                }
                                else if(strb.toString().contains("HSPA")){
                                    String []  items = currentLine.split(": ");
                                    HSPA = items[1];
                                    if (DEBUG) Log.i(TAG, "HSPA,"+HSPA);							
                                }
                                else if (strb.toString().contains("RSCP")){
                                    //if (DEBUG) Log.i(TAG, timestamp +":"+ strb.toString());
                                    String []  items = currentLine.split(":");
                                    RSCP = items[1].substring(0,3)+","+items[2].substring(0,3)+","+items[3].substring(0,2);
                                    if (DEBUG) Log.i(TAG, "RSCP,"+RSCP);	
                                }
                            }
                            catch (Exception e){
                                Log.e(TAG, "Error parsing RNC states: "+e.getMessage());
                            }
                        }
                        
		                    //String [] gsmLoc = getLocationGSM();
		                    //Log.e(TAG, "CID/LAC/MCC/MNC : "+gsmLoc[0]+"/"+gsmLoc[1]+"/"+gsmLoc[2]+"/"+gsmLoc[3]);


                        // AA535 custom debug code to log RRC states
                        //if (DEBUG) Log.i(TAG, "Type of 3GPP Network: "+mTelephonyManager.getNetworkType());
                        //if (DEBUG) Log.i(TAG,"RADIO,"+RRC+","+HSPA+","+RSCP+". Screen ON = "+mScreenReceiver.getScreenState()+"\n");
                        //Radio: RRC State, HSPA Channels, Current RSCP, Average RSCP, ECIO
                        if (RRC.equals("-1") && RRC.equals(prevRRC)){
                            radioIsOff = true;
                        }	
                        else{
                            //Only print positive events. If two RRC values are "-1"
                            //only the first one is printed,
                            radioIsOff = false;
                            //String currentReport = Long.toString(System.currentTimeMillis())+",RADIO,"+mScreenReceiver.getScreenState()+","
                            //+mTelephonyManager.getNetworkType()
                            //+","+RRC+","+HSPA+","+RSCP+","+gsmLoc[0]+","+gsmLoc[1]+","+gsmLoc[2]+","+gsmLoc[3]+"\n";
                            String currentReport = "" + RRC;
                            //if (currentReport.equals(prevReport)==false){
                                //Only report radio event if the current report differs from the
                                //prev one to reduce writing too much on sd card
                                //new RncLoggerFile().execute(currentReport);
                                new RncLogger().execute(currentReport);
                                //prevReport = currentReport;
                            //}
                        }
                        prevRRC = RRC;

                        //mHandler.sendEmptyMessageDelayed(ID_SERVICE_MODE_REFRESH, 200);

                        if(poolingRRC){
                            mCurrentSvcMode = OemCommands.OEM_SM_ENTER_MODE_MESSAGE;
                            mCurrentModeType = modeType;	
                            if (isAirplaneModeOn()){
                                Log.i(TAG, "*****AIRPLANE MODE*****\tDelayed Message");
                                mHandler.sendEmptyMessageDelayed(ID_SERVICE_MODE_REFRESH, 30000);
                            }								
                            else{	
                                sendRequest(dataToSend, ID_SERVICE_MODE_REQUEST);
                            }
                        }

                        // AA535 original code removed - no support for menu at the moment, removing drawing/refresh

                        //mListView.setAdapter(new ArrayAdapter<String>(
                        //            SamsungServiceModeActivity.this, R.layout.list_item, mDisplay));

                        //if (mFirstRun) {
                        //    mFirstPageHead = mDisplay[0];
                        //    mFirstRun = false;
                        //}

                        //if (mDisplay[0].contains("End service mode")) {
                        //    finish();
                        //} else if (((mDisplay[0].contains("[")) && (mDisplay[0].contains("]")))
                        //        || ((mDisplay[1].contains("[")) && (mDisplay[1].contains("]")))) {
                        //    // This is a menu, don't refresh
                        //} else if ((mDisplay[0].length() != 0) && (mDisplay[1].length() == 0)
                        //        && (mDisplay[0].charAt(1) > 48) && (mDisplay[0].charAt(1) < 58)) {
                        //    // Only numerical display, refresh
                        //    mHandler.sendEmptyMessageDelayed(ID_SERVICE_MODE_REFRESH, 200);
                        //} else {
                        //    // Periodical refresh
                        //    mHandler.sendEmptyMessageDelayed(ID_SERVICE_MODE_REFRESH, 1500);
                        //}
                        break;
                    case ID_SERVICE_MODE_END:
                        Log.v(TAG, "Service Mode End");
                        break;
                }
            }

    };

    /**
     * Gets the state of Airplane Mode.Used to avoid polling the system if it's OFF 	
     * Otherwise, it kills the thread and it's not recovered unless the device
     * is rebooted
     * 
     * @param context
     * @return true if enabled.
     */
    private boolean isAirplaneModeOn() {
        return Settings.System.getInt(this.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;
    }


    /*
     *	Binds the background service
     *  
     *  SamsungServiceMode turned into Service from Activity to be always running
     */
    public IBinder onBind(Intent arg0) {
        if (DEBUG) Log.i(TAG, "onBind");
        return mBinder;
    }

    public class MyBinder extends Binder {
        SamsungServiceModeActivity getService() {
            if (DEBUG) Log.i(TAG, "MyBinder.getService()");
            return SamsungServiceModeActivity.this;
        }
    }


	public void initialize(){
		if (DEBUG) Log.i(TAG, "initialize(). Nothing to do");
	}
    /**
     * Required for service implementation.
     * Starts the monitoring 
     */
    @Override
    public void onStart(Intent intent, int startId) {
        if (DEBUG) Log.i(TAG, "onStart()");
        //int modeType = OemCommands.OEM_SM_TYPE_MONITOR;
        //int subType = OemCommands.OEM_SM_TYPE_SUB_ENTER;		
        //enterServiceMode(modeType, subType);
    }

    /** Called when the service is first created. */
    @Override
    public void onCreate() {
        //AA535 no activity - no view
        //super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);

        //mInputText = new EditText(this);
        //mListView = (ListView)findViewById(R.id.displayList);
        //mListView.setOnItemClickListener(this);

        mPhone = PhoneFactory.getDefaultPhone();

        // AA535: enxt 15 lines custom
        Log.i(TAG, "Getting telephony manager to get type of net");
        mTelephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

        if(mTelephonyManager == null)
            Log.d(TAG, "Could not retrieve the telephonyManager");
        try {
            loggerClientSocket = new DatagramSocket();
        } catch (Exception e) {
            Log.e(TAG, "ERROR Creating socket for logging: ", e);
        }

        try{
            loggerIPAddress = InetAddress.getLocalHost();
        }
        catch(UnknownHostException e){
            Log.e(TAG, "ERROR getting localhost address for logging: UnknownHostEception ", e);
        }
	catch (Exception e) {
            Log.e(TAG, "ERROR getting localhost address for logging: ", e);
            try {
                loggerIPAddress = InetAddress.getByName("127.0.0.1");
            } catch (Exception e2) {
                Log.e(TAG, "ERROR retrying getting localhost address for logging: ", e2);
            }
	}

    RRCController controller = new RRCController();
    new Thread(controller).start();

        //Register intent filter to get screen events
        //mScreenReceiver = new ScreenReceiver();
        //IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        //filter.addAction(Intent.ACTION_SCREEN_OFF);
        //registerReceiver(mScreenReceiver, filter);

        // AA535 the reset removed because not activity anymore

        //// Go to the page specified by the code used to enter service mode
        //String code = getIntent().getStringExtra(EXTRA_SECRET_CODE);

        //// Default to main page
        //int modeType = OemCommands.OEM_SM_TYPE_TEST_MANUAL;
        //int subType = OemCommands.OEM_SM_TYPE_SUB_ENTER;
        //mAllowBack = true; // Some commands don't like having "back" executed on them

        //if (TextUtils.isEmpty(code) || code.equals("197328640")) {
        //    // Use default (this exists to prevent NPE when code is null)
        //}
        //else if (code.equals("0011")) {
        //    modeType = OemCommands.OEM_SM_TYPE_MONITOR;
        //    subType = OemCommands.OEM_SM_TYPE_SUB_ENTER;
        //} else if (code.equals("0228")) { // 0BAT
        //    subType = OemCommands.OEM_SM_TYPE_SUB_BATTERY_INFO_ENTER;
        //} else if (code.equals("32489")) {
        //    subType = OemCommands.OEM_SM_TYPE_SUB_CIPHERING_PROTECTION_ENTER;
        //} else if (code.equals("2580")) { // ALT0
        //    subType = OemCommands.OEM_SM_TYPE_SUB_INTEGRITY_PROTECTION_ENTER;
        //} else if (code.equals("9090") || code.equals("7284")) { // PATH
        //    subType = OemCommands.OEM_SM_TYPE_SUB_USB_UART_DIAG_CONTROL_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("0599") || code.equals("301279") || code.equals("279301")) {
        //    subType = OemCommands.OEM_SM_TYPE_SUB_RRC_VERSION_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("2263")) { // BAND
        //    subType = OemCommands.OEM_SM_TYPE_SUB_BAND_SEL_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("4238378")) { // GCFTEST
        //    subType = OemCommands.OEM_SM_TYPE_SUB_GCF_TESTMODE_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("0283")) { // 0AUD
        //    subType = OemCommands.OEM_SM_TYPE_SUB_GSM_FACTORY_AUDIO_LB_ENTER;
        //} else if (code.equals("1575")) {
        //    subType = OemCommands.OEM_SM_TYPE_SUB_GPSONE_SS_TEST_ENTER;
        //} else if (code.equals("73876766")) { // SETSMSON
        //    subType = OemCommands.OEM_SM_TYPE_SUB_SELLOUT_SMS_ENABLE_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("738767633")) { // SETSMSOFF
        //    subType = OemCommands.OEM_SM_TYPE_SUB_SELLOUT_SMS_DISABLE_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("7387678378")) { // SETSMSTEST
        //    subType = OemCommands.OEM_SM_TYPE_SUB_SELLOUT_SMS_TEST_MODE_ON;
        //    mAllowBack = false;
        //} else if (code.equals("7387677763")) { // SETSMSPROD
        //    subType = OemCommands.OEM_SM_TYPE_SUB_SELLOUT_SMS_PRODUCT_MODE_ON;
        //    mAllowBack = false;
        //} else if (code.equals("4387264636")) {
        //    subType = OemCommands.OEM_SM_TYPE_SUB_GET_SELLOUT_SMS_INFO_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("6984125*") || code.equals("2886")) { // AUTO
        //    // crash
        //    subType = OemCommands.OEM_SM_TYPE_SUB_TST_AUTO_ANSWER_ENTER;
        //} else if (code.equals("2767*2878")) {
        //    // crash
        //    subType = OemCommands.OEM_SM_TYPE_SUB_TST_NV_RESET_ENTER;
        //} else if (code.equals("1111")) {
        //    subType = OemCommands.OEM_SM_TYPE_SUB_TST_FTA_SW_VERSION_ENTER;
        //    mAllowBack = false;
        //} else if (code.equals("2222")) {
        //    subType = OemCommands.OEM_SM_TYPE_SUB_TST_FTA_HW_VERSION_ENTER;
        //    mAllowBack = false;
        //}

        //enterServiceMode(modeType, subType);
    }

    // AA535 does not apply for service
    //@Override
    //protected Dialog onCreateDialog(int id) {
    //    switch (id) {
    //    case DIALOG_INPUT:
    //        return new AlertDialog.Builder(this)
    //        .setTitle(R.string.input)
    //        .setView(mInputText)
    //        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
    //            public void onClick(DialogInterface dialog, int whichButton) {
    //                sendString(mInputText.getText().toString());
    //                mInputText.setText("");
    //            }
    //        })
    //        .setNegativeButton(android.R.string.cancel, null)
    //        .create();
    //    }
    //    return null;
    //}

    //@Override
    //public boolean onCreateOptionsMenu(Menu menu) {
    //    MenuInflater inflater = getMenuInflater();
    //    inflater.inflate(R.menu.menu, menu);
    //    return true;
    //}

    //@Override
    //public boolean onOptionsItemSelected(MenuItem item) {
    //    switch (item.getItemId()) {
    //    case R.id.menu_input:
    //        showDialog(DIALOG_INPUT);
    //        break;
    //    case R.id.menu_quit:
    //        endServiceMode();
    //        break;
    //    }
    //    return true;
    //}

    //@Override
    //public void onBackPressed() {
    //    if (!mAllowBack && mDisplay[0].equals(mFirstPageHead)) {
    //        Log.v(TAG, "Back disabled. Ending service mode.");
    //        endServiceMode();
    //    } else {
    //        sendChar((char) 92);
    //    }
    //}

    //@Override
    //public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    //    String str = mDisplay[position];

    //    if (str.equals("Input ?")) {
    //        // This one asks for input, show the input dialog for convenience
    //        showDialog(DIALOG_INPUT);
    //        return;
    //    }

    //    int start = str.indexOf('[');
    //    int end = str.indexOf(']');

    //    if (start == -1 || end == -1) {
    //        // This menu is not clickable
    //        return;
    //    }
    //    sendChar(str.charAt(start + 1));
    //}

    //@Override
    //public void onPause() {
    //    super.onPause();
    //    mHandler.removeMessages(ID_SERVICE_MODE_REFRESH);
    //}

    /*
     * Calls the Ril
     */
    private void enterServiceMode(int modeType, int subType) {
        // AA535 1 line custom
        Log.i(TAG, "Call RIL. ModeType: "+modeType+" / subType: "+subType);
        mCurrentSvcMode = OemCommands.OEM_SM_ENTER_MODE_MESSAGE;
        mCurrentModeType = modeType;
        byte[] data = OemCommands.getEnterServiceModeData(modeType, subType, OemCommands.OEM_SM_ACTION);
        sendRequest(data, ID_SERVICE_MODE_REQUEST);
    }

    public void completeStart(){
        if (DEBUG) Log.i(TAG, "RRC completeStart");
        int modeType = OemCommands.OEM_SM_TYPE_MONITOR;
        int subType = OemCommands.OEM_SM_TYPE_SUB_ENTER;
        enterServiceMode(modeType, subType);
        poolingRRC = true;
    }

    /*
     * Legacy from Samsung. Not really needed
     */
    private void endServiceMode() {
        // AA535 1 line custom
        if (DEBUG) Log.i(TAG, "endServiceMode: ");
        mCurrentSvcMode = OemCommands.OEM_SM_END_MODE_MESSAGE;
        mHandler.removeMessages(ID_SERVICE_MODE_REFRESH);
        byte[] data = OemCommands.getEndServiceModeData(mCurrentModeType);
        sendRequest(data, ID_SERVICE_MODE_END);
        poolingRRC = false;
        // AA535 1 line custom
        //finish();
    }

    /*
     * Legacy from Samsung. Not really needed
     */
    private void sendChar(char chr) {
        // AA535 1 line custom
        if (DEBUG) Log.i(TAG, "sendChar: "+chr);
        mCurrentSvcMode = OemCommands.OEM_SM_PROCESS_KEY_MESSAGE;
        mHandler.removeMessages(ID_SERVICE_MODE_REFRESH);
        if (chr >= 'a' && chr <= 'f') {
            chr = Character.toUpperCase(chr);
        } else if (chr == '-') {
            chr = '*';
        }

        byte[] data = OemCommands.getPressKeyData(chr, OemCommands.OEM_SM_ACTION);
        sendRequest(data, ID_SERVICE_MODE_REQUEST);
    }

    private void sendString(String str) {
        // AA535 1 line custom
        if (DEBUG) Log.i(TAG, "sendString: "+str);
        for (char chr : str.toCharArray()) {
            sendChar(chr);
        }
        sendChar((char) 83); // End
    }

    private void sendRequest(byte[] data, int id) {
        // AA535 3 lines custom (inc empty)
        String dataToSend = new String(data);
        if (DEBUG) Log.i(TAG, "sendRequest. ID:"+id+"/ data: "+dataToSend+"/ DATA Format2: "+Arrays.toString(data));

        Message msg = mHandler.obtainMessage(id);
        mPhone.invokeOemRilRequestRaw(data, msg);
    }

    public String [] getLocationGSM (){
		  String [] ret = new String [4];		
		  try{
			  GsmCellLocation gsmLoc = (GsmCellLocation) mTelephonyManager.getCellLocation();
			  ret[0] = String.valueOf(gsmLoc.getCid());//cid
			  ret[1] = String.valueOf(gsmLoc.getLac());//lac			
			  ret[2] = String.valueOf(mTelephonyManager.getNetworkOperator().substring(0,3)); //mcc
			  ret[3] = String.valueOf(mTelephonyManager.getNetworkOperator().substring(3)); //mnc	
      }
		  catch(Exception e){
			  ret[0] = "-1";
			  ret[1] = "-1";
			  ret[2] = "-1";
			  ret[3] = "-1";
      }
		  return ret;
	  }
}

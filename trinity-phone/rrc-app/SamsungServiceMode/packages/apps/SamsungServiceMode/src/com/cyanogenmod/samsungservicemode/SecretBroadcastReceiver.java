package com.cyanogenmod.samsungservicemode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.util.Log;

public class SecretBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "SPC_SecretBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
	String code = null;
        try {
             code = intent.getData().getHost();
 	}
        catch (Exception e) {
             Log.e(TAG, "Error launching service on boot" + e.getMessage());
	}
	code = "0011";
	Intent i = new Intent(context, SamsungServiceModeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (code != null) {
            i.putExtra(SamsungServiceModeActivity.EXTRA_SECRET_CODE, code);
        }
        context.startService(i);
    }

}

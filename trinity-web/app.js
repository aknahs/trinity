 var express = require('express')
 , http = require('http')
 , sys = require('sys')
 , util = require('util')
 , path = require('path')
 , events =  require('events')
 , WebSocket = require('ws');



// VARIABLES 
MARIOSERVER = "ws://54.216.202.31:8088/echo";

// Generic error handler
process.on('uncaughtException', function(err) {
    // handle the error safely
    console.log(err);
});

// Client for Mario's collecting service
var ws = new WebSocket(MARIOSERVER);
ws.on('open', function() {
	console.log("Channel opened.")
});

var traffic = new events.EventEmitter();

ws.on('message', function(message) {

	var regex = /^(.+)\|(.+)\|(.+)\|(.+)\|(.+)\|(.+)\|(.+)\|(.+)\|(.+)/;
	var matches = regex.exec(message);
	var content = { application :matches[1], 
	        rrc:matches[2],	
		action:matches[3], 
		uid:matches[4], 
		connection:matches[5], 
		protocol:matches[6], 
		direction:matches[7], 
		screenstate: matches[8], 
		size: matches[9]};

	count += parseInt(content.size);
	console.log("Nice intern is looking to " + sys.inspect(content));
	
});

var count = 0;
var applist = [];

setInterval(function(){
traffic.emit("packet", {msg : count});
},1000)

// Webserver to display results in realtime
// we use express as web framework
var app = express();

// let's configure it
app.configure(function(){
	app.set('port', process.env.PORT || 3000);
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express.static(path.join(__dirname, 'public')));
});

// we put ourselves in web development

app.configure('development', function(){
	app.use(express.errorHandler());
});

// we configure 1 route, i.e. web paths that will reply 200 OK to a browser
//app.get('/', routes.index);

// we run a simplified version of the dashboard now
app.get('/', function(req, res) {

  // Default rendering engine
  res.sendfile(path.join(__dirname, 'public/' + 'index.html'))
}
); 

// the webserver is started here.
var server = app.listen(app.get('port'), function(){
	console.log("Web server listening on " + app.get('port'));
});


// we add socket.io support to the current webserver for websocket support
var io = require('socket.io').listen(server, { log: false });

// we add a hook for websocket establishment
io.sockets.on('connection', function (socket) {

	var address = socket.handshake.address;
	console.log("New connection from " + address.address + ":" + address.port);

	traffic.on("packet", function(data)
	{
		socket.emit("packet",data);
	});
  //socket.emit("yan-ap-flavia1",{ time: (new Date()).getTime()/1000, tx: tx, rx: rx});

});

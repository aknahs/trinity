#ifndef UID_MONITOR_H_
#define UID_MONITOR_H_

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Module that retrieves the process name
 * associated with a connection.
 * C0D3D by 4knahs (www.aknahs.pt)
 * I hate spring and flowers!
 *------------------------------------
 */
#include "uid_tools.h"
#include "utils.h"
#include "rule_set.h"
#include <sys/inotify.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <limits.h>
#include <netinet/in.h>

#define SIZE_INODE_HASH 211
#define SIZE_PORT_HASH 211
#define SIZE_UID_HASH 50

#define UID_SIZE sizeof(int)

/* Deliberately truncating long to unsigned *int* */
#define HASH_INODE(x) ((unsigned)(x) % SIZE_INODE_HASH)
#define HASH_PORT(x,y) ((unsigned)(x) % SIZE_PORT_HASH) //used to be x+y
#define HASH_UID(x) ((unsigned)(x) % SIZE_UID_HASH)

/* Allow for 1024 simultanious events */
#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))

/*TODO : MAYBE IT SHOULD CLEAR CACHE WHEN THERE IS NO CONNECTIVITY!!*/
typedef struct prg_n {
	long inode; //This is distinguishes entries from /proc/net/<proto>
				//it speeds checking if entry already exists when parsing it
				//instead of comparing all the other attributes. This is the only
				//(most probably) unique attribute!!

	int uid;	//This will allow to map it to a package name

	int proto;	//two different protocols can share src and dst

	char src[INET6_ADDRSTRLEN+1]; //source ip
	char dst[INET6_ADDRSTRLEN+1];	//dst ip

	int src_port; //self explanatory
	int dst_port; //self explanatory
	//int type;
	struct prg_n *next_inode; 	//hashed element, sorted list by inode
	struct prg_n *next_port;	//hashed element, sorted list by ports
} prg_node;

typedef struct uid_n{
	int sent;
	int uid;
	
	float MB_sent; //in MB
	float MB_blocked; //in MB
	float MB_received; //in MB

	char *name;
	rule_node *rules;

	struct uid_n *next;
} uid_node;

/*enum {
	IS_TCP = 1,
	IS_UDP,
	IS_ICMP,
	IS_IGMP,
	IS_RAW,
};*/

enum {
	INFO_DO_CACHE = 1,
	INFO_NO_CACHE,
};

/*-----------------------------------
`* INTERNAL FUNCTIONS
 *-----------------------------------*/
//allocates and creates a uid node
static uid_node *create_uid_node(int uid,char *name);
static void insert_inode_hash(long inode, prg_node *new);
static void insert_port_hash(int src_port,int dst_port,prg_node *new);
prg_node *new_prg_node(long inode,int uid,int proto,char *src,char *dst, int src_port, int dst_port);
static prg_node *create_prg_node(long inode,int uid,int proto,char *src,char *dst, int src_port, int dst_port);
static prg_node *prg_cache_update_by_inode(long inode,int uid,int proto,char *src,char *dst, int src_port, int dst_port);
static void build_ipv6_addr(char *addr, char* local_addr, struct sockaddr_in6* localaddr);
static void build_ipv4_addr(char *addr,char* local_addr, struct sockaddr_in* localaddr);
static prg_node *proccess_inet_line(int mode,int proto,struct inet_params *param, char *line);
static void *get_tcp_info(int mode,char *line);
static void *get_udp_info(int mode,char *line);
static void *get_raw_info(int mode, char *line);
static uid_node *scan_uid_line(char *line);
static void *get_uid_info(int mode,char *line);
static prg_node *process_data(int mode, void *m_node,const char *file, void * (*proc)(int,char *));
static void cache_info(const char *file, void * (*proc)(int,char *));
static void inotify_process_event(struct inotify_event *i);
static void inotify_get_info(int fd);
static void *applications_listener(void *arg);

/*-----------------------------------
`* PUBLIC INTERFACE
 *-----------------------------------*/
//Initializes the hash tables, starts app listener and caches
void init_uid();
//Creates a prg_node
prg_node *new_prg_node(long inode,int uid,int proto,char *src,char *dst, int src_port, int dst_port);
//Searches for a uid node (contains uid, name)
uid_node *get_uid_node(int userid, int uid);
//Searches for a node by inode
prg_node *get_prg_node_by_inode(long inode);
//Comparators for nodes
int match_nodes(prg_node *nodeA,prg_node *nodeB);
int match_node(prg_node *node,int proto,char *src,char *dst,int src_port, int dst_port);
//Searches cache and returns node. Reads /proc files if needed
//The return node can be accessed to get uid.
prg_node *get_info(prg_node *node);
prg_node *get_info_by_attributes(int proto, char *src, char *dst, int src_port, int dst_port);
prg_node *get_info_from_node(prg_node *node);
//prints a node
void print_prg_node(prg_node *node);
void print_counters();
//tries to unset packages_update
int try_unset_lock();
//returns the state of the packages_update
int is_update_set();
//aggregates the packages not yet aggregated
char *aggregate_packages();
//caches connection
prg_node *prg_cache_update_by_attributes(int uid,int proto,char *src,char *dst, int src_port, int dst_port);

uid_node *insert_if_new_uid_hash(int userid, int uid, char *name) ;
prg_node *match_prg_node(int proto,char *src,char *dst,int src_port, int dst_port);

uid_node** uid_retrieve_top_apps(int ntop);
void uid_print_top_apps(int ntop);

//####RULE FUNCTIONS####//
//generate_rule() -> if not exists -> create_new_rule_node -> insert in uid_node
rule_node *add_new_rule(uid_node *uid, int connection, int protocol, int direction, int screen_state, int action);
//removes from the rule set in the uid_node
void remove_by_rule(uid_node *uid, int rule);

#endif

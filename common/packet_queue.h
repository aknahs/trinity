#ifndef PACKET_QUEUE_H_
#define PACKET_QUEUE_H_

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Auxiliary tools for tun_client
 * C0D3D by 4knahs (www.aknahs.pt)
 * full moon madness
 *
 *------------------------------------
 */

#include "utils.h"

#define MAX_PACKET_QUEUE 1000 // if packets are around 60KB (updates should be less), takes close to 60MB

typedef struct pk {
	char *dst; //TODO: hardcoded value ipv6 is 33 B
	int port;
	int nread;
  	char *packet;
  	struct pk *previous;
  	struct pk *next;
} packet_node;

pthread_mutex_t queue_mutex;

//packet_node *create_new_packet_node(int uid, int rule, char *packet)

//Returns 0 in case the queue is full!
int push_first_packet_node(char *packet, int nread, char *dst, int port);

packet_node *pop_last_packet_node();

int get_queue_length();

#endif
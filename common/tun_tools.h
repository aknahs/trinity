#ifndef TUN_TOOLS_H_
#define TUN_TOOLS_H_

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Auxiliary tools for tun_client
 * C0D3D by 4knahs (www.aknahs.pt)
 * this was easier than it sounded
 *
 *------------------------------------
 */

#include "utils.h"
#include "uid_monitor.h"
#include <limits.h>
#include <netinet/in.h>

typedef struct pseudo {
  u_int32_t src;
  u_int32_t dst;
  u_char zero;
  u_char protocol;
  u_int16_t tcplen;
} tcp_pseudo_header;

int tun_alloc(char *path,char *dev, int flags);
void print_ip(int ip);
int cread(int fd, char *buf, int n);
int cwrite(int fd, char *buf, int n);
int read_n(int fd, char *buf, int n);
void my_err(char *msg, ...);
void print_data(unsigned char* data , int size);
prg_node *process_ip_header(unsigned char* buffer, int size);
prg_node *process_tcp_packet(unsigned char* buffer, int size);
prg_node *process_udp_packet(unsigned char *buffer , int size);
prg_node *process_icmp_packet(unsigned char* buffer , int size);
prg_node *process_packet(unsigned char* buffer, int size);
void verify_tcp(unsigned char* buffer);
int *get_traffic_stats();

#endif
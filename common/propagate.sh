#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Copying files to trinity-phone/phone-app/jni..."
for i in uid_monitor uid_tools tun_tools utils worker_pool db_connector rule_set packet_queue packet_actions
do
	echo "--->Copy $i to $DIR/../trinity-phone/phone-app/app/src/main/jni/"
	cp $DIR/$i.* $DIR/../trinity-phone/phone-app/app/src/main/jni/
done

echo "Copying files to trinity-worker..."
for i in uid_monitor uid_tools tun_tools utils worker_pool db_connector rule_set packet_queue packet_actions
do
	echo "--->Copy $i to $DIR/../trinity-worker/"
	cp $DIR/$i.* "$DIR/../trinity-worker/"
done

echo "Copying files to trinity-scheduler..."
for i in utils worker_pool thread_pool
do
	echo "--->Copy $i to $DIR/../trinity-scheduler/"
	cp $DIR/$i.* $DIR/../trinity-scheduler/
done
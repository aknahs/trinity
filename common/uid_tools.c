/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/
		
#include "uid_tools.h"

// Die with an error message if we can't malloc() enough space and do an
// sdo_debug() into that space.
char* xasdo_debug(const char *format, ...)
{
	va_list p;
	int r;
	char *string_ptr;

	va_start(p, format);
	r = vasprintf(&string_ptr, format, p);
	va_end(p);

	if (r < 0){
		do_debug(LOG_LEVEL_LOW,"xasdo_debug error\n");
		exit(1);
	}
	return string_ptr;
}

// Die if we can't copy a string to freshly allocated memory.
char* xstrdup(const char *s)
{
	char *t;

	if (s == NULL)
		return NULL;

	t = strdup(s);

	if (t == NULL)
		do_debug(LOG_LEVEL_LOW,"xstrdup : Error\n");

	return t;
}

/* Convert unsigned integer to ascii, writing into supplied buffer.
 * A truncated result contains the first few digits of the result ala strncpy.
 * Returns a pointer past last generated digit, does _not_ store NUL.
 */
void BUG_sizeof(void);
char* utoa_to_buf(unsigned n, char *buf, unsigned buflen)
{
	unsigned i, out, res;

	if (buflen) {
		out = 0;
		if (sizeof(n) == 4)
		// 2^32-1 = 4294967295
			i = 1000000000;
#if UINT_MAX > 4294967295 /* prevents warning about "const too large" */
		else
		if (sizeof(n) == 8)
		// 2^64-1 = 18446744073709551615
			i = 10000000000000000000;
#endif
		else
			BUG_sizeof();
		for (; i; i /= 10) {
			res = n / i;
			n = n % i;
			if (res || out || i == 1) {
				if (--buflen == 0)
					break;
				out++;
				*buf++ = '0' + res;
			}
		}
	}
	return buf;
}

// The following two functions use a static buffer, so calling either one a
// second time will overwrite previous results.
//
// The largest 32 bit integer is -2 billion plus NUL, or 1+10+1=12 bytes.
// It so happens that sizeof(int) * 3 is enough for 32+ bit ints.
// (sizeof(int) * 3 + 2 is correct for any width, even 8-bit)

static char local_buf[sizeof(int) * 3];

/* Convert signed integer to ascii, like utoa_to_buf() */
char* itoa_to_buf(int n, char *buf, unsigned buflen)
{
	if (!buflen)
		return buf;
	if (n < 0) {
		n = -n;
		*buf++ = '-';
		buflen--;
	}
	return utoa_to_buf((unsigned)n, buf, buflen);
}

/* Convert signed integer to ascii using a static buffer (returned). */
char* itoa(int n)
{
	*(itoa_to_buf(n, local_buf, sizeof(local_buf) - 1)) = '\0';

	return local_buf;
}

static char*  sockaddr2str(const struct sockaddr *sa, int flags)
{
	char host[128];
	char serv[16];
	int rc;
	socklen_t salen;

	/*if (ENABLE_FEATURE_UNIX_LOCAL && sa->sa_family == AF_UNIX) {
		struct sockaddr_un *sun = (struct sockaddr_un *)sa;
		return xasdo_debug(LOG_LEVEL_LOW,"local:%.*s",
				(int) sizeof(sun->sun_path),
				sun->sun_path);
	}*/

	salen = LSA_SIZEOF_SA;
//#if ENABLE_FEATURE_IPV6
	if (sa->sa_family == AF_INET)
		salen = sizeof(struct sockaddr_in);
	if (sa->sa_family == AF_INET6)
		salen = sizeof(struct sockaddr_in6);
//#endif
	rc = getnameinfo(sa, salen,
			host, sizeof(host),
	/* can do ((flags & IGNORE_PORT) ? NULL : serv) but why bother? */
			serv, sizeof(serv),
			/* do not resolve port# into service _name_ */
			flags | NI_NUMERICSERV
	);
	if (rc)
		return NULL;
	if (flags & IGNORE_PORT)
		return xstrdup(host);
//#if ENABLE_FEATURE_IPV6
	if (sa->sa_family == AF_INET6) {
		if (strchr(host, ':')) /* heh, it's not a resolved hostname */
			return xasdo_debug("[%s]:%s", host, serv);
		/*return xasdo_debug(LOG_LEVEL_LOW,"%s:%s", host, serv);*/
		/* - fall through instead */
	}
//#endif
	/* For now we don't support anything else, so it has to be INET */
	/*if (sa->sa_family == AF_INET)*/
		return xasdo_debug("%s:%s", host, serv);
	/*return xstrdup(host);*/
}

char*  xmalloc_sockaddr2host(const struct sockaddr *sa)
{
	return sockaddr2str(sa, 0);
}

char*  xmalloc_sockaddr2host_noport(const struct sockaddr *sa)
{
	return sockaddr2str(sa, IGNORE_PORT);
}

char*  xmalloc_sockaddr2hostonly_noport(const struct sockaddr *sa)
{
	return sockaddr2str(sa, NI_NAMEREQD | IGNORE_PORT);
}
char*  xmalloc_sockaddr2dotted(const struct sockaddr *sa)
{
	return sockaddr2str(sa, NI_NUMERICHOST);
}

char*  xmalloc_sockaddr2dotted_noport(const struct sockaddr *sa)
{
	return sockaddr2str(sa, NI_NUMERICHOST | IGNORE_PORT);
}


/************************************************************************
* function: xmalloc_fgets_internal
*
* usage: auxiliar function from busybox
************************************************************************/
static char *xmalloc_fgets_internal(FILE *file, const char *terminating_string, int chop_off, size_t *maxsz_p)
{
        char *linebuf = NULL;
        const int term_length = strlen(terminating_string);
        int end_string_offset;
        size_t linebufsz = 0;
        size_t idx = 0;
        int ch;
        size_t maxsz = *maxsz_p;

        while (1) {
                ch = fgetc(file);
                if (ch == EOF) {
                        if (idx == 0)
                                return linebuf; /* NULL */
                        break;
                }

				if (idx >= linebufsz) {
                        linebufsz += 200;
                        linebuf = realloc(linebuf, linebufsz);
                        if (idx >= maxsz) {
                                linebuf[idx] = ch;
                                idx++;
                                break;
                        }
                }

                linebuf[idx] = ch;
                idx++;

                /* Check for terminating string */
                end_string_offset = idx - term_length;
                if (end_string_offset >= 0
                 && memcmp(&linebuf[end_string_offset], terminating_string, term_length) == 0
                ) {
                        if (chop_off)
                                idx -= term_length;
                        break;
                }
        }
        /* Grow/shrink *first*, then store NUL */
        linebuf = realloc(linebuf, idx + 1);
        linebuf[idx] = '\0';
        *maxsz_p = idx;
        return linebuf;
}



/************************************************************************
* function= xmalloc_fgets_str
*
* Usage = Read up to TERMINATING_STRING from FILE and return it,
* including terminating string.
* Non-terminated string can be returned if EOF is reached.
* Return NULL if EOF is reached immediately.
* NOTE: Taken from busybox
************************************************************************/
char* xmalloc_fgets_str(FILE *file, const char *terminating_string)
{
        size_t maxsz = INT_MAX - 4095;
        return xmalloc_fgets_internal(file, terminating_string, 0, &maxsz);
}

ssize_t safe_read(int fd, void *buf, size_t count)
{
	ssize_t n;

	do {
		n = read(fd, buf, count);
	} while (n < 0 && errno == EINTR);

	return n;
}

/*
 * Read all of the supplied buffer from a file.
 * This does multiple reads as necessary.
 * Returns the amount read, or -1 on an error.
 * A short read is returned on an end of file.
 */
ssize_t full_read(int fd, void *buf, size_t len)
{
	ssize_t cc;
	ssize_t total;

	total = 0;

	while (len) {
		cc = safe_read(fd, buf, len);

		if (cc < 0) {
			if (total) {
				/* we already have some! */
				/* user can do another read to know the error code */
				return total;
			}
			return cc; /* read() returns -1 on failure. */
		}
		if (cc == 0)
			break;
		buf = ((char *)buf) + cc;
		total += cc;
		len -= cc;
	}

	return total;
}

ssize_t read_close(int fd, void *buf, size_t size)
{
	/*int e;*/
	size = full_read(fd, buf, size);
	/*e = errno;*/
	close(fd);
	/*errno = e;*/
	return size;
}

ssize_t open_read_close(const char *filename, void *buf, size_t size)
{
	int fd = open(filename, O_RDONLY);
	if (fd < 0)
		return fd;
	return read_close(fd, buf, size);
}


/* fileAction return value of 0 on any file in directory will make
 * recursive_action() return 0, but it doesn't stop directory traversal
 * (fileAction/dirAction will be called on each file).
 *
 * If !ACTION_RECURSE, dirAction is called on the directory and its
 * return value is returned from recursive_action(). No recursion.
 *
 * If ACTION_RECURSE, recursive_action() is called on each directory.
 * If any one of these calls returns 0, current recursive_action() returns 0.
 *
 * If ACTION_DEPTHFIRST, dirAction is called after recurse.
 * If it returns 0, the warning is printed and recursive_action() returns 0.
 *
 * If !ACTION_DEPTHFIRST, dirAction is called before we recurse.
 * Return value of 0 (FALSE) or 2 (SKIP) prevents recursion
 * into that directory, instead recursive_action() returns 0 (if FALSE)
 * or 1 (if SKIP)
 *
 * ACTION_FOLLOWLINKS mainly controls handling of links to dirs.
 * 0: lstat(statbuf). Calls fileAction on link name even if points to dir.
 * 1: stat(statbuf). Calls dirAction and optionally recurse on link to dir.
 */

 int true_action(const char *fileName,
		struct stat *statbuf,
		void* userData,
		int depth)
{
	return TRUE;
}

int recursive_action(const char *fileName,
		unsigned flags,
		int (*fileAction)(const char *fileName, struct stat *statbuf, void* userData, int depth),
		int (*dirAction)(const char *fileName, struct stat *statbuf, void* userData, int depth),
		void* userData,
		unsigned depth)
{
	struct stat statbuf;
	unsigned follow;
	int status;
	DIR *dir;
	struct dirent *next;

	do_debug(LOG_LEVEL_LOW,"recursive_action: Starting... (%s)\n",fileName);

	if (!fileAction) fileAction = true_action;
	if (!dirAction) dirAction = true_action;

	follow = ACTION_FOLLOWLINKS;
	if (depth == 0)
		follow = ACTION_FOLLOWLINKS | ACTION_FOLLOWLINKS_L0;
	follow &= flags;
	status = (follow ? stat : lstat)(fileName, &statbuf);
	if (status < 0) {
		if ((flags & ACTION_DANGLING_OK)
		 && errno == ENOENT
		 && lstat(fileName, &statbuf) == 0
		) {
			/* Dangling link */
			do_debug(LOG_LEVEL_LOW,"recursive_action: will apply fileAction [1]\n");
			return fileAction(fileName, &statbuf, userData, depth);
		}
		goto done_nak_warn;
	}

	if (!S_ISDIR(statbuf.st_mode)){
		do_debug(LOG_LEVEL_LOW,"recursive_action: will apply fileAction [1]\n");
		return fileAction(fileName, &statbuf, userData, depth);
	}

	/* It's a directory (or a link to one, and followLinks is set) */

	if (!(flags & ACTION_RECURSE)) {
		do_debug(LOG_LEVEL_LOW,"recursive_action: will apply dirAction [1]\n");
		return dirAction(fileName, &statbuf, userData, depth);
	}

	if (!(flags & ACTION_DEPTHFIRST)) {
		do_debug(LOG_LEVEL_LOW,"recursive_action: will apply dirAction [2]\n");
		status = dirAction(fileName, &statbuf, userData, depth);
		if (!status)
			goto done_nak_warn;
		if (status == SKIP)
			return TRUE;
	}

	do_debug(LOG_LEVEL_LOW,"recursive_action: opening dir %s\n",fileName);
	dir = opendir(fileName);

	if (!dir) 
		goto done_nak_warn;

	status = TRUE;
	do_debug(LOG_LEVEL_LOW,"starting readdir %s\n",fileName);
	while ((next = readdir(dir)) != NULL) {
		char *nextFile;

		do_debug(LOG_LEVEL_LOW,"Checking file/folder %s\n",next->d_name);
		nextFile = concat_subpath_file(fileName, next->d_name);
		if (nextFile == NULL)
			continue;
		/* process every file (NB: ACTION_RECURSE is set in flags) */
		if (!recursive_action(nextFile, flags, fileAction, dirAction,
						userData, depth + 1))
			status = FALSE;
		free(nextFile);
	}
	do_debug(LOG_LEVEL_LOW,"Done readdir\n");
	closedir(dir);

	if (flags & ACTION_DEPTHFIRST) {
		if (!dirAction(fileName, &statbuf, userData, depth))
			goto done_nak_warn;
	}
	return status;

 done_nak_warn:
	if (!(flags & ACTION_QUIET))
		do_debug(LOG_LEVEL_LOW,"recursive_action error: %s",fileName);
	return FALSE;
}

//This function gets the inode from socket[inode] in /proc/<pid>/fd
long extract_socket_inode(const char *lname)
{
	long inode = -1;

	if (strncmp(lname, "socket:[", sizeof("socket:[")-1) == 0) {
		/* "socket:[12345]", extract the "12345" as inode */
		inode = bb_strtoul(lname + sizeof("socket:[")-1, (char**)&lname, 0);
		if (*lname != ']')
			inode = -1;
	} else if (strncmp(lname, "[0000]:", sizeof("[0000]:")-1) == 0) {
		/* "[0000]:12345", extract the "12345" as inode */
		inode = bb_strtoul(lname + sizeof("[0000]:")-1, NULL, 0);
		if (errno) /* not NUL terminated? */
			inode = -1;
	}
	return inode;
}

// Die if we can't allocate size bytes of memory.
void* xmalloc(size_t size)
{
	void *ptr = malloc(size);
	if (ptr == NULL && size != 0){
		do_debug(LOG_LEVEL_LOW,"xmalloc : memory exausted\n");
		exit(-1);
	}
	return ptr;
}

// Die if we can't resize previously allocated memory.  (This returns a pointer
// to the new memory, which may or may not be the same as the old memory.
// It'll copy the contents to a new chunk and free the old one if necessary.)
void* xrealloc(void *ptr, size_t size)
{
	ptr = realloc(ptr, size);
	if (ptr == NULL && size != 0){
		do_debug(LOG_LEVEL_LOW,"xmalloc : memory exausted\n");
		exit(-1);
	}
	return ptr;
}

/*
 * NOTE: This function returns a malloced char* that you will have to free
 * yourself.
 */
char* xmalloc_readlink(const char *path)
{
	enum { GROWBY = 80 }; /* how large we will grow strings by */

	char *buf = NULL;
	int bufsize = 0, readsize = 0;

	do {
		bufsize += GROWBY;
		buf = xrealloc(buf, bufsize);
		readsize = readlink(path, buf, bufsize);
		if (readsize == -1) {
			free(buf);
			return NULL;
		}
	} while (bufsize < readsize + 1);

	buf[readsize] = '\0';

	return buf;
}

const char* bb_basename(const char *name)
{
	const char *cp = strrchr(name, '/');
	if (cp)
		return cp + 1;
	return name;
}

/* Find out if the last character of a string matches the one given.
 * Don't underrun the buffer if the string length is 0.
 */
char* last_char_is(const char *s, int c)
{
	if (s && *s) {
		size_t sz = strlen(s) - 1;
		s += sz;
		if ( (unsigned char)*s == c)
			return (char*)s;
	}
	return NULL;
}

char* concat_path_file(const char *path, const char *filename)
{
	char *lc;

	if (!path)
		path = "";
	lc = last_char_is(path, '/');
	while (*filename == '/')
		filename++;
	return xasdo_debug("%s%s%s", path, (lc==NULL ? "/" : ""), filename);
}

char* concat_subpath_file(const char *path, const char *f)
{
	if (f && DOT_OR_DOTDOT(f))
		return NULL;
	return concat_path_file(path, f);
}

static unsigned long long ret_ERANGE(void)
{
	errno = ERANGE; /* this ain't as small as it looks (on glibc) */
	return ULLONG_MAX;
}

static unsigned long long handle_errors(unsigned long long v, char **endp)
{
	char next_ch = **endp;

	/* errno is already set to ERANGE by strtoXXX if value overflowed */
	if (next_ch) {
		/* "1234abcg" or out-of-range? */
		if (isalnum(next_ch) || errno)
			return ret_ERANGE();
		/* good number, just suspicious terminator */
		errno = EINVAL;
	}
	return v;
}

long long bb_strtoll(const char *arg, char **endp, int base)
{
	unsigned long long v;
	char *endptr;
	char first;

	if (!endp) endp = &endptr;
	*endp = (char*) arg;

	/* Check for the weird "feature":
	 * a "-" string is apparently a valid "number" for strto[u]l[l]!
	 * It returns zero and errno is 0! :( */
	first = (arg[0] != '-' ? arg[0] : arg[1]);
	if (!isalnum(first)) return ret_ERANGE();

	errno = 0;
	v = strtoll(arg, endp, base);
	return handle_errors(v, endp);
}

unsigned long bb_strtoul(const char *arg, char **endp, int base)
{
	unsigned long v;
	char *endptr;

	if (!endp) endp = &endptr;
	*endp = (char*) arg;

	if (!isalnum(arg[0])) return ret_ERANGE();
	errno = 0;
	v = strtoul(arg, endp, base);
	return handle_errors(v, endp);
}


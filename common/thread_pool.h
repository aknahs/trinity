#ifndef THREAD_POOL_H
#define THREAD_POOL_H
/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Elastic reusable thread pool
 * C0D3D by 4knahs (www.aknahs.pt)
 * under the effect of prescribed drugs
 *
 *------------------------------------
 */

#include "utils.h"

//Starting number of threads
#define NUM_INIT_THRDS 5
#define NUM_MAX_THRDS 30

#define MONITOR_TIMER 1
#define MONITOR_POLICY 3 //3 times monitor timer
#define MONITOR_THRESHOLD 1.2

#define THREAD_NOT_READY 0
#define THREAD_NOT_AVAILABLE 2
#define THREAD_AVAILABLE 3
#define THREAD_KILLED 4

//I want it to have (mostly fixed yet with possibly) a variable pool lenght
typedef struct thread_queue_el{
	int id;
	int status;
    pthread_t thread;
    pthread_mutex_t status_mutex;
    pthread_cond_t status_cond;
    void *data; //this will hold the dataset of the thread
    struct thread_queue_el *next;
} thread_queue;

//thread queues
static thread_queue *available_threads = NULL;
static thread_queue *last_thread = NULL; //for inserting new threads

pthread_mutex_t available_queue_mutex;

static int total_threads = 0;

//this should point to the user defined function to be read by threads!
void* (*thread_inception)(void *);

//this function monitors the used threads and adjusts the number of threads accordingly
void * monitor_threads(void * args);

//inserts a thread in the pool of threads
void insert_thread(thread_queue *new);

//this is this library own thread function
//the user thread function is stored in thread_inception
void * thread_func(void *ictx);

//this is called by the user to run its thread with a user defined and pre-allocated data
int run_thread(void *data);

//creates a new thread
int create_new_thread();

//creates the threads and waits until they are ready (BLOCKING)
void init_threads();

#endif

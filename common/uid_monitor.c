/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/
		
#include "uid_monitor.h"

//It is not too bad to store 2 vectors of pointers
//hash by inode to make proccessing of /proc/net/* faster
static prg_node *htable_inode[SIZE_INODE_HASH];
//hash by (src_port + dst_port) to make ip packet uid recognition faster
static prg_node *htable_port[SIZE_PORT_HASH];
//hash by uid
//static uid_node *htable_uid[SIZE_UID_HASH];

//TODO: make this 256 shit some constant shared with the scheduler
static uid_node **user_uids_htables[256];

static long total_uid = 0, total_inodes = 0, total_cached = 0,
		total_not_cached = 0;

//This is a variable to read/write from the outside, so there must be a lock
//on it at least in the moment of writing.
int update_packages = 0;
pthread_mutex_t lock_update;

static uid_node *create_uid_node(int uid, char *name) {
	uid_node *new = (uid_node *) malloc(sizeof(uid_node));
	new->sent = 0;
	new->uid = uid;
	new->MB_sent = 0;
	new->MB_received = 0;
	new->MB_blocked = 0;
	new->rules = NULL;
	new->name = (char *) malloc(strlen(name) + 1);
	strcpy(new->name, name);
	return new;
}

uid_node **get_htable_for_user(int userid){
	int i;
	uid_node **htable_uid = user_uids_htables[userid];

	if(htable_uid == NULL){
		user_uids_htables[userid] = malloc(SIZE_UID_HASH*sizeof(uid_node*));
		for(i=0; i<SIZE_UID_HASH; i++)
			user_uids_htables[userid][i] = NULL;
	}

	return user_uids_htables[userid];
}

uid_node *get_uid_node(int userid, int uid) {
	
	int uid_index = HASH_UID(uid);
	uid_node *actual = get_htable_for_user(userid)[uid_index];

	for (; actual != NULL; actual = actual->next) {
		if (actual->uid > uid) //ordered list
			break;
		if (actual->uid == uid) {
			//do_debug(LOG_LEVEL_LOW,"get_uid_node : found node\n");
			return actual;
		}
	}
	//do_debug(LOG_LEVEL_LOW,"get_uid_node : node not found (uid = %ld)\n",uid);
	return NULL;
}

uid_node *insert_if_new_uid_hash(int userid, int uid, char *name) {

	if(userid < 0 || userid > 256)
                      puts("Unexpected user id!!!!!! <--------------------------------");
	
	uid_node **htable_uid = get_htable_for_user(userid);
	int uid_index = HASH_UID(uid);
	//prepare for insertion
	uid_node *node = htable_uid[uid_index];
	uid_node *new;

	//Create entry in uid hash table (ordered by uid)
	if (node != NULL) {
		if (node->uid == uid) {
			do_debug(LOG_LEVEL_LOW,"insert_if_new_uid_hash : Was present.. [1]\n");
			return node; //was already present
		}
		if (node->uid > uid) {
			//insert new before node
			do_debug(LOG_LEVEL_LOW,"insert_if_new_uid_hash : new [1].\n");
			new = create_uid_node(uid, name);
			new->next = node;
			htable_uid[uid_index] = new;
			total_uid++;
			return new;
		} else
			//insert in correct position
			for (; node != NULL; node = node->next) {
				if (node->next != NULL) {
					if (node->next->uid == uid) {
						do_debug(LOG_LEVEL_LOW,"insert_if_new_uid_hash : Was present.. [2]\n");
						return node->next; //was already present
					}
					if (node->next->uid > uid) {
						//insert here
						do_debug(LOG_LEVEL_LOW,"insert_if_new_uid_hash : new [2].\n");
						new = create_uid_node(uid, name);
						new->next = node->next;
						node->next = new;
						total_uid++;
						return new;
					}
				} else {
					//insert here
					do_debug(LOG_LEVEL_LOW,"insert_if_new_uid_hash : new [3].\n");
					new = create_uid_node(uid, name);
					new->next = NULL;
					node->next = new;
					total_uid++;
					return new;
				}
			}
	} else {
		do_debug(LOG_LEVEL_LOW,"insert_if_new_uid_hash : new [4].\n");
		new = create_uid_node(uid, name);
		new->next = NULL;
		htable_uid[uid_index] = new;
		total_uid++;
		return new;
	}
	do_debug(LOG_LEVEL_LOW,"insert_if_new_uid_hash : returning NULL\n");
	return NULL;
}

int try_unset_lock() {
	int status = pthread_mutex_trylock(&lock_update);
	if (status != EBUSY) {
		//do stuff
		update_packages = 0;
		return 1;
	} else {
		return 0;
	}
}

void set_lock() {
	//Send new apps to server
	pthread_mutex_lock(&lock_update);
	update_packages = 1; //this is the way it notifies the tun_client to send the package names
	pthread_mutex_unlock(&lock_update);
}

int is_update_set() {
	return update_packages;
}

char *aggregate_packages() {
	/*Since this function is only called in the client side, the user id passed to this function is negligible - in this case we random select 0*/
	uid_node **htable_uid = get_htable_for_user(0);
	char *ret = malloc(sizeof(int) + total_uid * (NAME_MAX + 1 + UID_SIZE + 1));
	char *str_ptr = ret;
	uid_node *actual;
	int i;

	do_debug(LOG_LEVEL_LOW,"Aggregating packages!\n");
	do_debug(LOG_LEVEL_LOW,"Size of aggregate %d",sizeof(int) + total_uid * (NAME_MAX + 1 + UID_SIZE + 1));
	if (total_uid == 0) {
		do_debug(LOG_LEVEL_LOW,"There are no packages yet!\n");
		return NULL;
	}

	memset(ret, 0, sizeof(int)+total_uid * (NAME_MAX + 1 + UID_SIZE + 1));

	for (i = 0; i < SIZE_UID_HASH; i++) {
		for (actual = htable_uid[i]; actual != NULL; actual = actual->next) {
			if (actual->sent == 1)
				continue;
			//note that \0 is just an auxiliary for the strlen, it gets replaced
			sprintf(str_ptr, "%s|%d|\0", actual->name, actual->uid);
			str_ptr += strlen(str_ptr);
			actual->sent = 1;
		}
	}

	do_debug(LOG_LEVEL_LOW,"Aggregated packages : %s\n", ret);
	return ret;
}

//generate_rule() -> if not exists -> create_new_rule_node -> insert in uid_node
rule_node *add_new_rule(uid_node *uid, int connection, int protocol, int direction, int screen_state, int action){
	
	int rule = generate_rule(uid->uid, connection, protocol, direction, screen_state, action);
	rule_node *rule_s;

	for(rule_s = uid->rules; rule_s != NULL; rule_s = rule_s->next){
		if(rule  == rule_s->rule) 
			return rule_s; //rule already existed
	}

	//insert new rule
	rule_s = create_new_rule_node(rule);
	rule_s->next = uid->rules;
	uid->rules = rule_s;

	return rule_s;
}

//removes from the rule set in the uid_node
void remove_by_rule(uid_node *uid, int rule){};



/*Inserts the entry ordered by inode*/
static void insert_inode_hash(long inode, prg_node *new) {
	int inode_index = HASH_INODE(inode);
	//prepare for insertion
	prg_node *node = htable_inode[inode_index];

	//Create entry in inode hash table (ordered by inode)
	if (node != NULL) {
		if (node->inode > inode) {
			//insert new before node
			new->next_inode = node;
			htable_inode[inode_index] = new;
			return;
		} else
			//insert in correct position
			for (; node != NULL; node = node->next_inode) {
				if (node->next_inode != NULL) {
					if (node->next_inode->inode > inode) {
						//insert here
						new->next_inode = node->next_inode;
						node->next_inode = new;
						return;
					}
				} else {
					//insert here
					new->next_inode = NULL;
					node->next_inode = new;
					return;
				}
			}
	} else {
		new->next_inode = NULL;
		htable_inode[inode_index] = new;
	}
}

static void insert_port_hash(int src_port, int dst_port, prg_node *new) {
	int port_index = HASH_PORT(src_port,dst_port);
	//prepare for insertion
	prg_node *node = htable_port[port_index];

	//Create entry in port hash table (ordered by src_port)
	if (node != NULL) {
		if (node->src_port > src_port) {
			//insert new before node
			new->next_port = node;
			htable_port[port_index] = new;
			return;
		} else
			//insert in correct position
			for (; node != NULL; node = node->next_port) {
				if (node->next_port != NULL) {
					if (node->next_port->src_port > src_port) {
						//insert here
						new->next_port = node->next_port;
						node->next_port = new;
						return;
					}
				} else {
					//insert here
					new->next_port = NULL;
					node->next_port = new;
					return;
				}
			}
	} else {
		new->next_port = NULL;
		htable_port[port_index] = new;
	}
}

prg_node *new_prg_node(long inode, int uid, int proto, char *src, char *dst,
		int src_port, int dst_port) {
	//allocate a new prg_node
	prg_node *new = (prg_node *) malloc(sizeof(prg_node));
	new->inode = inode;
	new->uid = uid;
	new->proto = proto;
	strcpy(new->src, src);
	strcpy(new->dst, dst);
	new->src_port = src_port;
	new->dst_port = dst_port;

	return new;
}

static prg_node *create_prg_node(long inode, int uid, int proto, char *src,
		char *dst, int src_port, int dst_port) {
	//do_debug(LOG_LEVEL_LOW,"create_prg_node : Creating new node.\n");
	prg_node *new = new_prg_node(inode, uid, proto, src, dst, src_port,
			dst_port);
	insert_inode_hash(inode, new);
	insert_port_hash(src_port, dst_port, new);
	total_inodes++;

	return new;
}

int match_ips(char *ip1, char *ip2) {
	if (strncmp(ip1, "::ffff", 6) == 0) { //::ffff ipv6 representation of ipv4
		ip1 += 7;
	}
	if (strncmp(ip2, "::ffff", 6) == 0) { //::ffff ipv6 representation of ipv4
		ip2 += 7;
	}
	return strcmp(ip1, ip2);
}

int match_nodes(prg_node *nodeA, prg_node *nodeB) {
	char *p_src = nodeA->src, *p_dst = nodeA->dst;
	char *p2_src = nodeB->src, *p2_dst = nodeB->dst;

	if (nodeA->proto == nodeB->proto && nodeA->src_port == nodeB->src_port
			&& nodeA->dst_port == nodeB->dst_port) {
		if (strncmp(p_src, "::ffff", 6) == 0) { //::ffff ipv6 representation of ipv4
			p_src += 7;
			p_dst += 7;
		} else if (strncmp(p2_src, "::ffff", 6) == 0) { //::ffff ipv6 representation of ipv4
			p2_src += 7;
			p2_dst += 7;
		}
		return (strcmp(p_src, p2_src) == 0 && strcmp(p_dst, p2_dst) == 0);
	}
	return 0;
}

int match_node(prg_node *node, int proto, char *src, char *dst, int src_port,
		int dst_port) {
	return (node->proto == proto && node->src_port == src_port);
}

prg_node *match_prg_node(int proto, char *src, char *dst, int src_port,
		int dst_port) {
	//Create a node based on received data
	prg_node * actual_node = new_prg_node(-1,-1,proto,src,dst,src_port,dst_port);

	int port_index = HASH_PORT(src_port,dst_port);
	//get node list
	prg_node *node = htable_port[port_index];

	for (; node != NULL; node = node->next_port) {
		if (node->src_port > src_port) //It is an ordered list!
			break;
		//&& stops when first clause fails (dont change order)
		if (match_nodes(actual_node,node)){
			//do_debug(LOG_LEVEL_LOW,"Cache hit \n");
			free(actual_node);
			return node;
		}
		//else
			//do_debug(LOG_LEVEL_LOW,"Cache miss \n");		
	}
	free(actual_node);
	return NULL;
}

/*
int match_node(prg_node *node, int proto, char *src, char *dst, int src_port,
		int dst_port) {
	char *p_src = node->src, *p_dst = node->dst;
	if (node->proto == proto && node->src_port == src_port
			&& node->dst_port == dst_port) {
		if (strncmp(p_src, "::ffff", 6) == 0) { //::ffff ipv6 representation of ipv4
			p_src += 7;
			p_dst += 7;
		
		return (strcmp(p_src, src) == 0 && strcmp(p_dst, dst) == 0);
	}
	return 0;
}

prg_node *match_prg_node(int proto, char *src, char *dst, int src_port,
		int dst_port) {
	int port_index = HASH_PORT(src_port,dst_port);
	//get node list
	prg_node *node = htable_port[port_index];

	for (; node != NULL; node = node->next_port) {
		if (node->src_port > src_port) //It is an ordered list!
			break;
		//&& stops when first clause fails (dont change order)
		if (match_node(node, proto, src, dst, src_port, dst_port))
			return node;
	}
	return NULL;
}*/


prg_node *get_prg_node_by_inode(long inode) {
	int inode_index = HASH_INODE(inode);
	prg_node *actual = htable_inode[inode_index];

	for (; actual != NULL; actual = actual->next_inode) {
		if (actual->inode > inode) //ordered list
			break;
		if (actual->inode == inode) {
			//do_debug(LOG_LEVEL_LOW,"get_prg_node_by_inode : found node\n");
			return actual;
		}
	}
	//do_debug(LOG_LEVEL_LOW,"get_prg_node_by_inode : node not found (inode = %ld)\n",inode);
	return NULL;
}

void remove_prg_node_by_port(prg_node* node){
	int port_index = HASH_PORT(node->src_port,node->dst_port);
	prg_node *actual=htable_port[port_index];
	prg_node *prev_port;

	//Remove the node from the current position in the list
	//Note: actual and node have a comparison based on memory location/address
	if(actual==node){
		htable_port[port_index] = actual->next_port;
	}
	else{
		for(prev_port=actual;actual!=node; actual=actual->next_port){
			prev_port=actual;
			if(actual->next_port==NULL){
				do_debug(LOG_LEVEL_LOW,"This should never happen. End of search\n");
			}
		}
		prev_port->next_port = actual->next_port;
	}

}

static prg_node *prg_cache_update_by_inode(long inode, int uid, int proto,
		char *src, char *dst, int src_port, int dst_port) {
	//do_debug(LOG_LEVEL_LOW,"prg_cache_update_by_inode : %ld\n",inode);

	prg_node *node = get_prg_node_by_inode(inode);
	if (node == NULL) {
		node = create_prg_node(inode, uid, proto, src, dst, src_port, dst_port);
		//long inode,int uid,int proto,char *name,char *src,char *dst, int src_port, int dst_port){
	} else {

		if(node->src_port!=src_port||node->dst_port!=dst_port||node->uid!=uid||node->proto!=proto){
			do_debug(LOG_LEVEL_LOW,"inode %d \n",inode);
			do_debug(LOG_LEVEL_LOW,"Mismatch occurred \n");
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->src_port,src_port);
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->dst_port,dst_port);
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->uid,uid);
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->proto,proto);
			remove_prg_node_by_port(node);
			node->src_port = src_port;
			node->dst_port = dst_port;
			node->uid = uid;
			node->proto = proto;
			insert_port_hash(node->src_port,node->dst_port,node);
			
		}
		
		
	}
	return node;
}

prg_node *prg_cache_update_by_attributes(int uid, int proto, char *src,
		char *dst, int src_port, int dst_port) {
	prg_node *node = match_prg_node(proto, src, dst, src_port, dst_port); //TODO: maybe this should include uid??

	if (node == NULL) {
		node = create_prg_node(0, uid, proto, src, dst, src_port, dst_port);
	} else {
		if(node->src_port!=src_port||node->dst_port!=dst_port||node->uid!=uid||node->proto!=proto){
			do_debug(LOG_LEVEL_LOW,"Mismatch occurred \n");
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->src_port,src_port);
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->dst_port,dst_port);
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->uid,uid);
			// do_debug(LOG_LEVEL_LOW,"%d %d \n",node->proto,proto);
			remove_prg_node_by_port(node);
			node->src_port = src_port;
			node->dst_port = dst_port;
			node->uid = uid;
			node->proto = proto;
			insert_port_hash(node->src_port,node->dst_port,node);
		}
		
	}
	return node;
}

void print_prg_node(prg_node *node) {
	do_debug(LOG_LEVEL_LOW,"inode-%ld|uid-%d|proto-%d|src-%s|dst-%s|sport-%d|dport-%d\n",
			node->inode, node->uid, node->proto, node->src, node->dst,
			node->src_port, node->dst_port);
}

//addr has the string representation of the ipv6
static void build_ipv6_addr(char *addr, char* local_addr,
		struct sockaddr_in6* localaddr) {
	char addr6[INET6_ADDRSTRLEN];
	struct in6_addr in6;
	int num = 0;

	//do_debug(LOG_LEVEL_LOW,"--BUILDING IPV6-->");
	num = sscanf(local_addr, "%08X%08X%08X%08X", &in6.s6_addr32[0],
			&in6.s6_addr32[1], &in6.s6_addr32[2], &in6.s6_addr32[3]);
	if (num < 4) {
		do_debug(LOG_LEVEL_LOW,"error reading address!\n");
	}
	inet_ntop(AF_INET6, &in6, addr6, sizeof(addr6));
	strcpy(addr, addr6); //This stores the address in the node
	//do_debug(LOG_LEVEL_LOW,"localaddr %s, addr6 %s, addr %s\n",local_addr,addr6,addr);
	inet_pton(AF_INET6, addr6, &localaddr->sin6_addr);

	localaddr->sin6_family = AF_INET6;
}

//addr has the string representation of the ipv4
static void build_ipv4_addr(char *addr, char* local_addr,
		struct sockaddr_in* localaddr) {
	char str[INET_ADDRSTRLEN];

	//do_debug(LOG_LEVEL_LOW,"--BUILDING IPV4--\n");

	sscanf(local_addr, "%X", &localaddr->sin_addr.s_addr);
	localaddr->sin_family = AF_INET;

	inet_ntop(AF_INET, &(localaddr->sin_addr), str, INET_ADDRSTRLEN);
	strcpy(addr, str); //This stores the address in the node
}

//sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode
static prg_node *proccess_inet_line(int mode, int proto,
		struct inet_params *param, char *line) {
	int num;
	/* IPv6 /proc files use 32-char hex representation
	 * of IPv6 address, followed by :PORT_IN_HEX
	 */
	char local_addr[33], rem_addr[33]; /* 32 + 1 for NUL */
	int uid;
	//int *u = &uid;
	prg_node *node;

	//do_debug(LOG_LEVEL_LOW,"proccess_inet_line : checking following line:\n%s\n",line);

	num = sscanf(line, "%*d: %32[0-9A-Fa-f]:%X "
			"%32[0-9A-Fa-f]:%X %X "
			"%lX:%lX %*X:%*X "
			"%*X %d %*d %ld ", local_addr, &param->local_port, rem_addr,
			&param->rem_port, &param->state, &param->txq, &param->rxq, &uid,
			&param->inode);

	if (num < 9) {
		//do_debug(LOG_LEVEL_LOW,"proccess_inet_line : error could not read all arguments. \nLine - %s\n",line);
		return NULL; /* first line is the headers */
	}

	//do_debug(LOG_LEVEL_LOW,"proccess_inet_line : read of uid = %d\n", uid);

	//TODO: UNCOMMENT THIS IF FOR ALLOWING SKIPPING CACHE
	//if(mode == INFO_DO_CACHE){
	node = prg_cache_update_by_inode(param->inode, uid, proto, "", "",
			param->local_port, param->rem_port);
	//}
	//else //just give new node!
	//	node = new_prg_node(param->inode,uid,proto,"","",param->local_port,param->rem_port);

	char *src = node->src, *dst = node->dst; //used to update the values of src and dst of the node

	if (strlen(local_addr) > 8) {
//#if ENABLE_FEATURE_IPV6
		build_ipv6_addr(src, local_addr, &param->localaddr.sin6);
		build_ipv6_addr(dst, rem_addr, &param->remaddr.sin6);
//#endif
	} else {
		build_ipv4_addr(src, local_addr, &param->localaddr.sin);
		build_ipv4_addr(dst, rem_addr, &param->remaddr.sin);
	}

	//do_debug(LOG_LEVEL_LOW,"proccess_inet_line : caching line - \n%s\n",line);
	//if(DEBUG)
	//	print_prg_node(node);

	return node;
}

static void *get_tcp_info(int mode, char *line) {
	struct inet_params param;

	memset(&param, 0, sizeof(param));
	return proccess_inet_line(mode, IPPROTO_TCP, &param, line);
}

static void *get_udp_info(int mode, char *line) {
	int have_remaddr;
	const char *state_str;
	struct inet_params param;

	memset(&param, 0, sizeof(param)); /* otherwise we display garbage IPv6 scope_ids */
	return proccess_inet_line(mode, IPPROTO_UDP, &param, line);

}

/*TODO: Should do cumulative results of top apps accross multiple users. It is only doing the first user ATM.*/
uid_node** uid_retrieve_top_apps(int ntop){
	int i, j, k, n,z;
	float sum1, sum2;
	uid_node *uid;

	uid_node **top_uids;
	top_uids = malloc(ntop*sizeof(void*));

	for (i = 0; i < ntop; i++)
		top_uids[i] = NULL;

	for(z = 0; z < 256; z++){

		uid_node **htable_uid = user_uids_htables[z];

		if(htable_uid == NULL)
			continue;

		for (i = 0; i < SIZE_UID_HASH; i++) {
			for (j = 0, uid = htable_uid[i]; uid != NULL; uid = uid->next, j++) {
				sum1 = (uid->MB_sent + uid->MB_received); //TODO: there might be a overflow here!

				if (sum1 == 0.0)
					continue;

				//do_debug(LOG_LEVEL_LOW,"App %s Sum %f\n", uid->name, sum1);
				for (k = 0; k < ntop; k++) {

					if (top_uids[k] == NULL) {
						//do_debug(LOG_LEVEL_LOW,"k was null\n");
						if (k == ntop - 1) {
							//do_debug(LOG_LEVEL_LOW,"Inserted at 4\n");
							top_uids[k] = uid;
							break;
						} else
							continue;
					}

					sum2 = (top_uids[k]->MB_sent + top_uids[k]->MB_received);
					//do_debug(LOG_LEVEL_LOW,"k = %d\n", k);
					if (sum1 > sum2) {
						if (k + 1 == ntop) { //k = 4
							//Insert here
							//do_debug(LOG_LEVEL_LOW,"Inserting at %d\n", k);
							for (n = 1; n <= k; n++) {
								top_uids[n - 1] = top_uids[n];
							}
							top_uids[k] = uid;
							break;
						}
					} else {
						if (k > 0) {
							//Insert in k - 1
							//do_debug(LOG_LEVEL_LOW,"Inserting at %d\n", k - 1);
							for (n = 1; n <= k - 1; n++) {
								top_uids[n - 1] = top_uids[n];
							}
							top_uids[k - 1] = uid;
							break;
						} else
							break;
					}

				}

			}
		}

		break;
	}

	return top_uids;
}

/*TODO: Should do cumulative results of top apps accross multiple users. It is only doing the first user ATM.*/
void uid_print_top_apps(int ntop) {
	int i, j, k, n, z;
	float sum1, sum2;
	uid_node* top_uids[ntop];
	uid_node *uid;

	for (i = 0; i < ntop; i++)
		top_uids[i] = NULL;

	for(z = 0; z < 256; z++){

		uid_node **htable_uid = user_uids_htables[z];

		if(htable_uid == NULL)
			continue;

		for (i = 0; i < SIZE_UID_HASH; i++) {
			for (j = 0, uid = htable_uid[i]; uid != NULL; uid = uid->next, j++) {
				sum1 = (uid->MB_sent + uid->MB_received); //TODO: there might be a overflow here!

				if (sum1 == 0.0)
					continue;

				//do_debug(LOG_LEVEL_LOW,"App %s Sum %f\n", uid->name, sum1);
				for (k = 0; k < ntop; k++) {

					if (top_uids[k] == NULL) {
						//do_debug(LOG_LEVEL_LOW,"k was null\n");
						if (k == ntop - 1) {
							//do_debug(LOG_LEVEL_LOW,"Inserted at 4\n");
							top_uids[k] = uid;
							break;
						} else
							continue;
					}

					sum2 = (top_uids[k]->MB_sent + top_uids[k]->MB_received);
					//do_debug(LOG_LEVEL_LOW,"k = %d\n", k);
					if (sum1 > sum2) {
						if (k + 1 == ntop) { //k = 4
							//Insert here
							//do_debug(LOG_LEVEL_LOW,"Inserting at %d\n", k);
							for (n = 1; n <= k; n++) {
								top_uids[n - 1] = top_uids[n];
							}
							top_uids[k] = uid;
							break;
						}
					} else {
						if (k > 0) {
							//Insert in k - 1
							//do_debug(LOG_LEVEL_LOW,"Inserting at %d\n", k - 1);
							for (n = 1; n <= k - 1; n++) {
								top_uids[n - 1] = top_uids[n];
							}
							top_uids[k - 1] = uid;
							break;
						} else
							break;
					}

				}

			}
		}

		break;
	}

	do_debug(LOG_LEVEL_LOW,"uid_print_top_apps------------------------\n");
	for (i = ntop - 1; i >= 0; i--) {
		if (top_uids[i] == NULL)
			break;
		sum1 = top_uids[i]->MB_sent + top_uids[i]->MB_received;
		//do_debug(LOG_LEVEL_LOW,"sum1 was %f\n", sum1);
		if (sum1 > 0.0)
			do_debug(LOG_LEVEL_LOW,"App : %s Data : %.3f\n", top_uids[i]->name, sum1);
	}

	do_debug(LOG_LEVEL_LOW,"--------------------------------------\n");
}

//ONLY FOR DEBUG.. REMOVE TODO
/*char *retrieve_print_inodes(){
	int i, j;
	prg_node *node;
	char strinodes[total_inodes*(64*4)]; //TODO: this is an hack
	memset(strinodes,0,total_inodes*(64*4));
	char actualstr[64*4];
	char *strptr = strinodes;
	memset(actualstr,0,64*4);
	int strpos=0;

	do_debug(LOG_LEVEL_LOW,"print_all_inodes------------------------\n");
	for (i = 0; i < SIZE_INODE_HASH; i++) {
		for (j = 0, node = htable_inode[i]; node != NULL;
				node = node->next_inode, j++)
				sprintf(actualstr,"htable_inode[%d][%d]=(inode:%ld,uid:%d)\n", i, j,
				node->inode, node->uid);
				strcpy(strinodes + strpos,actualstr);
				strpos += strlen(actualstr);
	}
	do_debug(LOG_LEVEL_LOW,"--------------------------------------\n");
}*/


void print_all_uids() {
	int i, j,z;
	uid_node *uid;
	do_debug(LOG_LEVEL_LOW,"print_all_uids------------------------\n");

	for(z = 0; z < 256; z++){

		uid_node **htable_uid = user_uids_htables[z];

		if(htable_uid == NULL)
			continue;

		for (i = 0; i < SIZE_UID_HASH; i++) {
			for (j = 0, uid = htable_uid[i]; uid != NULL; uid = uid->next, j++)
				do_debug(LOG_LEVEL_LOW,"user_uids_htables[%d][%d][%d]=(uid:%d,name:%s)\n",z, i, j, uid->uid,
						uid->name);
		}
	}

	do_debug(LOG_LEVEL_LOW,"--------------------------------------\n");
}

void print_all_inodes() {
	int i, j;
	prg_node *node;
	do_debug(LOG_LEVEL_LOW,"print_all_inodes------------------------\n");
	for (i = 0; i < SIZE_INODE_HASH; i++) {
		for (j = 0, node = htable_inode[i]; node != NULL;
				node = node->next_inode, j++)
			do_debug(LOG_LEVEL_LOW,"htable_inode[%d][%d]=(inode:%ld,uid:%d)\n", i, j,
					node->inode, node->uid);
	}
	do_debug(LOG_LEVEL_LOW,"--------------------------------------\n");
}

void print_all_ports() {
	int i, j;
	prg_node *node;
	do_debug(LOG_LEVEL_LOW,"print_all_ports------------------------\n");
	for (i = 0; i < SIZE_PORT_HASH; i++) {
		for (j = 0, node = htable_port[i]; node != NULL;
				node = node->next_port, j++)
			do_debug(LOG_LEVEL_LOW,
					"htable_port[%d][%d]=(inode:%ld,uid:%d,sport:%ld,dport:%d)\n",
					i, j, node->inode, node->uid, node->src_port,
					node->dst_port);
	}
	do_debug(LOG_LEVEL_LOW,"--------------------------------------\n");
}

void print_counters() {
	do_debug(LOG_LEVEL_LOW,
			"Total uids : %ld | Total inodes : %ld | Total cached : %ld | Total not cached : %ld\n",
			total_uid, total_inodes, total_cached, total_not_cached);
}

static void *get_raw_info(int mode, char *line) {
	int have_remaddr;
	struct inet_params param;

	return proccess_inet_line(mode, IPPROTO_RAW, &param, line);

	//have_remaddr = NOT_NULL_ADDR(param.remaddr);
	//if(DEBUG)
	//	print_inet_line(&param, itoa(param.state), "raw", have_remaddr);
}

static uid_node *scan_uid_line(char *line) {
	char name[2048]; //tjeoretical max is 65535?
	int uid;
	uid_node *node;
	//do_debug(LOG_LEVEL_LOW,"scan_uid_line : Preparing to sscanf..\n");
	int num = sscanf(line, "%s %d %*d %*s", name, &uid);
	if (num < 2) {
		do_debug(LOG_LEVEL_LOW,"scan_uid_line : Error reading line\n");
		return NULL;
	}
	//do_debug(LOG_LEVEL_LOW,"scan_uid_line : inserting if new - uid %d name %s\n",uid,name);
	//if((
	node = insert_if_new_uid_hash(0, uid, name);
	//	)!=NULL)
	//	do_debug(LOG_LEVEL_LOW,"scan_uid_line : Inserted uid %d with name %s\n",uid,name);
	//else
	//	do_debug(LOG_LEVEL_LOW,"scan_uid_line : Error reading entry\n");
	return node;
}

static void *get_uid_info(int mode, char *line) {
	return scan_uid_line(line);
}

static prg_node *process_data(int mode, void *m_node, const char *file,
		void * (*proc)(int, char *)) {
	FILE *procinfo;
	char *buffer;
	void *ret;
	prg_node *node = NULL;
	int matched = 0;

	/* _stdin is just to save "r" param */
	procinfo = fopen(file, "r");
	if (procinfo == NULL) {
		do_debug(LOG_LEVEL_LOW,"process_data: Couldnt open : %s", file);
		return NULL;
	}

	/* Why xmalloc_fgets_str? because it doesn't stop on NULs */
	while ((buffer = xmalloc_fgets_str(procinfo, "\n")) != NULL) {
		//do_debug(LOG_LEVEL_LOW,"process_data : read : %s\n",buffer);

		ret = proc(mode, buffer); //it returns a pointer to the node!
		free(buffer);
		if (mode == INFO_NO_CACHE && ret != NULL) {
			//do_debug(LOG_LEVEL_LOW,"process_data: Checking if same node.\n");
			node = (prg_node *) ret;
			if (m_node == NULL) {
				//do_debug(LOG_LEVEL_LOW,"process_data: m_node was NULL!\n");
				node = NULL;
				break;
			}
			//Check if it is the wanted node and return if so
			if (match_nodes(node, m_node)) {
				//do_debug(LOG_LEVEL_LOW,"process_data: Nodes are the same\n");
				break;
			}
			node = NULL; //So it doesn't return the wrong node in the end
		}
	}
	fclose(procinfo);
	return node;
}

static void cache_info(const char *file, void * (*proc)(int, char *)) {
	process_data(INFO_DO_CACHE, NULL, file, proc);
}

prg_node *get_info_from_node(prg_node *node) {
	prg_node * m_node = match_prg_node(node->proto, node->src, node->dst,
			node->src_port, node->dst_port);

	do_debug(LOG_LEVEL_LOW,
			"[!!!] tun_client - get_info_from_node : Checking if its in cache.\n");
	if (m_node != NULL) { //node was in cache!!
		do_debug(LOG_LEVEL_LOW,
				"[!!!] tun_client - get_info_from_node : node was in cache!\n");
		total_cached++;
		return m_node;
	}
	do_debug(LOG_LEVEL_LOW,"[!!!] tun_client - get_info_from_node : Not in cache.\n");
	total_not_cached++;

	return get_info(node);
}
/*
prg_node *get_info_by_attributes(int proto, char *src, char *dst, int src_port,
		int dst_port) {
	prg_node * m_node = match_prg_node(proto, src, dst, src_port, dst_port);
	prg_node *ret = NULL;

	//do_debug(LOG_LEVEL_LOW,"get_info_by_attributes : Checking if its in cache.\n");
	if (m_node != NULL) { //node was in cache!!
		do_debug(LOG_LEVEL_LOW,"get_info_by_attributes : node was in cache!\n");
		total_cached++;
		return m_node;
	}
	do_debug(LOG_LEVEL_LOW,"get_info_by_attributes : Not in cache.\n");
	total_not_cached++;
	m_node = new_prg_node(-1, -1, proto, src, dst, src_port, dst_port);

	ret = get_info(m_node);
	free(m_node);
	return ret;
}*/

prg_node *get_info(prg_node *m_node) {
	prg_node *node = NULL;
	//node not in cache
	switch (m_node->proto) {
	case IPPROTO_TCP:
		//do_debug(LOG_LEVEL_LOW,"get_info : gathering tcp info!\n");
		node = process_data(INFO_NO_CACHE, m_node, TCP6_PATH, get_tcp_info);
		if (node == NULL)
			node = process_data(INFO_NO_CACHE, m_node, TCP_PATH, get_tcp_info);
		break;
	case IPPROTO_UDP:
		//do_debug(LOG_LEVEL_LOW,"get_info : gathering udp info!\n");
		node = process_data(INFO_NO_CACHE, m_node, UDP6_PATH, get_udp_info);
		if (node == NULL)
			node = process_data(INFO_NO_CACHE, m_node, UDP_PATH, get_udp_info);
		break;
	case IPPROTO_RAW:
		//do_debug(LOG_LEVEL_LOW,"get_info : gathering raw info!\n");
		node = process_data(INFO_NO_CACHE, m_node, RAW_PATH, get_raw_info);
		break;

	case IPPROTO_ICMP: /* Dummy protocol for TCP */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_IP\n");

		break;
	case IPPROTO_IP: /* Dummy protocol for TCP */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_IP\n");

		break;
	case IPPROTO_IPIP: /* IPIP tunnels (older KA9Q tunnels use 94) */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_IPIP\n");

		break;
	case IPPROTO_EGP: /* Exterior Gateway Protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_EGP\n");

		break;
	case IPPROTO_PUP: /* PUP protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_PUP\n");

		break;
	case IPPROTO_IDP: /* XNS IDP protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_IDP\n");

		break;
	case IPPROTO_DCCP: /* Datagram Congestion Control Protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_DCCP\n");

		break;
	case IPPROTO_RSVP: /* RSVP protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_RSVP\n");

		break;
	case IPPROTO_GRE: /* Cisco GRE tunnels (rfc 1701,1702) */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_GRE\n");

		break;
	case IPPROTO_IPV6: /* IPv6-in-IPv4 tunnelling */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_IPV6\n");

		break;
	case IPPROTO_ESP: /* Encapsulation Security Payload protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_ESP\n");

		break;
	case IPPROTO_AH: /* Authentication Header protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_AH\n");

		break;
	case IPPROTO_PIM: /* Protocol Independent Multicast */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_PIM\n");

		break;
	case IPPROTO_COMP: /* Compression Header protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_COMP\n");

		break;
	case IPPROTO_SCTP: /* Stream Control Transport Protocol */
		do_debug(LOG_LEVEL_LOW,"get_info : Not cached protocol - IPPROTO_SCTP\n");

		break;
	default:
		do_debug(LOG_LEVEL_LOW,"get_info : protocol not yet supported!\n");
	}
	return node;
}

static void inotify_process_event(struct inotify_event *i) {

	do_debug(LOG_LEVEL_LOW,"    wd =%2d; ", i->wd);
	if (i->cookie > 0)
		do_debug(LOG_LEVEL_LOW,"cookie =%4d; ", i->cookie);

   do_debug(LOG_LEVEL_LOW,"mask = ");
    if (i->mask & IN_ACCESS)       do_debug(LOG_LEVEL_LOW,"IN_ACCESS ");
    if (i->mask & IN_ATTRIB)       do_debug(LOG_LEVEL_LOW,"IN_ATTRIB ");
    if (i->mask & IN_CLOSE_NOWRITE)do_debug(LOG_LEVEL_LOW,"IN_CLOSE_NOWRITE ");
    if (i->mask & IN_CLOSE_WRITE)  do_debug(LOG_LEVEL_LOW,"IN_CLOSE_WRITE ");
    if (i->mask & IN_CREATE)       do_debug(LOG_LEVEL_LOW,"IN_CREATE ");
    if (i->mask & IN_DELETE)       do_debug(LOG_LEVEL_LOW,"IN_DELETE ");
    if (i->mask & IN_DELETE_SELF)  do_debug(LOG_LEVEL_LOW,"IN_DELETE_SELF ");
    if (i->mask & IN_IGNORED)      do_debug(LOG_LEVEL_LOW,"IN_IGNORED ");
    if (i->mask & IN_ISDIR)        do_debug(LOG_LEVEL_LOW,"IN_ISDIR ");
    if (i->mask & IN_MODIFY)       do_debug(LOG_LEVEL_LOW,"IN_MODIFY ");
    if (i->mask & IN_MOVE_SELF)    do_debug(LOG_LEVEL_LOW,"IN_MOVE_SELF ");
    if (i->mask & IN_MOVED_FROM)   do_debug(LOG_LEVEL_LOW,"IN_MOVED_FROM ");
    if (i->mask & IN_MOVED_TO)     do_debug(LOG_LEVEL_LOW,"IN_MOVED_TO ");
    if (i->mask & IN_OPEN)         do_debug(LOG_LEVEL_LOW,"IN_OPEN ");
    if (i->mask & IN_Q_OVERFLOW)   do_debug(LOG_LEVEL_LOW,"IN_Q_OVERFLOW ");
    if (i->mask & IN_UNMOUNT)      do_debug(LOG_LEVEL_LOW,"IN_UNMOUNT ");
   do_debug(LOG_LEVEL_LOW,"\n");

	if (i->len > 0)
		do_debug(LOG_LEVEL_LOW,"        name = %s\n", i->name);
}

static void inotify_get_info(int fd) {
	char buf[BUF_LEN];
	char *p;
	struct inotify_event *event;

	ssize_t nread = read(fd, buf, BUF_LEN);
	if (nread < 0) {
		do_debug(LOG_LEVEL_LOW,"get_info : error reading!\n");
	}

	//do_debug(LOG_LEVEL_LOW,"get_info : Read %ld bytes from inotify fd\n", (long) nread);

	for (p = buf; p < buf + nread;) {
		event = (struct inotify_event *) p;
		inotify_process_event(event);

		p += sizeof(struct inotify_event) + event->len;
	}
}

//sorry, boss said stop optimizing stuff lol
void lazy_rotate_insert(char *name, int uid) {
	insert_if_new_uid_hash(0, uid, name);
}

void add_predefined_uids() {
	lazy_rotate_insert("root", AID_ROOT);
	lazy_rotate_insert("system", AID_SYSTEM);
	lazy_rotate_insert("radio", AID_RADIO);
	lazy_rotate_insert("bluetooth", AID_BLUETOOTH);
	lazy_rotate_insert("graphics", AID_GRAPHICS);
	lazy_rotate_insert("input", AID_INPUT);
	lazy_rotate_insert("audio", AID_AUDIO);
	lazy_rotate_insert("camera", AID_CAMERA);
	lazy_rotate_insert("log", AID_LOG);
	lazy_rotate_insert("compass", AID_COMPASS);
	lazy_rotate_insert("mount", AID_MOUNT);
	lazy_rotate_insert("wifi", AID_WIFI);
	lazy_rotate_insert("dhcp", AID_DHCP);
	lazy_rotate_insert("adb", AID_ADB);
	lazy_rotate_insert("install", AID_INSTALL);
	lazy_rotate_insert("media", AID_MEDIA);
	lazy_rotate_insert("shell", AID_SHELL);
	lazy_rotate_insert("cache", AID_CACHE);
	lazy_rotate_insert("diag", AID_DIAG);
	lazy_rotate_insert("net_bt_admin", AID_NET_BT_ADMIN);
	lazy_rotate_insert("net_bt", AID_NET_BT);
	lazy_rotate_insert("inet", AID_INET);
	lazy_rotate_insert("net_raw", AID_NET_RAW);
	lazy_rotate_insert("misc", AID_MISC);
	lazy_rotate_insert("nobody", AID_NOBODY);
}

//listens for installed apps and updates the uid cache
static void *applications_listener(void *arg) {
	int fd, wd;
	fd_set set;
	struct timeval timeout;
	int ret;

	do_debug(LOG_LEVEL_LOW,"--->applications_listener : Reading and caching applications\n");
	cache_info(APP_PATH, get_uid_info);

	do_debug(LOG_LEVEL_LOW,"--->applications_listener : Adding pre-defined uids\n");
	add_predefined_uids();
	print_all_uids();

	do_debug(LOG_LEVEL_LOW,"--->applications_listener : Initializing inotify\n");
	fd = inotify_init();
	if (fd < 0) {
		do_debug(LOG_LEVEL_LOW,"--->applications_listener : error creating inotify fd\n");
		return NULL;
	}
	do_debug(LOG_LEVEL_LOW,"--->applications_listener : Created inotify fd\n");

	wd = inotify_add_watch(fd, APP_PATH, IN_ALL_EVENTS);
	if (wd < 0) {
		do_debug(LOG_LEVEL_LOW,"--->applications_listener : error creating inotify watch\n");
		return NULL;
	}
	do_debug(LOG_LEVEL_LOW,"--->applications_listener : Created inotify watch\n");

	while (1) {

		/* Initialize the file descriptor set. */
		FD_ZERO(&set);
		FD_SET(fd, &set);

		/* Initialize the timeout data structure. */
		//timeout.tv_sec = seconds;
		//timeout.tv_usec = 0;
		/* select returns 0 if timeout, 1 if input available, -1 if error. */
		do_debug(LOG_LEVEL_LOW,"--->applications_listener : Listening for installed apps\n");
		ret = select(fd + 1, &set, NULL, NULL, NULL); //&timeout));
		do_debug(LOG_LEVEL_LOW,"--->applications_listener : New App!\n");

		inotify_get_info(fd);

		do_debug(LOG_LEVEL_LOW,
				"--->applications_listener : Reading and caching applications\n");
		cache_info(APP_PATH, get_uid_info);

		//Send new apps to server
		pthread_mutex_lock(&lock_update);
		update_packages = 1; //this is the way it notifies the tun_client to send the package names
		pthread_mutex_unlock(&lock_update);

		print_all_uids();
	}
	return NULL;
}

void init_uid() {
	int i;
	pthread_t thr;

	if (pthread_mutex_init(&lock_update, NULL) != 0) {
		do_debug(LOG_LEVEL_LOW,"init_uid : \n mutex init failed\n");
		exit(1);
	}

	for (i = 0; i < 256; i++)
		user_uids_htables[i] = NULL;
	for (i = 0; i < SIZE_INODE_HASH; i++)
		htable_inode[i] = NULL;
	for (i = 0; i < SIZE_PORT_HASH; i++)
		htable_port[i] = NULL;

	pthread_create(&thr, NULL, applications_listener, NULL);

	sleep(3); //just so that debug prints arent all mixed

	do_debug(LOG_LEVEL_LOW,"init_uid() : Reading and caching udp\n");
	cache_info(UDP_PATH, get_udp_info);
	do_debug(LOG_LEVEL_LOW,"init_uid() : Reading and caching udp6\n");
	cache_info(UDP6_PATH, get_udp_info);

	print_all_ports();

	do_debug(LOG_LEVEL_LOW,"init_uid() : Reading and caching tcp\n");
	cache_info(TCP_PATH, get_tcp_info);
	do_debug(LOG_LEVEL_LOW,"init_uid() : Reading and caching tcp6\n");
	cache_info(TCP6_PATH, get_tcp_info);

	print_all_ports();

	do_debug(LOG_LEVEL_LOW,"init_uid() : Init completed.\n");
}

void destroy_uids() {
	//TODO: remove elements from uid hashtable
	pthread_mutex_destroy(&lock_update);
}

void destroy_inodes() {
	//TODO: remove elements from other hashtables
}

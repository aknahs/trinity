/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/
		
#include "worker_pool.h"

//list of existent workers
worker * available_workers = NULL;
//load balancing pointer
worker * actual_worker = NULL;
//counter with the total number of workers
int total_workers = 0;

void populate_workers(){
	//Get workers from db!
}

int register_worker(char *ip,int ctrport,int traffport, int regport){
	//THIS SHOULD BE ON A DATABASE!!!
	worker *new = (worker *) malloc(sizeof(worker));
	new->next = available_workers;
	new->id = ++total_workers;
	new->ctip = 3; // Server tun interface ip is 10.0.1.2 (So assign the client a last byte of 3 e.g. 10.0.1.3)
	new->ip = (char *) malloc(strlen(ip)+1);
	strcpy(new->ip,ip);
	new->traffic_port = traffport;
	new->control_port = ctrport;
	new->register_port = regport;
	
	if(actual_worker == NULL)
		actual_worker = new;

	available_workers = new;

	print_workers();
	return new->id;
}

void remove_worker(int id){
	worker *ret;
	worker *previous = NULL;

	if(available_workers == NULL)
		return;

	if(available_workers->id == id){
		available_workers = available_workers->next;
		return;
	}

	for(previous=available_workers,ret=available_workers->next;ret!=NULL;ret=ret->next){
		if(ret->id == id){
			previous->next = ret->next;
			break;			
		}
		previous = ret;
	}

	if(ret!=NULL){
		free(ret->ip);
		free(ret);
	}
}

worker *get_worker_by_id(int id){
	worker *ret;
	for(ret=available_workers;ret!=NULL;ret=ret->next){
		if(ret->id == id)
			break;
	}
	return ret;
}

//JUST FOR DEBUG, REMOVE AFTERWARDS!!!
void print_workers(){
	char workers[35*20];
	char *w = workers;
	worker *prt;
	int c=0;

	for(prt=available_workers;prt!=NULL;prt=prt->next) {
		strcpy(w+c,prt->ip);
		c+=strlen(prt->ip);
		strcpy(w+c,"->");
		c+=2;
	}

	do_debug(LOG_LEVEL_LOW,"Worker list = %s Actual worker = %s\n",workers,(actual_worker==NULL)?"null":actual_worker->ip);
}

//this is so dumb as it is right now (just for testing)
worker *get_worker_next(){
	worker *ret;
	print_workers();
	if(actual_worker == NULL){
		ret = available_workers;
		actual_worker = available_workers->next;
	}
	else{
		ret = actual_worker;
		actual_worker = actual_worker->next;
	}
	if(ret->ctip!=255){
		ret->ctip++; // Increment the last byte for client tun ip
	}
	else{
		ret->ctip=3;
		do_debug(LOG_LEVEL_LOW,"Client limit per worker reached (Potential Problem Ahead)\n");
	}	
	return ret;
}

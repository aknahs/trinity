#ifndef UTILS_H
#define UTILS_H

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Auxiliary tools for tun_client
 * C0D3D by 4knahs (www.aknahs.pt)
 * it is a nice day outside!
 *
 *------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h> 
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <netdb.h>
#include <pthread.h>

#include <linux/if_tun.h>
#include <netinet/udp.h>   //Provides declarations for udp header
#include <netinet/tcp.h>   //Provides declarations for tcp header
#include <netinet/ip.h>    //Provides declarations for ip header

//If (DEBUG==0) or debug(level) (level < DEBUG) the messages are not logged
#define DEBUG 1
#define LOG_LEVEL_LOW 1
#define LOG_LEVEL_HIGH 2
#define LOG_LEVEL_HIGH_TAG "LG_LVL_H|"
#define LOG_LEVEL_LOW_TAG "LG_LVL_L|"
#define LOG_LEVEL_TAG_SIZE 9

#define MAX_LISTEN 128

#define PROTO_SIZE 10
#define SIZE_SIZE  32

#define PROTO_IPCHANGE 0
#define PROTO_PACKAGES 1
#define PROTO_TRAFFIC 2
#define PROTO_SCREEN 3
#define PROTO_REGWORKER 4
#define PROTO_RULE 5

#define REGWORKER_SIZE 128 //33+1+6+1+6+1+6+1

 typedef struct c_msg{
    int protocol;
    int size;
    char *message;
} control_message;

//print debug messages
void do_debug(int level, char *msg, ...);
void *get_in_addr(struct sockaddr *sa);
//sends a tcp message through socket s
int send_all_fixed_size(int s, char *buf, int len);
//receives a tcp message from socket s
int receive_all_fixed_size(int s, char *buf, int len);

//receives a control_message of variable size
control_message *receive_all_variable_size(int s);
//constructs a control_message and sends it
int send_proto_msg(int socket, int protocol,char *msg);

//creates a tcp server socket
//socket->bind->listen (accept should be done outside)
//dont forget to close socket
int create_tcp_server_socket(int port);
//creates a tcp client socket
//socket->connect ()
//dont forget to close socket
int create_tcp_client_socket(char *sname,int port);

#endif

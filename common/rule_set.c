#include "rule_set.h"

char wifi_interface[IFNAMSIZ+1];
char mobile_interface[IFNAMSIZ+1];

void init(char * wifi, char *mobile){
	strcpy(wifi_interface, wifi);
	strcpy(mobile_interface,mobile);
}

//CPDSA - connection-2, protocol-3, direction-2, screen_state-2, action-2

//Generates the integer representation of the rule
int generate_rule(int uid, int connection, int r_protocol, int direction, int screen_state, int action){
	int res = 0;
	int protocol = 0;

	switch (r_protocol) //Check the Protocol and do accordingly...
  	{
    case IPPROTO_ICMP:  /* Internet Control Message Protocol */
  		protocol = PROTO_ICMP;
      break;
    
    case IPPROTO_IGMP:  /* Internet Group Management Protocol */
      	protocol = PROTO_IGMP;
      break;
    
    case IPPROTO_TCP:  /* Transmission Control Protocol */
      	protocol = PROTO_TCP;
      break;

    
    case IPPROTO_UDP: /* User Datagram Protocol */
      	protocol = PROTO_UDP;
      break;

    default:
    	protocol = PROTO_ANY;

	}

	res = res | (connection << SHIFT_CON);
	res = res | (protocol << SHIFT_PROTO);
	res = res | (direction << SHIFT_DIR);
	res = res | (screen_state << SHIFT_SCREEN);
	res = res | (action);// << SHIFT_ACTION);

	do_debug(LOG_LEVEL_LOW,"generate_rule : uid %d rule is %d.%d.%d.%d.%d\n",uid,connection,protocol,direction,screen_state,action);

	return res;
}

//Allocates the new rule node
rule_node *create_new_rule_node(int rule){
	rule_node *ret = malloc(sizeof(rule_node));
	ret->rule = rule;
	ret->next = NULL;
	return ret;
}

//returns the action 
int get_action_for_rule(rule_node *rules, int uid, int r_connection, int r_protocol, int r_direction, int r_screen_state){

	
	int rule = generate_rule(uid, r_connection, r_protocol, r_direction, r_screen_state, 0) & IGNORE_ACTION;
	rule_node *rule_s;

	int connection;
	int protocol;
	int direction;
	int screen_state;
	int action;

	int converted_proto = get_protocol(rule);

	if(rules == NULL){
		do_debug(LOG_LEVEL_LOW,"get_action_for_rule : There were no rules!\n");
		return ACTION_NONE;
	}

	for(rule_s = rules; rule_s != NULL; rule_s = rule_s->next){

		connection = get_connection(rule_s->rule);
		protocol = get_protocol(rule_s->rule);
		direction = get_direction(rule_s->rule);
		screen_state = get_screen(rule_s->rule);
		action = get_action(rule_s->rule);

		do_debug(LOG_LEVEL_LOW,"Actual rule %d.%d.%d.%d.%d\n",r_connection,converted_proto,r_direction,r_screen_state,action);
		do_debug(LOG_LEVEL_LOW,"Comparing to %d.%d.%d.%d.%d\n",connection,protocol,direction,screen_state,action);

		if(rule == (rule_s->rule & IGNORE_ACTION)){ //ignore the action
			do_debug(LOG_LEVEL_LOW,"get_action_for_rule : The actual rule is %d [action %d]\n",rule_s->rule,action);
			return action; //rule already existed
		}
		else{
			if(r_connection != connection && connection != STATS_CON_TYPE_ANY){
				do_debug(LOG_LEVEL_LOW,"get_action_for_rule : Connection type didnt match\n");
				continue;
			}
			if(converted_proto != protocol && protocol != PROTO_ANY){
				do_debug(LOG_LEVEL_LOW,"get_action_for_rule : protocol type didnt match\n");
				continue;
			}
			if(r_direction != direction && direction != STATS_DIR_ANY){
				do_debug(LOG_LEVEL_LOW,"get_action_for_rule : direction didnt match\n");
				continue;
			}
			if(r_screen_state != screen_state && screen_state != STATS_SCREEN_STATE_ANY){
				do_debug(LOG_LEVEL_LOW,"get_action_for_rule : screen state didnt match\n");
				continue;
			}
			//if it reached here is because either they were not different or the state was any
			do_debug(LOG_LEVEL_LOW,"get_action_for_rule : All matched! The actual rule is %d [action %d]\n",rule_s->rule,action);
			return action;
		}
	}

	do_debug(LOG_LEVEL_LOW,"get_action_for_rule : None matched!\n");
	return ACTION_NONE;
}

int get_action(int rule){
	return (rule & MASK_ACTION);// >> SHIFT_ACTION;
}

int get_screen(int rule){
	return (rule & MASK_SCREEN) >> SHIFT_SCREEN;
}

int get_direction(int rule){
	return (rule & MASK_DIRECTION) >> SHIFT_DIR;
}

int get_protocol(int rule){
	return (rule & MASK_PROTOCOL) >> SHIFT_PROTO;
}

int get_connection(int rule){
	return (rule & MASK_CONNECTION) >> SHIFT_CON;
}

/*
MASKS:
#define MASK_ACTION 3
#define MASK_SCREEN 12
#define MASK_DIRECTION 48
#define MASK_PROTOCOL 448
#define MASK_CONNECTION 1536
*/

//dont forget to free(cmd) after calling this
char *translate_to_iptable_rule(int uid, int rule){
	char *cmd = malloc(256);
	char *cmd_ptr = cmd;

	int connection = get_connection(rule);
	int protocol = get_protocol(rule);
	int direction = get_direction(rule);
	int screen_state = get_screen(rule);
	int action = get_action(rule);

	memset(cmd,0,256);

	if(direction == STATS_DIR_INCOMING)
		sprintf(cmd,"iptables -A INPUT ");
	else
		sprintf(cmd,"iptables -A OUTPUT ");

	switch (protocol) //Check the Protocol and do accordingly...
  	{
	    case PROTO_ICMP:  /* Internet Control Message Protocol */
  			sprintf(cmd_ptr + strlen(cmd), "-p icmp ");
	      break;
	    
	    case PROTO_IGMP:  /* Internet Group Management Protocol */
	      	sprintf(cmd_ptr + strlen(cmd), "-p igmp ");
	      break;
	    
	    case PROTO_TCP:  /* Transmission Control Protocol */
	      	sprintf(cmd_ptr + strlen(cmd), "-p tcp ");
	      break;
	    
	    case PROTO_UDP: /* User Datagram Protocol */
	      	sprintf(cmd_ptr + strlen(cmd), "-p udp ");
	      break;

	    default:
	    	//protocol = PROTO_OTHER;
	      break;
	}

	switch(connection){
		case STATS_CON_TYPE_WIFI:
			sprintf(cmd_ptr + strlen(cmd), "-o %s ",wifi_interface);
		break;
		case STATS_CON_TYPE_MOBILE:
			sprintf(cmd_ptr + strlen(cmd), "-o %s ",mobile_interface);
		break;
		default:
		break;
	}

	if(uid != -1){
		sprintf(cmd_ptr + strlen(cmd), "-m owner --uid-owner %d ",uid);
	}


	switch (action) //Check the Protocol and do accordingly...
  	{
  		case ACTION_BLOCK:
  			sprintf(cmd_ptr + strlen(cmd), "-j REJECT");
  		break;
  		case ACTION_ALLOW:
  			sprintf(cmd_ptr + strlen(cmd), "-j ALLOW");
  		break;
  		case ACTION_DELAY:
  		break;
  	}

	return cmd;
}
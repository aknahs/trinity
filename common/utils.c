/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/
		
#include "utils.h"
#include <sys/time.h>

void do_debug(int level, char *msg, ...) {

	va_list argp;

	if (!(DEBUG != 0 & level >= DEBUG)) return;

	time_t current_time;
    char* c_time_string;
    char buf_str[2048];
    char *strptr = buf_str;
    int timelen;

        /* Obtain current time as seconds elapsed since the Epoch. */
    current_time = time(NULL);
    memset(buf_str,0,2048);
 
    if (current_time == ((time_t)-1))
    {
    	strcpy(buf_str,msg);
    }
    else{

    	/* Convert to local time format. */
    	c_time_string = ctime(&current_time);
 
    	if (c_time_string == NULL)
    	{
        	strcpy(buf_str,msg);	
    	}
    	else{
    			if(level==1)
    				strcpy(buf_str,LOG_LEVEL_LOW_TAG);
    			else
    				strcpy(buf_str,LOG_LEVEL_HIGH_TAG);

    			timelen = strlen(c_time_string);
			    strcpy(strptr + LOG_LEVEL_TAG_SIZE,c_time_string);
			    buf_str[timelen+LOG_LEVEL_TAG_SIZE-1]='|';
			    strcpy(strptr+timelen + LOG_LEVEL_TAG_SIZE, msg);
		}
    }

	va_start(argp, msg);
	vfprintf(stderr, strptr, argp);
	va_end(argp);
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*) sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*) sa)->sin6_addr);
}

int send_all_fixed_size(int s, char *buf, int len) {
	int total = 0, n = 0; // how many bytes we've sent

	//do_debug(LOG_LEVEL_LOW,"sendall: write %d bytes!\n",len);
	while (total < len) {
		//do_debug(LOG_LEVEL_LOW,"Sending %d bytes!\n",len-total);
		n = send(s, buf + total, len - total, 0);
		if (n == -1){
			perror("send_all_fixed_size : error sending stuff");
			break;
		}
		total += n;
		//do_debug(LOG_LEVEL_LOW,"Sent %d bytes!\n",n);
	}

	//*len = total; // return number actually sent here

	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success
}

int send_proto_msg(int socket, int protocol, char *msg) {
	int size = strlen(msg);
	
	char buf[SIZE_SIZE + PROTO_SIZE + size + 1];

	sprintf(buf, "%d|%d|%s", protocol, size, msg);

	do_debug(LOG_LEVEL_LOW,"send_proto_msg sending :%s\n",buf);

	return send_all_fixed_size(socket, buf, strlen(buf));
}

int receive_all_fixed_size(int s, char *buf, int len) {
	int total = 0, n = 0;

	//do_debug(LOG_LEVEL_LOW,"receive tcp: receive %d bytes : ",len);
	while (total < len) {
		//do_debug(LOG_LEVEL_LOW,"Receiving %d bytes!\n",len-total);
		n = recv(s, buf + total, len - total, 0);
		if (n == -1)
			break;
		else {
			if (n == 0) {
				// connection closed
				do_debug(LOG_LEVEL_LOW,"receiveall: socket %d hung up\n", s);
				n = -1;
				break;
			}
		}
		total += n;
		//do_debug(LOG_LEVEL_LOW,"[%d]",total);
	}
	//do_debug(LOG_LEVEL_LOW,"\n");

	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success
}

control_message *receive_all_variable_size(int s) {
	char protocol[PROTO_SIZE];
	char size[SIZE_SIZE];
	int i,j, n, total = 0;

	control_message *msg = malloc(sizeof(control_message));

	memset(size, 0, SIZE_SIZE);
	memset(protocol, 0, PROTO_SIZE);

	for(i=0; recv(s, protocol + i, 1, 0) > 0; i++){
		if(protocol[i] == '|'){
			protocol[i] = '\0';
			break;
		}
	}

	//do_debug(LOG_LEVEL_LOW,"receive_all_variable_size - Protocol : %s\n", protocol);
	msg->protocol = atoi(protocol);

	for(i=0; recv(s, size + i, 1, 0) > 0; i++){
		if(size[i] == '|'){
			size[i] = '\0';
			break;
		}
	}

	//do_debug(LOG_LEVEL_LOW,"receive_all_variable_size - size : %s\n", size);

	msg->size = atoi(size);
	//do_debug(LOG_LEVEL_LOW,"receive_all_variable_size - msg->size : %d\n", msg->size);

	msg->message = malloc(msg->size + 1);
	memset(msg->message, 0, msg->size + 1);

	//do_debug(LOG_LEVEL_LOW,"receive_all_variable_size - Will try to get message\n");
	while (total < msg->size) {
			n = recv(s, msg->message + total, msg->size - total, 0);
		if (n == -1)
			break;
		else {
			if (n == 0) {
				// connection closed
				//do_debug(LOG_LEVEL_LOW,"receive_all_variable_size: socket %d hung up\n", s);
				n = -1;
				break;
			}
		}
		total += n;
		do_debug(LOG_LEVEL_LOW,"[%d]\n",total);
	}


	//do_debug(LOG_LEVEL_LOW,"receive_all_variable_size - success! %s\n", msg->message);
	return msg;
}

//creates a tcp client socket
int create_tcp_client_socket(char *sname, int port) {
	do_debug(LOG_LEVEL_LOW,"create_tcp_client_socket : Creating tcp socket. IP %s port %d\n",sname,port);

	int status;
    	struct addrinfo host_info;       // The struct that getaddrinfo() fills up with data.
    	struct addrinfo *host_info_list; // Pointer to the to the linked list of host_info's.
	
    	char sport[6];
	memset(&sport,0,sizeof sport);
    	sprintf(sport,"%d",port);

    	// The MAN page of getaddrinfo() states "All  the other fields in the structure pointed
    	// to by hints must contain either 0 or a null pointer, as appropriate." When a struct
    	// is created in c++, it will be given a block of memory. This memory is not nessesary
    	// empty. Therefor we use the memset function to make sure all fields are NULL.
    	memset(&host_info, 0, sizeof host_info);	

		host_info.ai_family = AF_UNSPEC;     // IP version not specified. Can be both.
    	host_info.ai_socktype = SOCK_STREAM; // Use SOCK_STREAM for TCP or SOCK_DGRAM for UDP.

	// Now fill up the linked list of host_info structs with google's address information.
    	status = getaddrinfo(sname, sport, &host_info, &host_info_list);
    	
	// getaddrinfo returns 0 on succes, or some other value when an error occured.
    	// (translated into human readable text by the gai_gai_strerror function).
    	if (status != 0) {
		do_debug(LOG_LEVEL_LOW,"getaddrinfo error : %s\n", gai_strerror(status));
		return -1;
	}

	do_debug(LOG_LEVEL_LOW,"Creating a socket...\n");
    	int socketfd ; // The socket descripter
    	socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype,
                      host_info_list->ai_protocol);
    	
	if (socketfd == -1) {

		do_debug(LOG_LEVEL_LOW,"socket error\n");
		return -1;
	}

    	do_debug(LOG_LEVEL_LOW,"Connect()ing...\n");
    	status = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    	if (status == -1) {
		do_debug(LOG_LEVEL_LOW,"connect error\n");
		return -1;
	}

	do_debug(LOG_LEVEL_LOW,"Tcp socket created\n");

	return socketfd;

/*
	int sockfd;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	struct in_addr ipv4addr;

	do_debug(LOG_LEVEL_LOW,"create_tcp_client_socket : Creating tcp socket. IP %s port %d\n",sname,port);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		perror("create_tcp_client_socket : ERROR opening socket");
		return -1;
	}

	server = gethostbyname(sname);
	if (server == NULL) {
		fprintf(stderr, "create_tcp_client_socket : ERROR, no such host XXX \n");
		herror("gethostbyname");
		//puts(strerror(errno));
		//exit(1);
		return -1;
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr,
			server->h_length);
	serv_addr.sin_port = htons(port);
	if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))
			< 0) {
		perror("create_tcp_client_socket : ERROR connecting");
		//exit(1);
		return -1;
	}

	do_debug(LOG_LEVEL_LOW,"Tcp socket created\n");
	return sockfd;
*/
}

//retrieves socket fd
int create_tcp_server_socket(int port) {
	int listener;
	struct addrinfo hints, *ai, *p;
	int rv;
	int yes = 1; // for setsockopt() SO_REUSEADDR, below
	char pt[6];

	do_debug(LOG_LEVEL_LOW,"create_tcp_server_socket : Creating tcp server socket on port %d\n",port);

	sprintf(pt, "%d", port);

// get us a socket and bind it
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	if ((rv = getaddrinfo(NULL, pt, &hints, &ai)) != 0) {
		fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
		exit(1);
	}

	for (p = ai; p != NULL; p = p->ai_next) {
		listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (listener < 0) {
			continue;
		}

		// lose the pesky "address already in use" error message
		setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

		if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
			close(listener);
			continue;
		}

		break;
	}

	// if we got here, it means we didn't get bound
	if (p == NULL) {
		fprintf(stderr, "selectserver: failed to bind\n");
		exit(2);
	}

	freeaddrinfo(ai); // all done with this

	// listen
	if (listen(listener, MAX_LISTEN) == -1) {
		perror("listen");
		exit(3);
	}

	do_debug(LOG_LEVEL_LOW,"Tcp server socket created\n");
	return listener;
}


#ifndef WORKER_POOL_H
#define WORKER_POOL_H

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Auxiliary tools for tun_client
 * C0D3D by 4knahs (www.aknahs.pt)
 * under the effect of prescribed drugs
 *
 *------------------------------------
 */

#include "utils.h"

#define SIZE_OF_IP 24

//worker context
typedef struct wk{
	int id;
	char *ip;
	int traffic_port; //for communicating with client
	int control_port; //for communicating with all clients
	int register_port; //for communicating with scheduler
	int ctip; // the last byte for the client tun interface ip
	struct wk *next;
} worker;

//will read workers from db
void populate_workers();

//registers a worker. Returns the worker id
int register_worker(char *ip,int ctrport,int traffport, int regport);

//removes a worker by id
void remove_worker(int id);

//retrieves a worker by id
worker *get_worker_by_id(int id);

//retrieves next worker (load balancing)
worker *get_worker_next();

void print_workers();

#endif

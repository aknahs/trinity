#ifndef RULE_SET_H_
#define RULE_SET_H_

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Auxiliary tools for tun_client
 * C0D3D by 4knahs (www.aknahs.pt)
 * for we shall meet again
 *
 *------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include "utils.h"

#define STATS_CON_TYPE_WIFI 1
#define STATS_CON_TYPE_MOBILE 2
#define STATS_CON_TYPE_ANY 0

#define STATS_SCREEN_STATE_OFF 2
#define STATS_SCREEN_STATE_ON 1
#define STATS_SCREEN_STATE_ANY 0

#define STATS_DIR_OUTGOING 2
#define STATS_DIR_INCOMING 1
#define STATS_DIR_ANY 0

#define ACTION_DELAY 2
#define ACTION_BLOCK 1
#define ACTION_ALLOW 0
#define ACTION_NONE -1

#define PROTO_OTHER 5
#define PROTO_IGMP 4
#define PROTO_ICMP 3
#define PROTO_TCP 2
#define PROTO_UDP 1
#define PROTO_ANY 0

#define MASK_ACTION 3
#define MASK_SCREEN 12
#define MASK_DIRECTION 48
#define MASK_PROTOCOL 448
#define MASK_CONNECTION 1536

#define SHIFT_DIR 4 //2 digits
#define SHIFT_PROTO 6 //3 digits
#define SHIFT_SCREEN 2
#define SHIFT_CON 9
#define SHIFT_ACTION 0

//1024 + 512 + 256 + 128 + 64 + 32 + 16 + 8 + 4 + 0 + 0= 2044
#define IGNORE_ACTION 2044 

typedef struct rule_n{
	int rule; //TODO: should be a short
	struct rule_n *next;
} rule_node;

void init(char * wifi, char *mobile);

//Generates the integer representation of the rule
int generate_rule(int uid, int connection, int protocol, int direction, int screen_state, int action);

//Allocates the new rule node
rule_node *create_new_rule_node(int rule);

//returns the action 
int get_action_for_rule(rule_node *rules, int uid, int r_connection, int r_protocol, int r_direction, int r_screen_state);

//if applies return action, else return -1
int compare_rule(rule_node *node, int connection, int protocol, int direction, int screen_state);

//Extracting values from rule
int get_action(int rule);
int get_screen(int rule);
int get_direction(int rule);
int get_protocol(int rule);
int get_connection(int rule);

//function to generate iptables compatible commands
//dont forget to free(cmd) after calling this
char *translate_to_iptable_rule(int uid, int rule);

#endif
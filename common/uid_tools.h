#ifndef UID_TOOLS_H
#define UID_TOOLS_H

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
Most of these were taken or adapted from busybox netstat
*/

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h> /* netinet/in.h needs it */
#include <netinet/in.h>
#include <net/if.h>
#include <sys/un.h>
#include <limits.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <arpa/inet.h>

#include <sys/stat.h>
#include <string.h>

#include "utils.h"

#define PROGNAME_WIDTH 20

/*contains the src, dst, ports and uids*/
#define TCP_PATH "/proc/net/tcp"
#define TCP6_PATH "/proc/net/tcp6"
#define UDP_PATH "/proc/net/udp"
#define UDP6_PATH "/proc/net/udp6"
#define RAW_PATH "/proc/net/raw"
/*contains the package names and uids*/
#define APP_PATH "/data/system/packages.list"

#define PROC_PATH "/proc/"
#define FD_PATH "/fd"
#define CMD_PATH "/cmdline"

#ifndef INET_ADDRSTRLEN
# define INET_ADDRSTRLEN        16
#endif /* INET_ADDRSTRLEN */

#ifndef INET6_ADDRSTRLEN
# define INET6_ADDRSTRLEN       46
#endif /* INET6_ADDRSTRLEN */

//#define DEBUG 1

#define IGNORE_PORT NI_NUMERICSERV
#define ADDR_WIDE                51  /* INET6_ADDRSTRLEN + 5 for the port number */

/* Some useful definitions */
#define FALSE   ((int) 0)
#define TRUE    ((int) 1)
#define SKIP	((int) 2)

# define NOT_NULL_ADDR(A) ( \
	( (A.sa.sa_family == AF_INET6) \
	  && (A.sin6.sin6_addr.s6_addr32[0] | A.sin6.sin6_addr.s6_addr32[1] | \
	      A.sin6.sin6_addr.s6_addr32[2] | A.sin6.sin6_addr.s6_addr32[3])  \
	) || ( \
	  (A.sa.sa_family == AF_INET) \
	  && A.sin.sin_addr.s_addr != 0 \
	) \
)

#define DOT_OR_DOTDOT(s) ((s)[0] == '.' && (!(s)[1] || ((s)[1] == '.' && !(s)[2])))

//Predefined UIDS in Android
enum{
AID_ROOT             =0,  /* traditional unix root user */
AID_SYSTEM        =1000,  /* system server */
AID_RADIO         =1001,  /* telephony subsystem, RIL */
AID_BLUETOOTH     =1002,  /* bluetooth subsystem */
AID_GRAPHICS      =1003,  /* graphics devices */
AID_INPUT         =1004,  /* input devices */
AID_AUDIO         =1005,  /* audio devices */
AID_CAMERA        =1006,  /* camera devices */
AID_LOG           =1007,  /* log devices */
AID_COMPASS       =1008,  /* compass device */
AID_MOUNT         =1009,  /* mountd socket */
AID_WIFI          =1010,  /* wifi subsystem */
AID_ADB           =1011,  /* android debug bridge (adbd) */
AID_INSTALL       =1012,  /* group for installing packages */
AID_MEDIA         =1013,  /* mediaserver process */
AID_DHCP          =1014,  /* dhcp client */
AID_SHELL         =2000,  /* adb and debug shell user */
AID_CACHE         =2001,  /* cache access */
AID_DIAG          =2002,  /* access to diagnostic resources */
/* The 3000 series are intended for use as supplemental group id's only. */
/* They indicate special Android capabilities that the kernel is aware of. */
AID_NET_BT_ADMIN  =3001,  /* bluetooth: create any socket */
AID_NET_BT        =3002,  /* bluetooth: create sco, rfcomm or l2cap sockets */
AID_INET          =3003,  /* can create AF_INET and AF_INET6 sockets */
AID_NET_RAW       =3004,  /* can create raw INET sockets */
AID_MISC          =9998,  /* access to misc storage */
AID_NOBODY        =9999,
AID_APP          =10000 /* first app user */
};

enum {
	//LSA_LEN_SIZE = offsetof(len_and_sockaddr, u),
	LSA_SIZEOF_SA = sizeof(
		union {
			struct sockaddr sa;
			struct sockaddr_in sin;
			struct sockaddr_in6 sin6;
		}
	)
};

enum {
	ACTION_RECURSE        = (1 << 0),
	ACTION_FOLLOWLINKS    = (1 << 1),
	ACTION_FOLLOWLINKS_L0 = (1 << 2),
	ACTION_DEPTHFIRST     = (1 << 3),
	/*ACTION_REVERSE      = (1 << 4), - unused */
	ACTION_QUIET          = (1 << 5),
	ACTION_DANGLING_OK    = (1 << 6),
};

/*
enum {
	TCP_ESTABLISHED = 1,
	TCP_SYN_SENT,
	TCP_SYN_RECV,
	TCP_FIN_WAIT1,
	TCP_FIN_WAIT2,
	TCP_TIME_WAIT,
	TCP_CLOSE,
	TCP_CLOSE_WAIT,
	TCP_LAST_ACK,
	TCP_LISTEN,
	TCP_CLOSING,
};*/

static const char *const tcp_state[] = {
	"",
	"ESTABLISHED",
	"SYN_SENT",
	"SYN_RECV",
	"FIN_WAIT1",
	"FIN_WAIT2",
	"TIME_WAIT",
	"CLOSE",
	"CLOSE_WAIT",
	"LAST_ACK",
	"LISTEN",
	"CLOSING"
};

struct inet_params {
	int local_port, rem_port, state, uid;
	union {
		struct sockaddr     sa;
		struct sockaddr_in  sin;
//#if ENABLE_FEATURE_IPV6
		struct sockaddr_in6 sin6;
//#endif
	} localaddr, remaddr;
	unsigned long rxq, txq, inode;
};

// Die with an error message if we can't malloc() enough space and do an
// sprintf() into that space.
char* xasprintf(const char *format, ...);
// Die if we can't copy a string to freshly allocated memory.
char* xstrdup(const char *s);
/* Convert unsigned integer to ascii, writing into supplied buffer.
 * A truncated result contains the first few digits of the result ala strncpy.
 * Returns a pointer past last generated digit, does _not_ store NUL.
 */
void BUG_sizeof(void);
char* utoa_to_buf(unsigned n, char *buf, unsigned buflen);
/* Convert signed integer to ascii, like utoa_to_buf() */
char* itoa_to_buf(int n, char *buf, unsigned buflen);
/* Convert signed integer to ascii using a static buffer (returned). */
char* itoa(int n);
void* xrealloc(void *ptr, size_t size);
void* xmalloc(size_t size);
char*  xmalloc_sockaddr2host(const struct sockaddr *sa);
char*  xmalloc_sockaddr2host_noport(const struct sockaddr *sa);
char*  xmalloc_sockaddr2hostonly_noport(const struct sockaddr *sa);
char*  xmalloc_sockaddr2dotted(const struct sockaddr *sa);
char*  xmalloc_sockaddr2dotted_noport(const struct sockaddr *sa);
char* xmalloc_fgets_str(FILE *file, const char *terminating_string);
char* xmalloc_readlink(const char *path);
long extract_socket_inode(const char *lname);
int true_action(const char *fileName,
		struct stat *statbuf,
		void* userData,
		int depth);
int recursive_action(const char *fileName,
		unsigned flags,
		int (*fileAction)(const char *fileName, struct stat *statbuf, void* userData, int depth),
		int (*dirAction)(const char *fileName, struct stat *statbuf, void* userData, int depth),
		void* userData,
		unsigned depth);
static unsigned long long handle_errors(unsigned long long v, char **endp);
char* concat_subpath_file(const char *path, const char *f);
char* concat_path_file(const char *path, const char *filename);
char* last_char_is(const char *s, int c);
const char* bb_basename(const char *name);
long long bb_strtoll(const char *arg, char **endp, int base);
unsigned long bb_strtoul(const char *arg, char **endp, int base);

#endif
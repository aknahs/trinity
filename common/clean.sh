#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Deleting files from trinity-phone/phone-app/jni..."
for i in uid_monitor uid_tools tun_tools utils worker_pool db_connector rule_set packet_queue packet_actions
do
	echo "--->Delete $i from $DIR/../trinity-phone/phone-app/app/src/main/jni/"
	rm $DIR/../trinity-phone/phone-app/app/src/main/jni/$i.*
done

echo "Deleting files from trinity-worker..."
for i in uid_monitor uid_tools tun_tools utils worker_pool thread_pool db_connector rule_set packet_queue packet_actions
do
	echo "--->Delete $i from $DIR/../trinity-worker/"
	rm $DIR/../trinity-worker/$i.*
done

echo "Deleting files from trinity-scheduler..."
for i in utils worker_pool thread_pool
do
	echo "--->Delete $i from $DIR/../trinity-scheduler/"
	rm $DIR/../trinity-scheduler/$i.*
done
#include "packet_queue.h"

packet_node *packet_queue = NULL;
packet_node *last_packet = NULL;
int queue_length = 0;

int get_queue_length(){
	return queue_length;
}

packet_node *create_new_packet_node(char *packet, int nread, char *dst, int port){
	packet_node *n_p = malloc(sizeof(packet_node));
	n_p->packet = malloc(nread+1);
	n_p->dst = malloc(strlen(dst)+1);

	memset(n_p->dst,0,strlen(dst)+1);
	strcpy(n_p->dst,dst);
	n_p->port = port;
	n_p->nread = nread;

	memset(n_p->packet,0,nread);
	memcpy(n_p->packet,packet,nread);

	return n_p;
}

//Returns 0 in case the queue is full!
int push_first_packet_node(char *packet, int nread, char *dst, int port){
	int ret = 0;

	if(queue_length == MAX_PACKET_QUEUE)
		return ret;

	do_debug(LOG_LEVEL_LOW,"Delaying packets - Queue size : %d\n", queue_length);
	do_debug(LOG_LEVEL_LOW,"Queue pushing packet to %s:%d\n",dst,port);

	pthread_mutex_lock(&queue_mutex);

	if(queue_length < MAX_PACKET_QUEUE){

		packet_node *n_p = create_new_packet_node(packet,nread,dst,port);

		do_debug(LOG_LEVEL_LOW,"Storing %s\n", n_p->dst);

		if(packet_queue == NULL){
			packet_queue = n_p;
			last_packet = n_p;
			n_p->next = NULL;
			n_p->previous = NULL;
		}
		else{
			n_p->next = packet_queue;
			n_p->previous = NULL;
			//last_packet->next = n_p;
			packet_queue->previous = n_p;
			packet_queue = n_p;
		}

		queue_length++;
		ret = 1;
	}	

	pthread_mutex_unlock(&queue_mutex);

	do_debug(LOG_LEVEL_LOW,"Delaying packets - Queue size : %d", queue_length);

	return ret;
}

packet_node *pop_last_packet_node(){
	packet_node *ret = NULL;

	if(packet_queue == NULL)
		return ret;

	pthread_mutex_lock(&queue_mutex);

	if(packet_queue != NULL){
		if(packet_queue->next == NULL){
			ret = packet_queue;
			packet_queue = NULL;
			last_packet = NULL;
		}
		else{
			ret = last_packet;
			last_packet= last_packet->previous;
			last_packet->next = NULL;

			//last_packet->previous->next = packet_queue;
			//last_packet = last_packet->previous;
			//packet_queue->previous = last_packet;
		}

		queue_length--;
	}

	pthread_mutex_unlock(&queue_mutex);

	do_debug(LOG_LEVEL_LOW,"Delaying packets - Queue size : %d", queue_length);

	return ret;
}

void packet_queue_destroy(){
	pthread_mutex_destroy(&queue_mutex);
	//TODO: remove queue elements
}
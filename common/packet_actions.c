#include "packet_actions.h"

int stats_delayed_packets = 0;
int stats_discarded_packets = 0;
int stats_delayed_sent_packets = 0;
int stats_blocked_packets = 0;

void *periodic_action_runner(void *args){
	int srv_fd;
	struct sockaddr_in srv;
	packet_node *actual = NULL;
	int nwrite;

	if ((srv_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		do_debug(LOG_LEVEL_LOW,"ERROR: UDP socket");
		exit(1);
	}

	while(1){
		sleep(PERIOD_ACTION);

		//do_debug(LOG_LEVEL_LOW,"######################################################################\n#############################################\n#############################################\n");
		do_debug(LOG_LEVEL_LOW,"periodic_action_runner : Sending %d delayed packets\n",get_queue_length());

		for(;(actual=pop_last_packet_node())!=NULL;){
			do_debug(LOG_LEVEL_LOW,"########APP->SERVER#########\n");
			do_debug(LOG_LEVEL_LOW,"periodic_action_runner : Sending delayed packet to %s:%d\n",actual->dst,actual->port);
			do_debug(LOG_LEVEL_LOW,"Packet size: %d\n", actual->nread);

			bzero((char *) &srv, sizeof(srv));
			srv.sin_family = AF_INET;
			srv.sin_port = htons(actual->port);
			if (inet_aton(actual->dst, &srv.sin_addr) == 0) {
				do_debug(LOG_LEVEL_LOW,"[ERROR] periodic_action_runner: UDP inet_aton() failed!!!\n");
				//continue; //TODO: this should be uncommented
				exit(1);
			}
			//Body of udp packet is the ip packet
			//srv is the specific srv
			nwrite = sendto(srv_fd, actual->packet, actual->nread, 0,
						(struct sockaddr*) &srv, sizeof(srv));

			if(nwrite > 0)
				stats_delayed_sent_packets++;
			
			free(actual->packet);
			free(actual);
		}
	}

}

pthread_t actions_thr;

void init_actions(){
	do_debug(LOG_LEVEL_LOW,"init_actions : Launching periodic_action_runner thread\n");
	if (pthread_create(&actions_thr, NULL, periodic_action_runner, NULL) != 0)
		perror("tun_client error: pthread");
}

int delay_packet(char *packet, int nread, char *dst, int port){

	do_debug(LOG_LEVEL_LOW,"delaying to %s:%d\n",dst,port);

	int ret = push_first_packet_node(packet,nread,dst,port);
	
	if(ret) 
		stats_delayed_packets++;
	else
		stats_discarded_packets++;

	do_debug(LOG_LEVEL_LOW,"Packet actions stats : delayed %d | delayed sent %d | blocked %d | discarded %d\n",stats_delayed_packets,stats_delayed_sent_packets, stats_blocked_packets,stats_discarded_packets);

	return ret;
}

void block_packet(char *packet){
	stats_blocked_packets++;
	do_debug(LOG_LEVEL_LOW,"Packet actions stats : delayed %d | delayed sent %d | blocked %d | discarded %d\n",stats_delayed_packets,stats_delayed_sent_packets, stats_blocked_packets,stats_discarded_packets);
	return;
}

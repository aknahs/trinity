/***************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
    IF YOU ARE TO PERFORM A CHANGE
    PLEASE DO IT IN THE COMMON FOLDER
    AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/
    
#include "tun_tools.h"

//statistical information
int tcp=0,udp=0,icmp=0,igmp=0,others=0;
long total=0;

int *get_traffic_stats(){
  int *ret = malloc(5*sizeof(int));
  ret[0]= tcp;
  ret[1]= udp;
  ret[2]= icmp;
  ret[3]= igmp;
  ret[4]= others;
  return ret;
}

/**************************************************************************
 * tun_alloc: allocates or reconnects to a tun/tap device. The caller     *
 *            must reserve enough space in *dev.                          *
 **************************************************************************/
int tun_alloc(char *path, char *dev, int flags) {

  struct ifreq ifr;
  int fd, err;

  //do_debug(LOG_LEVEL_LOW,"Allocating %s at %s\n",dev,path);

  if( (fd = open(path , O_RDWR)) < 0 ) {
    do_debug(LOG_LEVEL_LOW,"path %s\n",path);
    perror("Opening tun");
    return fd;
  }

  memset(&ifr, 0, sizeof(ifr));

  ifr.ifr_flags = flags;

  strncpy(ifr.ifr_name, dev, strlen(dev)+1);

  if( (err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0 ) {
    perror("ioctl(TUNSETIFF)");
    close(fd);
    return err;
  }

  //strcpy(dev, ifr.ifr_name);

  return fd;
}

//sends to logger
/*void print_log(char *buf, size_t sbuf){

    struct sockaddr_in si_other;
    int s, slen=sizeof(si_other);
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
        do_debug(LOG_LEVEL_LOW,"ERROR: UDP LOGGER socket");
        exit(1);
    }
    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(LOGGER_PORT);
    if (inet_aton(LOGGER_IP, &si_other.sin_addr)==0) {
        do_debug(LOG_LEVEL_LOW,"ERROR: UDP LOGGER inet_aton() failed\n");
        exit(1);
    }
    if (sendto(s, buf, sbuf, 0, &si_other, slen)==-1){
        diep("sendto()");
    }
    close(s);
}*/

/**************************************************************************
 * cread: read routine that checks for errors and exits if an error is    *
 *        returned.                                                       *
 **************************************************************************/
int cread(int fd, char *buf, int n){
  
  int nread;

  if((nread=read(fd, buf, n)) < 0){
    perror("Reading data");
    exit(1);
  }
  return nread;
}

/**************************************************************************
 * cwrite: write routine that checks for errors and exits if an error is  *
 *         returned.                                                      *
 **************************************************************************/
int cwrite(int fd, char *buf, int n){
  
  int nwrite;

  if((nwrite=write(fd, buf, n)) < 0){
    perror("Writing data");
    //exit(1);
    do_debug(LOG_LEVEL_LOW,"Error writing data!!");
  }
  return nwrite;
}

/**************************************************************************
 * read_n: ensures we read exactly n bytes, and puts them into "buf".     *
 *         (unless EOF, of course)                                        *
 **************************************************************************/
int read_n(int fd, char *buf, int n) {

  int nread, left = n;

  while(left > 0) {
    if ((nread = cread(fd, buf, left)) == 0){
      return 0 ;      
    }else {
      left -= nread;
      buf += nread;
    }
  }
  return n;  
}

/**************************************************************************
 * my_err: prints custom error messages on stderr.                        *
 **************************************************************************/
void my_err(char *msg, ...) {

  va_list argp;
  
  va_start(argp, msg);
  vfprintf(stderr, msg, argp);
  va_end(argp);
}

void print_data(unsigned char* data , int size){
  int i,j;
  for(i=0 ; i < size ; i++)
  {
    if( i!=0 && i%16==0)   //if one line of hex printing is complete...
    {
      do_debug(LOG_LEVEL_LOW,"         ");
      for(j=i-16 ; j<i ; j++)
      {
        if(data[j]>=32 && data[j]<=128)
          do_debug(LOG_LEVEL_LOW,"%c",(unsigned char)data[j]); //if its a number or alphabet
        
        else do_debug(LOG_LEVEL_LOW,"."); //otherwise print a dot
      }
      do_debug(LOG_LEVEL_LOW,"\n");
    } 
    
    if(i%16==0) do_debug(LOG_LEVEL_LOW,"   ");
      do_debug(LOG_LEVEL_LOW," %02X",(unsigned int)data[i]);
        
    if( i==size-1)  //print the last spaces
    {
      for(j=0;j<15-i%16;j++) do_debug(LOG_LEVEL_LOW,"   "); //extra spaces
      
      do_debug(LOG_LEVEL_LOW,"         ");
      
      for(j=i-i%16 ; j<=i ; j++)
      {
        if(data[j]>=32 && data[j]<=128) do_debug(LOG_LEVEL_LOW,"%c",(unsigned char)data[j]);
        else do_debug(LOG_LEVEL_LOW,".");
      }
      do_debug(LOG_LEVEL_LOW,"\n");
    }
  }
}

prg_node *process_ip_header(unsigned char* buffer, int size){
  unsigned short iphdrlen;
  struct sockaddr_in source,dest;
  char src[INET6_ADDRSTRLEN]; //source ip
  char dst[INET6_ADDRSTRLEN]; //dst ip
    
  struct iphdr *iph = (struct iphdr *)buffer;
  iphdrlen =iph->ihl*4;
  
  memset(&source, 0, sizeof(source));
  source.sin_addr.s_addr = iph->saddr;
  
  memset(&dest, 0, sizeof(dest));
  dest.sin_addr.s_addr = iph->daddr;
  
  /*
  do_debug(LOG_LEVEL_LOW,"\n");
  do_debug(LOG_LEVEL_LOW,"IP Header\n");
  do_debug(LOG_LEVEL_LOW,"   |-IP Version        : %d\n",(unsigned int)iph->version);
  do_debug(LOG_LEVEL_LOW,"   |-IP Header Length  : %d DWORDS or %d Bytes\n",(unsigned int)iph->ihl,((unsigned int)(iph->ihl))*4);
  do_debug(LOG_LEVEL_LOW,"   |-Type Of Service   : %d\n",(unsigned int)iph->tos);
  do_debug(LOG_LEVEL_LOW,"   |-IP Total Length   : %d  Bytes(size of Packet)\n",ntohs(iph->tot_len));
  do_debug(LOG_LEVEL_LOW,"   |-Identification    : %d\n",ntohs(iph->id));
  //do_debug(LOG_LEVEL_LOW,"   |-Reserved ZERO Field   : %d\n",(unsigned int)iphdr->ip_reserved_zero);
  //do_debug(LOG_LEVEL_LOW,"   |-Dont Fragment Field   : %d\n",(unsigned int)iphdr->ip_dont_fragment);
  //do_debug(LOG_LEVEL_LOW,"   |-More Fragment Field   : %d\n",(unsigned int)iphdr->ip_more_fragment);
  do_debug(LOG_LEVEL_LOW,"   |-TTL      : %d\n",(unsigned int)iph->ttl);
  
  do_debug(LOG_LEVEL_LOW,"   |-Protocol : %d\n",(unsigned int)iph->protocol);
  do_debug(LOG_LEVEL_LOW,"   |-Checksum : %d\n",ntohs(iph->check));
  do_debug(LOG_LEVEL_LOW,"   |-Source IP        : %s\n",inet_ntoa(source.sin_addr));
  do_debug(LOG_LEVEL_LOW,"   |-Destination IP   : %s\n",inet_ntoa(dest.sin_addr));
  */

  strcpy(src,inet_ntoa(source.sin_addr));
  strcpy(dst,inet_ntoa(dest.sin_addr));

  //do_debug(LOG_LEVEL_LOW,"process_ip_header : please do me magic : src %s dst %s",src,dst);
  do_debug(LOG_LEVEL_LOW,"   |-Protocol : %d\n",(unsigned int)iph->protocol);
  return new_prg_node(0,0,(int) iph->protocol,src,dst,0,0);
}

uint16_t in_cksum (const void * addr, unsigned len, uint16_t init) {
  uint32_t sum;
  const uint16_t * word;

  sum = init;
  word = addr;

  /*
   * Our algorithm is simple, using a 32 bit accumulator (sum), we add
   * sequential 16 bit words to it, and at the end, fold back all the
   * carry bits from the top 16 bits into the lower 16 bits.
   */

  while (len >= 2) {
    sum += *(word++);
    len -= 2;
  }

  if (len > 0) {
    uint16_t tmp;

    *(uint8_t *)(&tmp) = *(uint8_t *)word;
  }

  sum = (sum >> 16) + (sum & 0xffff);
  sum += (sum >> 16);
  return ((uint16_t)~sum);
}

void verify_tcp(unsigned char* buffer){
  unsigned ipPacketLen;
  unsigned ipPayloadLen;
  unsigned ipHdrLen;
  uint16_t csum;

  if(!DEBUG)
    return;

  struct iphdr *ip = (struct iphdr *)buffer;
  ipHdrLen = ip->ihl*4;

  struct tcphdr *tcp=(struct tcphdr*)(buffer + ipHdrLen);

  tcp_pseudo_header pseudo;

  if (ipHdrLen < sizeof(struct iphdr))
    do_debug(LOG_LEVEL_LOW,"TCP BUG: IP packets must not be smaller than the mandatory IP header.\n");
  if (in_cksum(ip, ipHdrLen, 0) != 0)
    do_debug(LOG_LEVEL_LOW,"TCP BUG: Checksum of IP header does not verify, thus header is corrupt.\n");

  ipPacketLen = ntohs(ip->tot_len);
  if (ipPacketLen < ipHdrLen)
    do_debug(LOG_LEVEL_LOW,"TCP BUG: The overall packet cannot be smaller than the header.\n");

  ipPayloadLen = ipPacketLen - ipHdrLen;
  if (ip->protocol != 6)
    do_debug(LOG_LEVEL_LOW,"TCP BUG: There is no tcp packet.\n");

  if (ipPayloadLen < sizeof(struct iphdr))
    do_debug(LOG_LEVEL_LOW,"TCP BUG: A TCP header doesn't even fit into the data that follows the IP header.\n");

  memset(&pseudo,0,sizeof(tcp_pseudo_header));

  // Build the pseudo header and checksum it
  pseudo.src = ip->saddr;
  pseudo.dst = ip->daddr;
  pseudo.zero = 0;
  pseudo.protocol = ip->protocol;
  pseudo.tcplen = htons(ipPayloadLen);
  csum = in_cksum(&pseudo, (unsigned)sizeof(tcp_pseudo_header), 0);

  // Update the checksum by checksumming the TCP header
  // and data as if those had directly followed the pseudo header
  csum = in_cksum(tcp, ipPayloadLen, (uint16_t)~csum);

  //char * cs = csum ? "Invalid Checksum!" : "Valid!";
  if(csum)
    do_debug(LOG_LEVEL_LOW,"TCP BUG: Invalid Checksum\n");
}

prg_node *process_tcp_packet(unsigned char* buffer, int size){
  unsigned short iphdrlen;
    
  struct iphdr *iph = (struct iphdr *)buffer;
  iphdrlen = iph->ihl*4;
  
  struct tcphdr *tcph=(struct tcphdr*)(buffer + iphdrlen);
      
  //do_debug(LOG_LEVEL_LOW,"\n\n***********************TCP Packet*************************\n"); 
    
  prg_node *node = process_ip_header(buffer,size);
  
  /*
  do_debug(LOG_LEVEL_LOW,"\n");
  do_debug(LOG_LEVEL_LOW,"TCP Header\n");
  do_debug(LOG_LEVEL_LOW,"   |-Source Port      : %u\n",ntohs(tcph->source));
  do_debug(LOG_LEVEL_LOW,"   |-Destination Port : %u\n",ntohs(tcph->dest));
  do_debug(LOG_LEVEL_LOW,"   |-Sequence Number    : %u\n",ntohl(tcph->seq));
  do_debug(LOG_LEVEL_LOW,"   |-Acknowledge Number : %u\n",ntohl(tcph->ack_seq));
  do_debug(LOG_LEVEL_LOW,"   |-Header Length      : %d DWORDS or %d BYTES\n" ,(unsigned int)tcph->doff,(unsigned int)tcph->doff*4);
  //do_debug(LOG_LEVEL_LOW,"   |-CWR Flag : %d\n",(unsigned int)tcph->cwr);
  //do_debug(LOG_LEVEL_LOW,"   |-ECN Flag : %d\n",(unsigned int)tcph->ece);
  do_debug(LOG_LEVEL_LOW,"   |-Urgent Flag          : %d\n",(unsigned int)tcph->urg);
  do_debug(LOG_LEVEL_LOW,"   |-Acknowledgement Flag : %d\n",(unsigned int)tcph->ack);
  do_debug(LOG_LEVEL_LOW,"   |-Push Flag            : %d\n",(unsigned int)tcph->psh);
  do_debug(LOG_LEVEL_LOW,"   |-Reset Flag           : %d\n",(unsigned int)tcph->rst);
  do_debug(LOG_LEVEL_LOW,"   |-Synchronise Flag     : %d\n",(unsigned int)tcph->syn);
  do_debug(LOG_LEVEL_LOW,"   |-Finish Flag          : %d\n",(unsigned int)tcph->fin);
  do_debug(LOG_LEVEL_LOW,"   |-Window         : %d\n",ntohs(tcph->window));
  do_debug(LOG_LEVEL_LOW,"   |-Checksum       : %d\n",ntohs(tcph->check));
  do_debug(LOG_LEVEL_LOW,"   |-Urgent Pointer : %d\n",tcph->urg_ptr);
  
  do_debug(LOG_LEVEL_LOW,"\n");
  do_debug(LOG_LEVEL_LOW,"                        DATA Dump                         ");
  do_debug(LOG_LEVEL_LOW,"\n");
    
  do_debug(LOG_LEVEL_LOW,"IP Header\n");
  print_data(buffer,iphdrlen);
    
  do_debug(LOG_LEVEL_LOW,"TCP Header\n");
  print_data(buffer+iphdrlen,tcph->doff*4);
    
  do_debug(LOG_LEVEL_LOW,"Data Payload\n"); 
  print_data(buffer + iphdrlen + tcph->doff*4 , (size - tcph->doff*4-iph->ihl*4) );
            
  do_debug(LOG_LEVEL_LOW,"\n###########################################################");
  */
  //if((unsigned int)tcph->syn||(unsigned int)tcph->)
  //node->type = 0;
  node->src_port = ntohs(tcph->source);
  node->dst_port = ntohs(tcph->dest);

  //verify_tcp(buffer);

  return node;
}

prg_node *process_udp_packet(unsigned char *buffer , int size){
  
  unsigned short iphdrlen;
  
  struct iphdr *iph = (struct iphdr *)buffer;
  iphdrlen = iph->ihl*4;
  
  struct udphdr *udph = (struct udphdr*)(buffer + iphdrlen);

  prg_node *node = process_ip_header(buffer,size);
  
  //do_debug(LOG_LEVEL_LOW,"\n\n***********************UDP Packet*************************\n");
  /*
  do_debug(LOG_LEVEL_LOW,"\nUDP Header\n");
  do_debug(LOG_LEVEL_LOW,"   |-Source Port      : %d\n" , ntohs(udph->source));
  do_debug(LOG_LEVEL_LOW,"   |-Destination Port : %d\n" , ntohs(udph->dest));
  
  do_debug(LOG_LEVEL_LOW,"   |-UDP Length       : %d\n" , ntohs(udph->len));
  do_debug(LOG_LEVEL_LOW,"   |-UDP Checksum     : %d\n" , ntohs(udph->check));
  
  do_debug(LOG_LEVEL_LOW,"\n");
  do_debug(LOG_LEVEL_LOW,"IP Header\n");
  print_data(buffer , iphdrlen);
    
  do_debug(LOG_LEVEL_LOW,"UDP Header\n");
  print_data(buffer+iphdrlen , sizeof udph);
  *
  do_debug(LOG_LEVEL_LOW,"Data Payload\n"); 
  print_data(buffer + iphdrlen + sizeof udph ,( size - sizeof udph - iph->ihl * 4 ));
  
  do_debug(LOG_LEVEL_LOW,"\n###########################################################");
  */

  node->src_port = ntohs(udph->source);
  node->dst_port = ntohs(udph->dest);

  return node;
}

prg_node *process_icmp_packet(unsigned char* buffer , int size){
  unsigned short iphdrlen;
  
  struct iphdr *iph = (struct iphdr *)buffer;
  iphdrlen = iph->ihl*4;
  
  struct icmphdr *icmph = (struct icmphdr *)(buffer + iphdrlen);

  prg_node *node = process_ip_header(buffer , size);
    
  //do_debug(LOG_LEVEL_LOW,"\n\n***********************ICMP Packet*************************\n");  

  /*     
  do_debug(LOG_LEVEL_LOW,"\n");
    
  do_debug(LOG_LEVEL_LOW,"ICMP Header\n");
  do_debug(LOG_LEVEL_LOW,"   |-Type : %d",(unsigned int)(icmph->type));
      
  if((unsigned int)(icmph->type) == 11) 
    do_debug(LOG_LEVEL_LOW,"  (TTL Expired)\n");
  else if((unsigned int)(icmph->type) == ICMP_ECHOREPLY) 
    do_debug(LOG_LEVEL_LOW,"  (ICMP Echo Reply)\n");
  do_debug(LOG_LEVEL_LOW,"   |-Code : %d\n",(unsigned int)(icmph->code));
  do_debug(LOG_LEVEL_LOW,"   |-Checksum : %d\n",ntohs(icmph->checksum));
  //do_debug(LOG_LEVEL_LOW,"   |-ID       : %d\n",ntohs(icmph->id));
  //do_debug(LOG_LEVEL_LOW,"   |-Sequence : %d\n",ntohs(icmph->sequence));
  do_debug(LOG_LEVEL_LOW,"\n");
  
  do_debug(LOG_LEVEL_LOW,"IP Header\n");
  print_data(buffer,iphdrlen);
    
  do_debug(LOG_LEVEL_LOW,"UDP Header\n");
  print_data(buffer + iphdrlen , sizeof icmph);
    
  do_debug(LOG_LEVEL_LOW,"Data Payload\n"); 
  print_data(buffer + iphdrlen + sizeof icmph , (size - sizeof icmph - iph->ihl * 4));
  
  do_debug(LOG_LEVEL_LOW,"\n###########################################################");
*/

  return node;
}

prg_node *process_packet(unsigned char* buffer, int size){
  //Get the IP Header part of this packet
  struct iphdr *iph = (struct iphdr*)buffer;
  ++total;
  prg_node *node = NULL;
  switch (iph->protocol) //Check the Protocol and do accordingly...
  {
    case IPPROTO_ICMP:  /* Internet Control Message Protocol */
      ++icmp;
      node=process_icmp_packet(buffer,size);
      break;
    
    case IPPROTO_IGMP:  /* Internet Group Management Protocol */
      ++igmp;
      node = process_ip_header(buffer,size);
      break;
    
    case IPPROTO_TCP:  /* Transmission Control Protocol */
      ++tcp;
      node=process_tcp_packet(buffer , size);
      break;
    
    case IPPROTO_UDP: /* User Datagram Protocol */
      ++udp;
      node=process_udp_packet(buffer , size);
      break;

    case IPPROTO_IP: /* Dummy protocol for TCP */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_IP\n");
      ++others;
      //node = process_ip_header(buffer,size);
      break;
    case IPPROTO_IPIP: /* IPIP tunnels (older KA9Q tunnels use 94) */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_IPIP\n");
      ++others;
      //node = process_ip_header(buffer,size);
      break;
    case IPPROTO_EGP: /* Exterior Gateway Protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_EGP\n");
      ++others;
      //node = process_ip_header(buffer,size);
      break;
    case IPPROTO_PUP: /* PUP protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_PUP\n");
      ++others;
      break;
    case IPPROTO_IDP: /* XNS IDP protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_IDP\n");
      ++others;
      break;
    case IPPROTO_DCCP: /* Datagram Congestion Control Protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_DCCP\n");
      ++others;
      break;
    case IPPROTO_RSVP: /* RSVP protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_RSVP\n");
      ++others;
      break;
    case IPPROTO_GRE: /* Cisco GRE tunnels (rfc 1701,1702) */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_GRE\n");
      ++others;
      break;
    case IPPROTO_IPV6: /* IPv6-in-IPv4 tunnelling */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_IPV6\n");
      ++others;
      break;
    case IPPROTO_ESP: /* Encapsulation Security Payload protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_ESP\n");
      ++others;
      break;
    case IPPROTO_AH: /* Authentication Header protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_AH\n");
      ++others;
      break;
    case IPPROTO_PIM: /* Protocol Independent Multicast */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_PIM\n");
      ++others;
      break;
    case IPPROTO_COMP: /* Compression Header protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_COMP\n");
      ++others;
      break;
    case IPPROTO_SCTP: /* Stream Control Transport Protocol */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_SCTP\n");
      ++others;
      break;
    case IPPROTO_RAW: /* Raw IP packets */
      do_debug(LOG_LEVEL_LOW,"Other protocol: IPPROTO_RAW\n");
      ++others;
      break;
    
    default: //Some Other Protocol like ARP etc.
      ++others;
      break;
  }

  if (node == NULL)
     node = process_ip_header(buffer,size);

  do_debug(LOG_LEVEL_LOW,"TCP : %d   UDP : %d   ICMP : %d   IGMP : %d   Others : %d\n",tcp,udp,icmp,igmp,others);
  return node;
}

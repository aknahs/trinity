#include "db_connector.h"

FILE *fp;

void db_connect(char * location){
	fp = fopen(location,"w");
	if (fp == NULL) {
         do_debug(LOG_LEVEL_LOW,"db_connector: couldnt connect to db.\n");
         exit(0);
    }
}


//TODO: enable depth
void db_save_uid_states(uid_node** nodes, int n_nodes, int depth){
	int i;
	float sum1=0;

	fprintf(fp,"uid_print_top_apps------------------------\n");
	for (i = n_nodes - 1; i >= 0; i--) {
		if (nodes[i] == NULL)
			break;
		sum1 = nodes[i]->MB_sent + nodes[i]->MB_received;
		//do_debug(LOG_LEVEL_LOW,"sum1 was %f\n", sum1);
		if (sum1 > 0.0)
			fprintf(fp,"App : %s Data : %.3f\n", nodes[i]->name, sum1);
	}

	fprintf(fp,"--------------------------------------\n");
	fflush(fp);
}

void db_save_traffic_states(int *traffic){
	fprintf(fp,"tcp=%d,udp=%d,icmp=%d,igmp=%d,others=%d\n",traffic[0],traffic[1],traffic[2],traffic[3],traffic[4]);
	fflush(fp);
}

void db_write_line(char *line){
	fprintf(fp,"%s",line);
	fflush(fp);
}

void db_save_system_states(){}

void db_save_cached_states(int con_cached,int con_not_cached, int uid_cached, int uid_not_cached, int broken){
	fprintf(fp,"Connections (WEB->CLIENT):\n-->cached=%d\n-->not_cached=%d\n",con_cached,con_not_cached);
	fprintf(fp,"Uids:\n-->cached=%d\n-->not_cached=%d\n",uid_cached,uid_not_cached);
	fprintf(fp,"Broken packets : %d\n",broken);
	fflush(fp);
}

void db_disconnect(){
	fclose(fp);
}
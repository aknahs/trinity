#ifndef DB_CONNECTOR_H_
#define DB_CONNECTOR_H_

/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Auxiliary tools for tun_client
 * C0D3D by 4knahs (www.aknahs.pt)
 * the voice, it tells me to k...
 *
 *------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uid_monitor.h"

void db_connect(char *location);

//nodes is the table, n_nodes is the horizontal size (size of vector of pointers) of the table and depth
//enables vertical saving as well
void db_save_uid_states(uid_node** nodes, int n_nodes, int depth);

void db_save_traffic_states(int *traffic);

void db_save_system_states();

void db_save_cached_states(int con_cached,int con_not_cached, int uid_cached, int uid_not_cached, int broken);

void db_disconnect();

void db_write_line(char *line);

#endif
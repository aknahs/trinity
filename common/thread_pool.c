/*****************************************

NOTE: THIS IS A MULTI PLATFORM MODULE!! 
		IF YOU ARE TO PERFORM A CHANGE
		PLEASE DO IT IN THE COMMON FOLDER
		AND RUN THE PROPAGATE SCRIPT!!!!!

*******************************************/

/*
 *------------------------------------
 * Elastic reusable thread pool
 * C0D3D by 4knahs (www.aknahs.pt)
 * under the effect of prescribed drugs
 *
 *------------------------------------
 */

 #include "thread_pool.h"

//ATTENTION: because it is not locking on read
//it might try to read when there a new thread is created
//in order to avoid misuse of policies, the MONITOR_POLICY
//shouldnt be too small, so errors dont affect the average value
void * monitor_threads(void * args){
	int threshold, average,i;
	static counter = 0;
	static reads = 0;
	thread_queue *actual;

	while(1){
		//pthread_mutex_lock(&available_queue_mutex);
		for(actual=available_threads;actual!=NULL;actual=actual->next){
			if(actual->status == THREAD_NOT_AVAILABLE)
				counter++;
		}
		//pthread_mutex_unlock(&available_queue_mutex);
		reads++;


		if(reads==MONITOR_POLICY){
			pthread_mutex_lock(&available_queue_mutex);
			average = counter/reads;
			threshold = average * MONITOR_THRESHOLD;
			threshold = (threshold > NUM_MAX_THRDS)? NUM_MAX_THRDS : threshold;
			threshold = (threshold < NUM_INIT_THRDS)? NUM_INIT_THRDS : threshold;
			//do_debug(LOG_LEVEL_LOW,"@@@ monitor_threads: Average used threads %d, threshold %d, available threads %d\n",average,threshold, total_threads);
			// set last thread status as KILLED and remove it in next monitor execution
			//set previous next as null!!
			if(total_threads < threshold){
				for(i=0;i<threshold - total_threads; i++)
					create_new_thread();
			}
			pthread_mutex_unlock(&available_queue_mutex);
			reads = 0;
			counter = 0;
		}

		sleep(MONITOR_TIMER);
	}

}

void insert_thread(thread_queue *new){
	//do_debug(LOG_LEVEL_LOW,"insert_thread : inserting thread\n");

	//pthread_mutex_lock(&available_queue_mutex);
	//do_debug(LOG_LEVEL_LOW,"insert_thread : Acquired lock\n");

	total_threads++;
	new->id = total_threads;

	if(available_threads == NULL){
		available_threads = new;
		//new->next = new;
		new->next = NULL;
		last_thread = new;
	}
	else{
		//new->next = available_threads->next;
		//available_threads->next = new;
		new->next = NULL;
		last_thread->next = new;
		last_thread = new;
		//new->next = available_threads;
		//available_threads = new;
	}
	//pthread_mutex_unlock(&available_queue_mutex);
	//do_debug(LOG_LEVEL_LOW,"insert_thread : Released lock\n");
}

//this is this library own thread function
//the user thread function is stored in thread_inception
void * thread_func(void *ictx){
	thread_queue * ctx = (thread_queue *) ictx;

	//do_debug(LOG_LEVEL_LOW,"---[%d] thread_func: I was created!\n", ctx->id);
	//This is done for initialization purposes!
	pthread_mutex_lock(&(ctx->status_mutex));
	ctx->status = THREAD_AVAILABLE;
	pthread_mutex_unlock(&(ctx->status_mutex));
	while(1){
		pthread_mutex_lock(&(ctx->status_mutex));
		//Only perfoms stuff when set to status
		//note that wait automatically and atomically unlocks the mutex!!
		//do_debug(LOG_LEVEL_LOW,"---[%d] thread_func: waiting for activation!\n", ctx->id);
		pthread_cond_wait(&(ctx->status_cond),&(ctx->status_mutex)); //status is = 1

		//do_debug(LOG_LEVEL_LOW,"---[%d] thread_func: status_mutex acquired\n", ctx->id);

		if(ctx->status != THREAD_NOT_AVAILABLE){
			//do_debug(LOG_LEVEL_LOW,"[%d] thread_func: was not set to running!\n", ctx->id);
			continue;
		}

		//do_debug(LOG_LEVEL_LOW,"---[%d] thread_func: status! Running function\n", ctx->id);

		//user defined functionality receives the thread context data
		(*thread_inception)(ctx->data);

		//do_debug(LOG_LEVEL_LOW,"---[%d] thread_func: Completed function\n", ctx->id);

		ctx->status = THREAD_AVAILABLE;

		pthread_mutex_unlock(&(ctx->status_mutex));

		//do_debug(LOG_LEVEL_LOW,"---[%d] thread_func: status_mutex released\n", ctx->id);
	}

	return NULL;
}

int run_thread(void *data){
	//if not status : create new thread
	//if status : use thread and rotate
	//set data
	//set to status
	//use pthread_cond_signal to wake thread
	thread_queue * actual = available_threads;
	int found_thread = 0;
	int new_thread = 0;
	pthread_t thread;

	static int requests=0, fails=0, oks=0, creates=0;

	//do_debug(LOG_LEVEL_LOW,"run_thread: Looking for available threads\n");

	requests++;
	//do_debug(LOG_LEVEL_LOW,"Give me the stupid thread mutex!\n");
	for(;actual != NULL;actual = actual= actual->next){
		//this is done twice because it is faster not to try a lock every time!!
		if(actual->status == THREAD_AVAILABLE){
		do_debug(LOG_LEVEL_LOW,"run_thread: trying lock on thread %d - state : %d\n", actual->id,actual->status);
			if(pthread_mutex_trylock(&(actual->status_mutex)) == 0){
				//run this thread
				
				/*if(actual->status == THREAD_NOT_READY){
					//do_debug(LOG_LEVEL_LOW,"run_thread: thread %d not ready!\n", actual->id);
					pthread_mutex_unlock(&(actual->status_mutex));
					new_thread = 1;
					continue;
				}

				if(actual->status == THREAD_NOT_AVAILABLE) {
					//do_debug(LOG_LEVEL_LOW,"[!] run_thread: thread %d was already running!\n", actual->id);
					pthread_mutex_unlock(&(actual->status_mutex));
					continue;
				}*/

				//because the check was done before locking ;)
				if(actual->status != THREAD_AVAILABLE) {
					pthread_mutex_unlock(&(actual->status_mutex));
					continue;
				}

				//do_debug(LOG_LEVEL_LOW,"run_thread: lock acquired on thread %d!\n", actual->id);
				pthread_cond_signal(&(actual->status_cond));
				//do_debug(LOG_LEVEL_LOW,"run_thread: setting thread %d to not available!\n", actual->id);
				actual->status = THREAD_NOT_AVAILABLE;
				//do_debug(LOG_LEVEL_LOW,"run_thread: setting data input to thread %d!\n", actual->id);
				actual->data = data;
				//do_debug(LOG_LEVEL_LOW,"run_thread: cond signal to thread %d!\n", actual->id);
				pthread_mutex_unlock(&(actual->status_mutex));
				//do_debug(LOG_LEVEL_LOW,"run_thread: released lock on thread %d!\n", actual->id);

				found_thread = 1;
				break;
			}
			else{
				//do_debug(LOG_LEVEL_LOW,"run_thread: thread %d was unavailable\n", actual->id);
			}
		}
		else{
			//do_debug(LOG_LEVEL_LOW,"run_thread: thread %d was unavailable\n", actual->id);
		}
	}

	if(!found_thread){
		pthread_create(&thread,NULL,thread_inception,data);
		fails++;
	}
	else
		oks++;

	do_debug(LOG_LEVEL_LOW,"run_thread: requests %d, reused %d, not reused %d [%d]\n",requests,oks,fails,total_threads);
	return 0;
}

int create_new_thread(){
	if(total_threads == NUM_MAX_THRDS){
		//do_debug(LOG_LEVEL_LOW,"create_new_queue_thread(): maximum number of threads reached!\n");
		return -1;
	}
	//do_debug(LOG_LEVEL_LOW,"create_new_queue_thread(): Creating new thread\n");
	thread_queue * new = (thread_queue *) malloc(sizeof(thread_queue));
	pthread_mutex_init(&(new->status_mutex),NULL);
	pthread_cond_init(&(new->status_cond),NULL);
	//hack to signal thread as not available until it initializes correctly
	new->status = THREAD_NOT_READY;
	insert_thread(new);
	pthread_create(&(new->thread),NULL,thread_func,(void *) new);
	//do_debug(LOG_LEVEL_LOW,"create_new_queue_thread(): Number of threads : %d\n",total_threads);
	return 0;
}

void init_threads(){
	int i;
	thread_queue *actual;
	pthread_mutex_lock(&available_queue_mutex);
	for(i=0;i<NUM_INIT_THRDS;i++){
		create_new_thread();
	}
	pthread_mutex_unlock(&available_queue_mutex);
	do_debug(LOG_LEVEL_LOW,"To the infinite loop and Beyond!!!\n");
	while(1){
		i=1;
		for(actual=available_threads;actual!=NULL;actual=actual->next){
			if(actual->status == THREAD_NOT_READY)
				i=0;
		}
		if(i)
			break;
	}
	do_debug(LOG_LEVEL_LOW,"Back to the finite world :(\n");
}

void *dummy_func(void *data){
	char *a = (char *) data;
	int i;

	/*for(i=0;i<3;i++){
		//do_debug(LOG_LEVEL_LOW,"I print: %s\n",a);
		sleep(1);
	}*/

	//sleep(5);
	//do_debug(LOG_LEVEL_LOW,"I print: %s\n",a);

	return NULL;
}

/*
int main(){
	char *str;
	int d,i;
	pthread_t thread;

	thread_inception = &dummy_func;

	do_debug(LOG_LEVEL_LOW,"#####PHASE 1\n");

	init_threads();
	sleep(10);

	pthread_create(&thread,NULL,monitor_threads,NULL);

	str = (char *) malloc(sizeof(16));
	memset(str,0,16);
	strcpy(str,"hello world 1");

	do_debug(LOG_LEVEL_LOW,"#####PHASE 2\n");

	for(d=0;d<40;d++){
		for(i=0;i<20;i++){
			run_thread((void *) str);
			//sleep(1);
		}
		//run_thread((void *) str);
		do_debug(LOG_LEVEL_LOW,"%d\n",d);
		//sleep(1);
	}

	//do_debug(LOG_LEVEL_LOW,"#####DONE\n");

	return 0;
}
*/


/*
int main(){
	char *str;
	int d,i;
	pthread_t thread;
	thread_inception = &dummy_func;

	do_debug(LOG_LEVEL_LOW,"#####PHASE 1\n");

	init_threads();
	sleep(10);

	pthread_create(&thread,NULL,monitor_threads,NULL);

	str = (char *) malloc(sizeof(16));
	memset(str,0,16);
	strcpy(str,"hello world 1");

	do_debug(LOG_LEVEL_LOW,"#####PHASE 2\n");

	for(d=0;d<40;d++){
		for(i=0;i<20;i++){
			//run_thread((void *) str);
			//sleep(1);
			pthread_create(&thread,NULL,thread_inception,(void *) str);
		
		}
		//run_thread((void *) str);
		do_debug(LOG_LEVEL_LOW,"%d\n",d);
		//sleep(1);
	}

	return 0;
}
*/

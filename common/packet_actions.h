#ifndef PACKET_ACTION_H_
#define PACKET_ACTION_H_


#include <pthread.h>
#include "packet_queue.h"
#include "utils.h"

#define PERIOD_ACTION 600 //10 minutes

//This is called within a thread in the client and server
//it should for example send the delayed packets
void *periodic_action_runner(void *args);

int delay_packet(char *packet, int nread, char *dst, int port);

void block_packet(char *packet);

//launchs periodic thread
void init_actions();

#endif
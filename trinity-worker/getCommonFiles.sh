#!/bin/bash

echo "Copying common files to trinity-worker..."
for i in uid_monitor uid_tools tun_tools utils worker_pool db_connector rule_set packet_queue packet_actions
do
		echo "--->Copy $i to ../trinity-worker/"
		cp ../common/$i.* .
done
#include "tun_server.h"

//Keeps the actual message uid
//on multiple client this is not global
int actual_uid;
//Keeps the actual message uid
//on multiple client this is not global
int actual_userid;

//int stats_cached = 0;
//int stats_not_cached = 0;

//Connection caching stats
int stats_con_cached=0, stats_con_not_cached=0;
//Uid caching stats
int stats_uid_cached=0, stats_uid_not_cached=0;
//Unparsable packets
int stats_broken_packets=0;
//screen state (on/off)
int stats_screen_state=1;
//connection type (wifi/3g)
int stats_con_type = -1;
//rrc state
char rrc_state[RRC_STATE_LENGTH];

char wifi_iface[IFNAMSIZ] = "";
char mobile_iface[IFNAMSIZ] = "";

/*
 typedef struct c_msg{
    int protocol;
    int size;
    char *message;
} control_message;

int traffic_port;
int control_port;
int scheduler_port;
*/

void scheduler_register(char *sch_ip, int trafport, int ctrport, int schport) {
  char buf[REGWORKER_SIZE];

  do_debug(LOG_LEVEL_LOW,
      "--->scheduler_register : Will register on scheduler {%s} and port %d\n",
      sch_ip, SCHEDULER_PORT);

  memset(buf,0,REGWORKER_SIZE);

  //scheduler communication here
  int sch_socket = create_tcp_client_socket(sch_ip,SCHEDULER_PORT);

  if(sch_socket < 0){
    do_debug(LOG_LEVEL_LOW,"[ERROR] : scheduler_register - error creating tcp socket\n");
    exit(0);
  }

  sprintf(buf,"%d|%d|%d",traffic_port,control_port,scheduler_port);

  do_debug(LOG_LEVEL_LOW,"scheduler_register : registering worker - %s\n",buf);

  if(send_proto_msg(sch_socket, PROTO_REGWORKER, buf) != 0)
    do_debug(LOG_LEVEL_LOW,"scheduler_register : failed sending message\n");

  close(sch_socket);
}

void process_applications(control_message *msg){
  int read_size = 0;
  char *begin, *end;
  char app_name[64]; //TODO: hardcoded value
  int uid;
  int is_uid = 0;
  int i,j;

  do_debug(LOG_LEVEL_LOW,"Processing applications\n");
  int userid = -1;
  do_debug(LOG_LEVEL_LOW,"Message received %s \n",msg->message);
  for(i=j=0; read_size != msg->size; j++,read_size++){

    //this is assuming there is always at least one app, which should be ok
    if((msg->message)[j] == '\0'){
      //This is the last element
      uid = atoi(msg->message + i);
      //Insert in uid_monitor hash table
      do_debug(LOG_LEVEL_LOW,"process_applications : Storing %s|%d\n",app_name,uid);
      insert_if_new_uid_hash(userid, uid,app_name);
      break;
    }
    if((msg->message)[j] == '|'){
      msg->message[j] = '\0';

      if(userid == -1){
                userid = atoi(msg->message);
                if(userid < 0 || userid > 256)
                      puts("Unexpected user id!!!!!! <--------------------------------");
                 i=j+1;
      } else {
                if(is_uid){
                  uid = atoi(msg->message + i);
                  //Insert in uid_monitor hash table
                  do_debug(LOG_LEVEL_LOW,"process_applications : Storing %s|%d\n",app_name,uid);
                  insert_if_new_uid_hash(userid, uid,app_name);
                  i=j+1;
                }
                else{
                  memset(app_name,0,64);
                  strcpy(app_name,msg->message + i);
                  i=j+1;
                }

                is_uid = (is_uid +1) %2;
                //do_debug(LOG_LEVEL_LOW,"is_uid = %d\n",is_uid);
      }
    }

  }
}


void *statistics_updater(void *arg){
  uid_node** top;
  int *traffic;

  db_connect("log.txt");

  while(1){
    top = uid_retrieve_top_apps(20);
    traffic = get_traffic_stats();

    db_save_uid_states(top,20,0);
    db_save_traffic_states(traffic);
    db_save_system_states();
    db_save_cached_states(stats_con_cached,stats_con_not_cached, stats_uid_cached, stats_uid_not_cached, stats_broken_packets);

    free(top);
    free(traffic);

    //uid_print_top_apps(10);
  
    sleep(STATS_INTERVAL);
  }

  db_disconnect();
}

int set_client_ip(struct sockaddr_storage remoteaddr){
  //cleans the ip of the client
        memset(client_ip,0,INET6_ADDRSTRLEN);

        //Sets the ip of the client (client_ip)
        if(inet_ntop(remoteaddr.ss_family,get_in_addr((struct sockaddr*)&remoteaddr),client_ip, INET6_ADDRSTRLEN) == NULL){
          perror("set_client_ip : ERROR inet_ntop");
          //continue;
          //exit(4);
          return 0;
        }

        do_debug(LOG_LEVEL_LOW,"set_client_ip : Setted client_ip to %s\n",client_ip);


        bzero((char *) &client,sizeof(client));
        client.sin_family=AF_INET;
        //client.sin_port=htons(traffic_port);
        
        struct sockaddr *sa = (struct sockaddr *) &remoteaddr;

        //Setting client port
        if (sa->sa_family == AF_INET)
          client_port = ((struct sockaddr_in *)sa)->sin_port;
        else
          client_port = ((struct sockaddr_in6 *)sa)->sin6_port;

        client.sin_addr = ((struct sockaddr_in *)sa)->sin_addr;
        client.sin_port = client_port;

        int chosen_port = (int) ntohs(client.sin_port);

        do_debug(LOG_LEVEL_LOW,"set_client_ip : Setting port to %d\n",chosen_port);

        do_debug(LOG_LEVEL_LOW,"set_client_ip : Registered client ip : %s\n", client_ip);

        return 1;
}

void *client_listen_control_messages(void *arg){
    fd_set read_fds;  // temp file descriptor list for select()
    int fdmax;        // maximum file descriptor number
    
    int listener;     // listening socket descriptor
    int newfd;        // newly accept()ed socket descriptor
    struct sockaddr_storage remoteaddr; // client address
    socklen_t addrlen;

    char buf[CLIENT_CONTROL_MSG_LEN];

    listener = create_tcp_server_socket(control_port);
    
    for(;;) {
      FD_ZERO(&read_fds);
      FD_SET(listener,&read_fds);

      if (select(listener+1, &read_fds, NULL, NULL, NULL) == -1) {
            do_debug(LOG_LEVEL_LOW,"client_listen_control_messages : error select [XXX]\n");
            //exit(4);
            continue;
      }

      addrlen = sizeof remoteaddr;
      newfd = accept(listener,(struct sockaddr *)&remoteaddr,&addrlen);
      if (newfd == -1) {
        do_debug(LOG_LEVEL_LOW,"client_listen_control_messages : error accept [XXX]\n");
      } else {

        // handle data from a client
        control_message *msg = receive_all_variable_size(newfd);

        if(msg != NULL){
        //if(receiveall(newfd,buf,CLIENT_CONTROL_MSG_LEN)!=-1){
            //

            //Protocol|Size|Data(Size)
            switch(msg->protocol){
              case PROTO_IPCHANGE:
                  if(!set_client_ip(remoteaddr))
                    continue;
                  //This a NOP since we already check the ip and port ;)
                  do_debug(LOG_LEVEL_LOW,"client_listen_control_messages : Received : %s\n",msg->message);
                  if(strncmp(msg->message,"eth",3)==0 || strncmp(msg->message,"wlan",4)==0){
                    stats_con_type = STATS_CON_TYPE_WIFI;
                    strcpy(wifi_iface,msg->message);
                  }
                  else{
                    stats_con_type = STATS_CON_TYPE_MOBILE;
                    strcpy(mobile_iface,msg->message);
                  }
              break;
              case PROTO_PACKAGES:
                  process_applications(msg);
              break;
              default:
                do_debug(LOG_LEVEL_LOW,"One of those hidden messages that should never appear: I <3 (.)(.)\n");
            }

            //TODO : read following comment
            //In case there is some kind of confirmation message (not really needed, its tcp) <- i was naive when i wrote this ahahah
            //memset(buf,0,CLIENT_CONTROL_MSG_LEN);
            //strcpy(buf,"Ok!");
            //if(sendall(newfd, buf, CLIENT_CONTROL_MSG_LEN)==-1)
              //  do_debug(LOG_LEVEL_LOW,"client_listen_control_messages : could not sendall\n");

            free(msg->message);
            free(msg);
        }
        else{
            do_debug(LOG_LEVEL_LOW,"client_listen_control_messages : ERROR could not receiveall\n");
        }
        
        close(newfd);         
                        
      }
    }
}

int process_uid(char *buffer_pointer){
  int i;
  char app_name[33+1]; //TODO: hardcoded value
  int proto;
  int len = strlen(buffer_pointer);
  int pos = 0;

      for(i=0; i<len; i++)
        if(buffer_pointer[i]=='|')
          buffer_pointer[i]='\0';
      
      proto = atoi(buffer_pointer);
      do_debug(LOG_LEVEL_LOW,"proto = %d\n",proto);
      pos += strlen(buffer_pointer) + 1;

      stats_screen_state = atoi(buffer_pointer + pos);
      do_debug(LOG_LEVEL_LOW,"screen = %d\n",stats_screen_state);
      pos += strlen(buffer_pointer + pos) + 1;

      actual_uid = atoi(buffer_pointer + pos);
      do_debug(LOG_LEVEL_LOW,"uid = %d\n",actual_uid);
      pos += strlen(buffer_pointer + pos) + 1;

      strcpy(rrc_state,buffer_pointer + pos);
      do_debug(LOG_LEVEL_LOW,"rrc = %s\n",rrc_state);
      pos += strlen(buffer_pointer + pos) + 1;

      return pos;
      //return j+1;
}

prg_node *cache_connection(prg_node *node){
  do_debug(LOG_LEVEL_LOW,"Reverting src/dst and caching connection\n");
  //Note that src and destination are interchanged! (Also ports)
  //Also the uid is the actual_uid, taken from the received package
 
  return prg_cache_update_by_attributes(actual_uid,node->proto,node->dst,node->src,node->dst_port,node->src_port);
}

prg_node *retrieve_connection(prg_node *node){

  //do_debug(LOG_LEVEL_LOW,"Retrieving connection\n");
  prg_node *cached_node = match_prg_node(node->proto,node->src,node->dst,node->src_port,node->dst_port);
  if(cached_node != NULL){
    //do_debug(LOG_LEVEL_LOW,"Connection WAS cached\n");
    //print_prg_node(cached_node);
    stats_con_cached++;
  }
  else{
    //do_debug(LOG_LEVEL_LOW,"Connection was NOT cached\n");
    stats_con_not_cached++;
  }

  do_debug(LOG_LEVEL_LOW,"Cached - %d | Not cached - %d\n",stats_con_cached,stats_con_not_cached);
  return cached_node;
}

void init_threads(){
  pthread_t pth;

  pthread_t web_socket_server;
  //  if(pthread_create(&web_socket_server,NULL,web_socket_thread,NULL)!=0)
  //      perror("tun_server error: web_socket_server pthread");

  if(pthread_create(&pth,NULL,client_listen_control_messages,NULL)!=0)
            perror("tun_server error: client_listen_control_messages pthread");
  if(pthread_create(&pth,NULL,statistics_updater,NULL)!=0)
            perror("tun_server error: statistics_updater pthread");

  init_actions();

  //if(pthread_create(&pth,NULL,web_connector_thread,NULL)!=0)
  //          perror("tun_server error: web_connector_thread pthread");
}

char *read_properties(){
  FILE * fp;
       char * line = NULL;
       size_t len = 0;
       ssize_t read;

       fp = fopen("properties.config", "r");
       if (fp == NULL)
           exit(EXIT_FAILURE);

       while ((read = getline(&line, &len, fp)) != -1) {
           printf("Retrieved line of length %zu :\n", read);
           printf("%s", line);
       }

       fclose(fp);

       return line;
       //if (line)
       //    free(line);
}

//Register the client using last byte of the client ip as index to  an array with socket addresses
void register_client(char* client_ip){
    char * ip1 = client_ip;
    do_debug(LOG_LEVEL_LOW,"Client ip %s:\n",((ip1==NULL)?"null":ip1));
    do_debug(LOG_LEVEL_LOW,"Client ip length %d \n",strlen(ip1));

    if (strncmp(ip1, "::ffff", 6) == 0) { //::ffff ipv6 representation of ipv4
        ip1 += 7;
    }
    int valid = (strncmp(ip1,"10.",3)==0);
    int count = 0;
    for(;count<3;ip1++){
      if(*ip1=='.'){
        count++;
      }
    }
    do_debug(LOG_LEVEL_LOW,"%s \n",ip1);
    int index = atoi(ip1);
    do_debug(LOG_LEVEL_LOW,"index %d \n",index);

      //Check if the ip is 10.x.x.x
      if(valid!=1){
        do_debug(LOG_LEVEL_LOW,"Trash IP. Get out of my traffic. First byte is %d \n",valid);
      }
      else{
        actual_userid= index;
        //actual_userid = (int)(client.sin_addr.s_addr&0xFF);
        memcpy((struct sockaddr_in *) &client_map[actual_userid],&client, sizeof(struct sockaddr_in));
      }
}

struct sockaddr_in get_client(char * ip){
    int i;

    for(i=strlen(ip);i>0;i--){
        if(ip[i] == '.')
          break;
    }
    actual_userid = atoi(ip+i+1);
    do_debug(LOG_LEVEL_LOW,"Actual uid: %d \n", actual_userid);
    do_debug(LOG_LEVEL_LOW,"Incoming IP: %s \n", ip);
    return client_map[actual_userid];
}

int main(int argc, char *argv[]) {
  
  int tap_fd,client_fd;
  int maxfd;
  int nread, nwrite;
  char buffer[CLIENT_TRAFFIC_MSG_LEN];
  char web_buffer[256];
  unsigned char * buffer_pointer = buffer;
  int pos,len;
  int i,action = ACTION_NONE;
  uid_node *uid;
  char *sch_ip;
  struct sockaddr_in return_addr; 

  traffic_port = CLIENT_TRAFFIC_PORT;
  control_port = CLIENT_CONTROL_PORT;
  scheduler_port = SCHEDULER_PORT;

  memset((struct sockaddr_in *)&client_map, 0, sizeof(client_map)); //Initialize

  if(argc == 4){
    do_debug(LOG_LEVEL_LOW,"Setting scheduler ip and ports.\n");
    traffic_port = atoi(argv[1]);
    control_port = atoi(argv[2]);
    scheduler_port = atoi(argv[3]);
    sch_ip = malloc(20);
    strcpy(sch_ip,read_properties()); //TODO: read_properties return should be released
    scheduler_register(sch_ip,traffic_port, control_port, scheduler_port);
  }
  else{
      do_debug(LOG_LEVEL_LOW,"Default ips and ports used.\n");
      sch_ip = malloc(20);
      strcpy(sch_ip,read_properties()); //TODO: read_properties return should be released
      scheduler_register(sch_ip,traffic_port, control_port, scheduler_port);
  }

  /* initialize tun/tap interface */
  if ( (tap_fd = tun_alloc(IF_PATH,IF_NAME, IFF_TUN | IFF_NO_PI)) < 0 ) {
    my_err("Error connecting to tun/tap interface %s!\n", IF_NAME);
    exit(1);
  }

  if ((client_fd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
    perror("LOGGER Error: socket");
  }

  bzero((char *) &server, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_port = htons(traffic_port);
  server.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(client_fd,(struct sockaddr *)&server,sizeof(server))<0){ 
       perror("bind()"); 
       exit(1);
  } 

  init_threads();
  sleep(30);
  /* use select() to handle two descriptors at once */
  maxfd = (tap_fd > client_fd)?tap_fd:client_fd;

  FILE *fd = fopen("ip_port_tuples.log","w");
	if(fd ==NULL){
		do_debug(LOG_LEVEL_HIGH,"Error opening file");	
	}
  else{
	fprintf(fd,"#13#traff_dir:1 pkt_action:2 scr_state:3 valid_uid:4 app_pkg:5 proto:6 server_ip:7 client_ip:8 server_port:9 client_port:10 bytes_written:11 network:12 rrc_state:13\n");
	
  }
fclose(fd);
   /*
    //THREAD_POOL CODE----------------
    do_debug(LOG_LEVEL_LOW,"Initializing... please wait\n");
    init_threads();
    sleep(10);
    do_debug(LOG_LEVEL_LOW,"Pool of threads initialized!\n");
    thread_inception = &handle_client;
    thread_ctx *ctx = NULL;
    //--------------------------------
	*/
long packet_count = 0;
  while(1) {
    int ret;
    fd_set rd_set;

    FD_ZERO(&rd_set);
    FD_SET(tap_fd, &rd_set); FD_SET(client_fd, &rd_set);

    ret = select(maxfd + 1, &rd_set, NULL, NULL, NULL);

    if (ret < 0 && errno == EINTR){
      continue;
    }

    if (ret < 0) {
      perror("select()");
      continue;
      //exit(1);
    }

    uid = NULL;
    action = ACTION_NONE;

    if(FD_ISSET(tap_fd, &rd_set)) {
      /* data from tun/tap: just read it and write it to the network */
      
      do_debug(LOG_LEVEL_LOW,"########TUN - WEB->CLIENT##########\n");

      nread = cread(tap_fd, buffer, CLIENT_TRAFFIC_MSG_LEN);
      //do_debug(LOG_LEVEL_LOW,"Read %d bytes\n", nread);

      if (nread <= 0) {
        stats_broken_packets++;
        continue;
      }

      do_debug(LOG_LEVEL_LOW,"Packet size: %d\n",nread);

      //Processes packet and returns a representation as a uid monitor node
      prg_node *node = process_packet(buffer,nread);

      //Wrapper method that calls functions to retrieve connection
      prg_node *cached_node = retrieve_connection(node);

      //Get the sockaddr_in corresponding to the tun ip of the destination
      return_addr= get_client(node->dst);

      if(cached_node != NULL){
        uid = get_uid_node(actual_userid, cached_node->uid);
        do_debug(LOG_LEVEL_LOW,"node cached ->");
        print_prg_node(cached_node);
        if(uid != NULL){
          stats_uid_cached++;
          do_debug(LOG_LEVEL_LOW,"uid cached -> %d|%s\n",cached_node->uid,uid->name);

          action = get_action_for_rule(uid->rules, uid->uid, stats_con_type, node->proto, STATS_DIR_INCOMING, stats_screen_state);

          do_debug(LOG_LEVEL_LOW,"Action is %d\n", action);
        }
        else{
          stats_uid_not_cached++;
          action = ACTION_NONE;
          do_debug(LOG_LEVEL_LOW,"uid not cached!\n");
        }
      }
      else{
        do_debug(LOG_LEVEL_LOW,"node not cached!\n"); //counter already incremented in retrieve_connection
      }

      switch (action) //Check the Protocol and do accordingly...
      {
      case ACTION_BLOCK:
        do_debug(LOG_LEVEL_LOW,"Blocking..");
        block_packet(buffer);

        //------------------------------------------------------------< EXIT POINT
        do_debug(LOG_LEVEL_HIGH,"WEB->CLIENT,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
        "ACTION_BLOCK", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
        (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
        node->proto, node->src, node->dst, node->src_port, node->dst_port, 
        0, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state);
        if (node != NULL)
          free(node);

        continue;
        break;
      case ACTION_ALLOW:
        break;
      case ACTION_DELAY:
        do_debug(LOG_LEVEL_LOW,"Delaying..");
        delay_packet(buffer,nread,client_ip, client_port);
        //------------------------------------------------------------< EXIT POINT
        do_debug(LOG_LEVEL_HIGH,"WEB->CLIENT,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
        "ACTION_DELAY", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
        (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
        node->proto, node->src, node->dst, node->src_port, node->dst_port, 
        0, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state);
        if (node != NULL)
          free(node);

        continue;
        break;
      }
      
      if(return_addr.sin_port==0){
        do_debug(LOG_LEVEL_HIGH,"Invalid client redirection, stale traffic (Uninitialized territory)\n");
        continue;
      }

      //do_debug(LOG_LEVEL_LOW,"TAP2NET: Read %d bytes from the tap interface\n", nread);
      //client is the specific client
      nwrite = sendto(client_fd,buffer,nread,0,(struct sockaddr *)&return_addr,sizeof(return_addr)); 
      //do_debug(LOG_LEVEL_LOW,"Written %d bytes\n", nwrite);
      //do_debug(LOG_LEVEL_LOW,"TAP2NET: Written %d bytes to the network\n", nwrite);

      if(nwrite > 0 && uid != NULL){
        uid->MB_sent += ((float) nwrite) / 1024; //in KB
      }
      else {
        if (nwrite <= 0)
          stats_broken_packets++;
      }

      //------------------------------------------------------------< EXIT POINT
      if(node!= NULL){
      do_debug(LOG_LEVEL_HIGH,"WEB->CLIENT,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
        "ACTION_ALLOW", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
        (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
        node->proto, node->src, node->dst, node->src_port, node->dst_port, 
        nwrite, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state);
	//Write data to file for post-processing using awk
	FILE *fd = fopen("ip_port_tuples.log","a+");
	if(fd ==NULL){
		do_debug(LOG_LEVEL_HIGH,"Error opening file");	
	}
	else{
	packet_count++;
		fprintf(fd,"WEB->CLIENT %s %s %d %s %d %s %s %d %d %d %s %s %ld\n",
        "ACTION_ALLOW", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
        (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
        node->proto, node->src, node->dst, node->src_port, node->dst_port, 
        nwrite, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state, packet_count)	;
	}
	fclose(fd);
      if(is_web_server_up() && uid != NULL){
            memset(web_buffer,0,256);
            sprintf(web_buffer,"%s|%s|%d|%d|%d|%d|%d|%d|%d",uid->name,rrc_state,action, uid->uid, stats_con_type, node->proto, STATS_DIR_INCOMING, stats_screen_state,nwrite);
            send_to_webserver(web_buffer);
      }

          free(node);
      }
    }

    //INCOMING TRAFFIC (CLIENT->ETH0->PROXY->ETH0->WEB)
    if(FD_ISSET(client_fd, &rd_set)) {
      do_debug(LOG_LEVEL_LOW,"#########eth0 - CLIENT TO WEB###########\n");

      socklen_t slen = sizeof(struct sockaddr_in);
      //Receive packet from CLIENT (note last two arguments, NULL NULL would be from anyone)
      //note that server is ANYADDR
      nread = recvfrom(client_fd,buffer,CLIENT_TRAFFIC_MSG_LEN + 34 + 4,0,(struct sockaddr *)&client,&slen); //TODO: hardcoded
      //do_debug(LOG_LEVEL_LOW,"Read %d bytes\n", nread);

      if (nread <= 0) {
        stats_broken_packets++;
        continue;
      }

      do_debug(LOG_LEVEL_LOW,"Packet size: %d\n",nread);

      buffer_pointer = buffer;

      //Reads the protocol message and sets the actual_uid
      pos = process_uid(buffer_pointer);

      //Processes packet and returns a representation as a uid monitor node
      prg_node *node = process_packet(buffer_pointer+pos,nread - pos);
      register_client(node->src);
      if(node!= NULL){
        print_prg_node(node);
        //Wrapper method that calls functions to cache connection
        cache_connection(node);

        uid = get_uid_node(actual_userid, actual_uid);
        
        if(uid != NULL){
            stats_uid_cached++;
            do_debug(LOG_LEVEL_LOW,"uid cached -> %d|%s\n",actual_uid,uid->name);

            action = get_action_for_rule(uid->rules, uid->uid, stats_con_type, node->proto, STATS_DIR_OUTGOING, stats_screen_state);
        }
        else{
            stats_uid_not_cached++;
            action = ACTION_NONE;
            do_debug(LOG_LEVEL_LOW,"uid not cached!\n");
        }
      }
      //do_debug(LOG_LEVEL_LOW,"CLIENT2WEB: Read %d bytes from the network\n", nread);

      switch (action) //Check the Protocol and do accordingly...
      {
        case ACTION_BLOCK:
          block_packet("");
          if(node!=NULL){
          //------------------------------------------------------------< EXIT POINT
          do_debug(LOG_LEVEL_HIGH,"CLIENT->WEB,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
            "ACTION_BLOCK", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
            (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
            node->proto, node->src, node->dst, node->dst_port,node->src_port,  
            0, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state);
	//Write data to file for post-processing using awk
	  FILE *fd = fopen("ip_port_tuples.log","a+");
	if(fd ==NULL){
		do_debug(LOG_LEVEL_HIGH,"Error opening file");	
	}
	else{
	packet_count++;
		fprintf(fd,"CLIENT->WEB %s %s %d %s %d %s %s %d %d %d %s %s %ld\n",
        "ACTION_ALLOW", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
        (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
        node->proto, node->dst, node->src, node->src_port, node->dst_port, 
        nwrite, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state, packet_count)	;
	}
	fclose(fd);
            free(node);
          }
          else
            do_debug(LOG_LEVEL_HIGH,"node=NULL\n");
          continue;
        break;
        case ACTION_ALLOW:
        break;
        case ACTION_DELAY:
          //delay_packet(buffer,nread); ONLY SUPPORTED in worker/client communication
          //continue;
        break;
      }

      /* now buffer[] contains a full packet or frame, write it into the tun/tap interface */ 
      nwrite = cwrite(tap_fd, buffer_pointer+pos, nread - pos);
      //do_debug(LOG_LEVEL_LOW,"Written %d bytes\n", nwrite);
      if(nwrite > 0 && uid != NULL){
        uid->MB_received += ((float) nwrite) / 1024; //in KB
      }
      else {
        if (nwrite <= 0)
          stats_broken_packets++;
      }

      if(node!=NULL){
      //------------------------------------------------------------< EXIT POINT
      do_debug(LOG_LEVEL_HIGH,"CLIENT->WEB,%s,%s,%d,%s,%d,%s,%s,%d,%d,%d,%s,%s\n",
        "ACTION_ALLOW", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
        (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
        node->proto, node->src, node->dst, node->src_port, node->dst_port, 
        nwrite, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state);
	//Write data to file for post-processing using awk	
	FILE *fd = fopen("ip_port_tuples.log","a+");
	if(fd ==NULL){
		do_debug(LOG_LEVEL_HIGH,"Error opening file");	
	}
	else{
		fprintf(fd,"CLIENT->WEB %s %s %d %s %d %s %s %d %d %d %s %s %ld\n",
        "ACTION_ALLOW", (stats_screen_state==STATS_SCREEN_STATE_OFF)?"SCREEN_OFF":"SCREEN_ON", 
        (uid==NULL)?-1:uid->uid, (uid==NULL)?"unknow_uid":uid->name,
        node->proto, node->dst, node->src, node->dst_port,node->src_port,  
        nwrite, (stats_con_type==STATS_CON_TYPE_MOBILE)?"3G":"WIFI", rrc_state, packet_count)	;
	}
	fclose(fd);
        free(node);
      }
      else
        do_debug(LOG_LEVEL_HIGH,"node=NULL\n");

      if(is_web_server_up() && uid != NULL){
            memset(web_buffer,0,256);
            sprintf(web_buffer,"%s|%s|%d|%d|%d|%d|%d|%d|%d",uid->name,rrc_state,action, uid->uid, stats_con_type, node->proto, STATS_DIR_OUTGOING, stats_screen_state,nwrite);
            send_to_webserver(web_buffer);
      }
    }
    //INCOMING TRAFFIC (WEB->ETH0->PROXY->ETH0->CLIENT)
  }
  
  return(0);
}

#include "web_socket_api.h"

int clientSocket = -1;
int client_ready = 0;

//in theory this should have a lock
void prepare_webserver(int client){
    do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : Redirecting traffic to webserver\n");
    clientSocket = client;
    client_ready = 1;
}

void close_webserver(){
    do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : Stopping traffic redirection to webserver\n");
    clientSocket = -1;
    client_ready = 0;
}

int is_web_server_up(){
    return client_ready;
}

int safeSend(int s_client, const uint8_t *buffer, size_t bufferSize)
{
    ssize_t written = send(s_client, buffer, bufferSize, 0);
    if (written == -1) {
        close(s_client);
        perror("[web_socket_thread] : send failed");
        return EXIT_FAILURE;
    }
    if (written != bufferSize) {
        close(s_client);
        perror("[web_socket_thread] : written not all bytes");
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

void init_worker(int s_client){
    uint8_t buffer[WEB_BUF_LEN];
    memset(buffer, 0, WEB_BUF_LEN);
    size_t readedLength = 0;
    size_t frameSize = WEB_BUF_LEN;
    enum wsState state = WS_STATE_OPENING;
    uint8_t *data = NULL;
    size_t dataSize = 0;
    enum wsFrameType frameType = WS_INCOMPLETE_FRAME;
    struct handshake hs;
    nullHandshake(&hs);
    
    #define prepareBuffer frameSize = WEB_BUF_LEN; memset(buffer, 0, WEB_BUF_LEN);
    #define initNewFrame frameType = WS_INCOMPLETE_FRAME; readedLength = 0; memset(buffer, 0, WEB_BUF_LEN);
    
    while (frameType == WS_INCOMPLETE_FRAME) {
        ssize_t readed = recv(s_client, buffer+readedLength, WEB_BUF_LEN-readedLength, 0);
        if (!readed) {
            close(s_client);
            perror("[web_socket_thread] : recv failed");
            return;
        }

        readedLength+= readed;
        assert(readedLength <= WEB_BUF_LEN);

        if (state == WS_STATE_OPENING) {
            frameType = wsParseHandshake(buffer, readedLength, &hs);
        } else {
            frameType = wsParseInputFrame(buffer, readedLength, &data, &dataSize);
        }
        
        if ((frameType == WS_INCOMPLETE_FRAME && readedLength == WEB_BUF_LEN) || frameType == WS_ERROR_FRAME) {
            if (frameType == WS_INCOMPLETE_FRAME)
                do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : buffer too small");
            else
                do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : error in incoming frame\n");
            
            if (state == WS_STATE_OPENING) {
                prepareBuffer;
                frameSize = sprintf((char *)buffer,
                                    "HTTP/1.1 400 Bad Request\r\n"
                                    "%s%s\r\n\r\n",
                                    versionField,
                                    version);
                safeSend(s_client, buffer, frameSize);
                break;
            } else {
                prepareBuffer;
                wsMakeFrame(NULL, 0, buffer, &frameSize, WS_CLOSING_FRAME);
                if (safeSend(s_client, buffer, frameSize) == EXIT_FAILURE)
                    break;
                state = WS_STATE_CLOSING;
                initNewFrame;
            }
        }
        
        if (state == WS_STATE_OPENING) {
            assert(frameType == WS_OPENING_FRAME);
            if (frameType == WS_OPENING_FRAME) {
                // if resource is right, generate answer handshake and send it
                if (strcmp(hs.resource, "/echo") != 0) {
                    frameSize = sprintf((char *)buffer, "HTTP/1.1 404 Not Found\r\n\r\n");
                    if (safeSend(s_client, buffer, frameSize) == EXIT_FAILURE)
                        break;
                }
            
                prepareBuffer;
                wsGetHandshakeAnswer(&hs, buffer, &frameSize);
                if (safeSend(s_client, buffer, frameSize) == EXIT_FAILURE)
                    break;
                state = WS_STATE_NORMAL;
                initNewFrame;

                do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : Connection handshake done!\n");
                prepare_webserver(s_client);
                //return;
            }
        } else {
            if (frameType == WS_CLOSING_FRAME) {
                do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : Will close connection\n");
                close_webserver();
                if (state == WS_STATE_CLOSING) {
                    break;
                } else {
                    prepareBuffer;
                    wsMakeFrame(NULL, 0, buffer, &frameSize, WS_CLOSING_FRAME);
                    safeSend(s_client, buffer, frameSize);
                    break;
                }
            } else if (frameType == WS_TEXT_FRAME) {
                uint8_t *recievedString = NULL;
                recievedString = malloc(dataSize+1);
                assert(recievedString);
                memcpy(recievedString, data, dataSize);
                recievedString[ dataSize ] = 0;
                
                prepareBuffer;
                wsMakeFrame(recievedString, dataSize, buffer, &frameSize, WS_TEXT_FRAME);
                if (safeSend(s_client, buffer, frameSize) == EXIT_FAILURE)
                    break;
                initNewFrame;
            }
        }
    } // read/write cycle
    
    close(s_client);
}

int send_to_webserver(char *buf){
    uint8_t buffer[WEB_BUF_LEN];
    memset(buffer, 0, WEB_BUF_LEN);
    size_t readedLength = 0;
    size_t frameSize = WEB_BUF_LEN; 
    int ret;

    if(!client_ready){
        do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : Client was not ready!\n");
        return;
    }

    wsMakeFrame(buf, strlen(buf), buffer, &frameSize, WS_TEXT_FRAME);
    ret= safeSend(clientSocket,buffer, frameSize); //note that safeSend closes socket if needed
    if(ret == EXIT_FAILURE)
        close_webserver();

    return ret;
}

void *web_socket_thread()
{
    int listenSocket = socket(AF_INET, SOCK_STREAM, 0);
    int s_client;
    int value = 151;
    char buf[30];

    bzero(buf,30);

    if (listenSocket == -1) {
        error("[web_socket_thread] : create socket failed");
    }
    
    struct sockaddr_in local;
    memset(&local, 0, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = INADDR_ANY;
    local.sin_port = htons(PORT);
    if (bind(listenSocket, (struct sockaddr *) &local, sizeof(local)) == -1) {
        error("[web_socket_thread] : bind failed");
    }
    
    if (listen(listenSocket, 1) == -1) {
        error("[web_socket_thread] : listen failed");
    }
    do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : opened %s:%d\n", inet_ntoa(local.sin_addr), ntohs(local.sin_port));
    
    while (1) {
        struct sockaddr_in remote;
        socklen_t sockaddrLen = sizeof(remote);
        s_client = accept(listenSocket, (struct sockaddr*)&remote, &sockaddrLen);
        if (s_client == -1) {
            error("[web_socket_thread] : accept failed");
        }
        
        do_debug(LOG_LEVEL_LOW,"[web_socket_thread] : Client connecting %s:%d\n", inet_ntoa(remote.sin_addr), ntohs(remote.sin_port));
        
        init_worker(s_client);
    }
    
    close(listenSocket);
    return EXIT_SUCCESS;
}

/*
int main(){
    pthread_t web_socket_server;
    if(pthread_create(&web_socket_server,NULL,web_socket_thread,NULL)!=0)
        exit(-1);

    while(1){
        send_to_webserver("hello");
        sleep(1);
    }

    return 0;
}
*/


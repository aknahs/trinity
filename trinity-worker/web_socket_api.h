#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#include "websockets/websocket.h"
#include "utils.h"

#define PORT 8088
#define WEB_BUF_LEN 0xFFFF

//creates a websocket server that establishes connections
void *web_socket_thread();
//sends traffic to webserver client IF possible
//returns EXIT_FAILURE if not possible
int send_to_webserver(char *buf);
#ifndef TUN_SERVER_H_
#define TUN_SERVER_H_

/*
 *------------------------------------
 * Auxiliary tools for tun_client
 * C0D3D by 4knahs (www.aknahs.pt)
 * this was easier than it sounded
 *
 *------------------------------------
 */

#include "utils.h"
#include "tun_tools.h"
#include "worker_pool.h" //to get the worker structure
#include "uid_monitor.h"
#include "db_connector.h"
#include "rule_set.h"
#include "web_socket_api.h"
#include <pthread.h>

//tun interface name
#define IF_NAME "tun3"
#define IF_PATH "/dev/net/tun"

#define SCHEDULER_IP "46.137.143.180"
#define SCHEDULER_PORT 9036

//Client monitoring notification macros
#define CLIENT_CONTROL_MSG_LEN 4096
#define CLIENT_CONTROL_PORT 55560

//Client traffic notification macros
#define CLIENT_TRAFFIC_PORT 55556
#define CLIENT_TRAFFIC_MSG_LEN 4096

//Time in between statistical saves (seconds)
#define STATS_INTERVAL 10

#define RRC_STATE_LENGTH 24

#define MAX_CLIENTS 256

char client_ip[INET6_ADDRSTRLEN];
in_port_t client_port;
struct sockaddr_in client,server;

struct sockaddr_in client_map[MAX_CLIENTS];  // Structure mapping the client ip to socket address

int traffic_port;
int control_port;
int scheduler_port;

/*
Naming convention : (module it communicates with)_notify/listen_function
*/

void *client_listen_control_messages(void *arg);

#endif
echo "graphing : $1 $2 $3 $4"
gnuplot <<- EOF
                        set datafile separator ','
                        set grid front
                        #set title "Cpu consumption"
                        unset colorbox

                        
                        set xlabel "Time (hour:minutes)"
                        set ylabel "Traffic (Num packets)"
                        
                        
                        #set xrange [0:1600]
                        
                        #set term pdf
                        #set term pdfcairo size 1300,600 dashed
                       
                        #set output "speedtestVSnetworklog_memory.png"

                        # Line style for axes
                        set style line 80 lt rgb "#808080"
                        # Line style for grid
                        set style line 81 lt 0  # dashed
                        set style line 81 lt rgb "#808080"  # grey

                        set grid back linestyle 81
                        set border 3 back linestyle 80 # Remove border on top and right.  These
                                     # borders are useless and make it harder
                                     # to see plotted lines near the border.
                            # Also, put it in grey; no need for so much emphasis on a border.
                        #set xtics nomirror
                        #set ytics nomirror

                        #set style data lines

                        set style line 1 lc rgb "#A00000" lw 2 lt 1 #pt 1
                        set style line 2 lc rgb "#00A000" lw 2 lt 2 #pt 6
                        set style line 3 lc rgb "#5060D0" lw 2 lt 3 #pt 2
                        set style line 4 lc rgb "#F25900" lw 2 lt 4 #pt 9
                        
                        set style fill solid
                        set boxwidth 0.5
                        set style histogram rows

                        #set xtics 120

                        set xdata time
                        set timefmt "%d %H:%M:%S" 
                        set format x "%H:%M" 
                        set xtics 1800
                        set xtics rotate by 90 offset 0,-2.0
                        set xrange ["27 19:00:00":"28 08:00:00"]
                        set logscale y

                        set term pdfcairo size 3200,600 font "Gill Sans,9" linewidth 2 #rounded
                        set output "$1.pdf"
                        plot "$2" using 1:3 ls 1 w boxes title "$3" 

                        set term pngcairo size 3200,600 font "Gill Sans,9" linewidth 2 #rounded
                        set output "$1.png"
                        plot "$2" using 1:3 ls 1 w boxes title "$3" 

                        #set xrange [0:60]
                        #set xrange ["21 04:00:00":"21 05:00:00"]
                        #set logscale y
                        #set xtics 5
                        #set output "$1-hour.pdf"
                        #set term pdfcairo size 1300,600 font "Gill Sans,9" linewidth 2 #rounded
                        #plot "$2" using 1:3 ls 1 w boxes title "$3"

                        #set term pngcairo size 1300,600 font "Gill Sans,9" linewidth 2 #rounded
                        #set output "$1-hour.png"
                        #plot "$2" using 1:3 ls 1 w boxes title "$3" 

EOF

#$1- output name (results/$4_traffic-client-web.pdf) $2- off graph $3 - on graph $4 - app name
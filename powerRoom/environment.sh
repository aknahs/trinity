!/bin/bash

#wmctrl xdotool <- dependencies
COMMAND=$1
#POSITION=$2

SSHPATH='/home/aknahs/.ssh'
SSHCMD='ssh -oStrictHostKeyChecking=no -i /home/aknahs/.ssh/traffic.pem'
USER='ubuntu'
i=1
while read myLine
do
	echo "host : $myLine"
	PC[$i]="$myLine"
	i=$(($i+1))
done < hosts.txt

echo "hosts are: ${PC[*]}"

WID=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)"| awk '{print $5}')
xdotool windowfocus $WID

xdotool key ctrl+shift+o
#wmctrl -i -a $WID
sleep 1; xdotool type --delay 1 --clearmodifiers "$SSHCMD -l $USER ${PC[1]}"; xdotool key Return;

xdotool key ctrl+shift+e
#wmctrl -i -a $WID
sleep 1; xdotool type --delay 1 --clearmodifiers "$SSHCMD -l $USER ${PC[2]}"; xdotool key Return;

xdotool key ctrl+shift+p
xdotool key ctrl+shift+p
xdotool key ctrl+shift+e
#wmctrl -i -a $WID
sleep 1; xdotool type --delay 1 --clearmodifiers "$SSHCMD -l $USER ${PC[3]}"; xdotool key Return;

#wmctrl -i -a $WID
#sleep 1; xdotool type --delay 1 --clearmodifiers "$COMMAND"; xdotool key Return;
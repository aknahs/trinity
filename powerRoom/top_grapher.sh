gnuplot <<- EOF
                        set datafile separator ','
                        set grid front
                        #set title "Cpu consumption"
                        unset colorbox

                        #set xtics 20
                        #set xlabel "Application name"
                        set ylabel "Traffic (KB)"
                        #
                        #set term pdfcairo dashed
                        
                        #set output "worker_cached_uid.png"

                        # Line style for axes
                        set style line 80 lt rgb "#808080"
                        # Line style for grid
                        set style line 81 lt 0  # dashed
                        set style line 81 lt rgb "#808080"  # grey

                        set grid back linestyle 81
                        set border 3 back linestyle 80 # Remove border on top and right.  These
                                     # borders are useless and make it harder
                                     # to see plotted lines near the border.
                            # Also, put it in grey; no need for so much emphasis on a border.
                        #set xtics nomirror
                        #set ytics nomirror

                        #set style data lines
                        #set style data histogram

                        #unset key 

                        set style line 1 lc rgb "green" #lw 2 lt 1 #pt 1
                        set style line 2 lc rgb "red" #lw 2 lt 2 #pt 6
                        set style line 3 lc rgb "orange" #lw 2 lt 3 #pt 2
                        set style line 4 lc rgb "blue" #lw 2 lt 4 #pt 9
                        

                        #set style line 1 lt 2 lc rgb "red" lw 3
                        #set style line 2 lt 2 lc rgb "orange" lw 2
                        #set style line 3 lt 2 lc rgb "yellow" lw 3
                        #set style line 4 lt 2 lc rgb "green" lw 2
                        #show style line

                        #set style line 1 lc rgb "red"
                        #set style line 2 lc rgb "blue"

                        set xtic rotate by -45 scale 0 font ",8"
                        #set logscale x

                        #set style fill solid
                        #set boxwidth 0.5
                        set xtics nomirror rotate by -45 scale 0 font ",8"
                        set style data histogram
                        set style histogram rowstacked
                        set style fill solid border -0.1
                        set boxwidth 0.3

                        set term pdfcairo font "Gill Sans,9" linewidth 2 #rounded
                        set output "$2.pdf"
                        plot "$1" u 5:xtic(2) ls 1 title "Screen ON",\
                        '' u 6:xtic(2) ls 2 title "Screen OFF"

                        set term pngcairo font "Gill Sans,9" linewidth 2 #rounded
                        set output "$2.png"

                        plot "$1" u 5:xtic(2) ls 1 title "Screen ON",\
                        '' u 6:xtic(2) ls 2 title "Screen OFF"

                        set term pdfcairo font "Gill Sans,9" linewidth 2 #rounded
                        set output "$2-multi.pdf"
                        plot "$1" u 7:xtic(2) ls 1 title "Screen ON + 3G",\
                        '' u 8:xtic(2) ls 2 title "Screen OFF + 3G",\
                        '' u 9:xtic(2) ls 3 title "Screen ON + WIFI",\
                        '' u 10:xtic(2) ls 4 title "Screen OFF + WIFI"

                        set term pngcairo font "Gill Sans,9" linewidth 2 #rounded
                        set output "$2-multi.png"

                        plot "$1" u 7:xtic(2) ls 1 title "Screen ON + 3G",\
                        '' u 8:xtic(2) ls 2 title "Screen OFF + 3G",\
                        '' u 9:xtic(2) ls 3 title "Screen ON + WIFI",\
                        '' u 10:xtic(2) ls 4 title "Screen OFF + WIFI"


EOF
#index,name,MB,packets
#index,name,packets,count,countOn,countOff,countOn3G,countOff3G,countOnWifi,countOffWifi
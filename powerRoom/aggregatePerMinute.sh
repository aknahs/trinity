#CREATES A FILE WITH THE AGGREGATED BANDWIDTH AND PACKET COUNT PER MINUTE

awk '
BEGIN{FS = ",";count=0;countP=0;previous=0;initial = 0}
{
      if(initial == 0)
            initial = $1;
      if(previous==0){
            count = $17;
            countP = 1;
      }
      else{
            if(previous == $1){
                  count = count + $17;
                  countP = countP + 1;
            }
            else{
                  #prt = previous -initial;
                  #print prt","count;
                  print previous","count","countP;
                  count = $17;
                  countP = 1;
            }
      }
      previous = $1;

}' $1 > $2

echo "Grep high level logging (highlog-$1)"
grep LG_LVL_H $1 > highlog-$1

#set timefmt "%d %H:%M:%S" 
echo "Parsing data (highlog-$1)"
awk '
BEGIN{
	count=0;
}
{
	day=$3;
	split($4,array,":");
	hour=array[1];
	minutes=array[2];
	seconds=array[3];

	split($5,array,"|");
	line=array[2];

	split(line,cpus,",");

	print day" "hour":"minutes":"seconds","count","cpus[2]","cpus[3];
	count++;
}
END{

}
' highlog-$1 > parseddata-$1

echo "Generating graph"

gnuplot <<- EOF

			set datafile separator ','
			set grid front
			#set title "Cpu consumption"
			unset colorbox

			#set xtics 120
			set xlabel "Time (s)"
			set ylabel "Cpu usage (%)"

			#set term pdfcairo font "Gill Sans,9" dashed linewidth 2 #rounded
			set term pngcairo size 1300,600 font "Gill Sans,9" dashed #linewidth 2 #rounded			#set term pdf
			#set term pdfcairo dashed
			#set output "cpu.pdf"
			set output "$2.png"

			# Line style for axes
			set style line 80 lt rgb "#808080"
			# Line style for grid
			set style line 81 lt 0  # dashed
			set style line 81 lt rgb "#808080"  # grey

			set grid back linestyle 81
			set border 3 back linestyle 80 # Remove border on top and right.  These
			             # borders are useless and make it harder
			             # to see plotted lines near the border.
			    # Also, put it in grey; no need for so much emphasis on a border.
			#set xtics nomirror
			#set ytics nomirror

			set xdata time
            set timefmt "%d %H:%M:%S" 
            set format x "%H:%M:%S" 
            
            #set xtics nomirror rotate by -90
            set logscale y
            #set yrange [0:5]

            #set style data lines
			set style histogram rows
			set style fill solid noborder
			set boxwidth 10

            #set xtics "15:04:00","00:00:01","16:12:00"

			set style line 1 lc rgb "#A00000" lw 2 lt 1 #pt 2
			set style line 2 lc rgb "#00A000" lw 2 lt 2 #pt 6
			set style line 3 lc rgb "#5060D0" lw 2 lt 3 #pt 2
			set style line 4 lc rgb "#F25900" lw 2 lt 4 #pt 9
			

			#set style line 1 lt 2 lc rgb "red" lw 3
			#set style line 2 lt 2 lc rgb "orange" lw 2
			#set style line 3 lt 2 lc rgb "yellow" lw 3
			#set style line 4 lt 2 lc rgb "green" lw 2
			#show style line

			plot "parseddata-$1" using 1:3 w boxes ls 1 title "User CPU" #, \
				 #'' using 1:4 ls 2 title "System CPU"

			#set xrange ["4 14:30:00":"4 15:00:00"]
			#set output "$2-1430-1500.png"
			#plot "parseddata-$1" using 1:3 w boxes ls 1 title "User CPU"

			#set xrange ["4 16:30:00":"4 17:00:00"]
			#set output "$2-1630-1700.png"
			#plot "parseddata-$1" using 1:3 w boxes ls 1 title "User CPU"
EOF
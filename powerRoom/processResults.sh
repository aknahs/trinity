#-------------------------------------------#
#     SCRIPT THAT GENERATES ALL GRAPHS     	#
#											#
#-------------------------------------------#

# example input : logfile 'com.facebook.katana com.android.browser com.skype.raider com.android.vending com.facebook.orca com.google.android.talk com.rovio.angrybirds google.services jp.naver.line.android com.instagram.android com.twitter.android com.cyanogenmod.lockclock com.whatsapp com.viber.voip'

rm -rf results
mkdir results

echo "Grep high level logging (results/highlog-$1.txt)"
grep LG_LVL_H $1 > results/highlog-$1.txt

#set timefmt "%d %H:%M:%S" 
echo "Parsing data (results/parseddata-$1.txt)"
awk '
BEGIN{
}
{
	day=$3;
	split($4,array,":");
	hour=array[1];
	minutes=array[2];
	seconds=array[3];

	split($5,array,"|");
	line=array[2];
	year=array[1];

	print day" "hour":"minutes":"seconds","year","day","hour","minutes","seconds","line;
}
END{

}
' results/highlog-$1.txt > results/parseddata-$1.txt

echo "#index,name,packets,count,countOn,countOff,countOn3G,countOff3G,countOnWifi,countOffWifi" >> results/stats.txt
ix=0
#for i in katana media browser skype google-services googlequicksearchbox root
for i in $2
do
      ix=$(($ix+1))
      echo "Generating $i info"
      ./generatePerApp.sh $i results/parseddata-$1.txt
      ./plot_cdf.sh "count-$i-client-web"
      echo -n "$ix,$i," >> results/stats.txt
      echo -n "$(cat results/$i.txt | grep -V NOT_CACHED | wc -l)," >> results/stats.txt
      ./aggregateAppStats.sh results/$i.txt >> results/stats.txt
done

./top_grapher.sh results/stats.txt top_apps
#For an application name received ($1) it aggregates messages
#per minute for each wanted graphs.

echo "Copying formated_data.txt to results/"
grep $1 $2 > results/$1.txt

echo "Prepare overall----------------------------------------------------------------------"
./aggregatePerMinute.sh "results/$1.txt" "results/count-$1.txt"

echo "Prepare overall client->web----------------------------------------------------------"
grep "CLIENT->WEB" results/$1.txt > results/$1-client-web.txt
./aggregatePerMinute.sh "results/$1-client-web.txt" "results/count-$1-client-web.txt"

echo "Prepare ON/OFF for client->web-------------------------------------------------------"
grep "SCREEN_ON" results/$1-client-web.txt > results/$1-client-web-ON.txt
grep "SCREEN_OFF" results/$1-client-web.txt > results/$1-client-web-OFF.txt
./aggregatePerMinute.sh results/$1-client-web-ON.txt results/$1-client-web-count-ON.txt
./aggregatePerMinute.sh results/$1-client-web-OFF.txt results/$1-client-web-count-OFF.txt

echo "prepare overall web->client----------------------------------------------------------"
grep "WEB->CLIENT" results/$1.txt > results/$1-web-client.txt
./aggregatePerMinute.sh results/$1-web-client.txt results/count-$1-web-client.txt

echo "Prepare ON/OFF for web->client-------------------------------------------------------"
grep "SCREEN_ON" results/$1-web-client.txt > results/$1-web-client-ON.txt
grep "SCREEN_OFF" results/$1-web-client.txt > results/$1-web-client-OFF.txt
./aggregatePerMinute.sh results/$1-web-client-ON.txt results/$1-web-client-count-ON.txt
./aggregatePerMinute.sh results/$1-web-client-OFF.txt results/$1-web-client-count-OFF.txt

#$1- output name (results/$4_traffic-client-web.pdf) $2- off graph $3 - on graph $4 - app name

echo "plotOnOffSingleApp client->web-------------------------------------------------------"
./plotOnOffSingleApp.sh "results/$1_traffic-client-web" "results/$1-client-web-count-OFF.txt" "results/$1-client-web-count-ON.txt" "$1" "$1 client->web SCREEN_OFF" "$1 client->web SCREEN_ON"

echo "plotOnOffSingleApp web->client-------------------------------------------------------"
./plotOnOffSingleApp.sh "results/$1_traffic-web-client" "results/$1-web-client-count-OFF.txt" "results/$1-web-client-count-ON.txt" "$1" "$1 web->client SCREEN_OFF" "$1 web->client SCREEN_ON"

echo "plotNoScreenSingleApp web->client----------------------------------------------------"
./plotNoScreenSingleApp.sh "results/$1_traffic-web-client-noscreen" "results/count-$1-web-client.txt" "$1 web->client"

echo "plotNoScreenSingleApp overall--------------------------------------------------------"
./plotNoScreenSingleApp.sh "results/$1_traffic_inAndOut" "results/count-$1.txt" "$1 traffic"

suffix=$(date +%s)  # The "+%s" option to 'date' is GNU-specific.
rm -rf experiments
mkdir experiments
nohup $1 > experiments/foo.out 2> experiments/output.txt < /dev/null &
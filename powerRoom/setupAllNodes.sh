#!/bin/bash

echo '
 ______   ______     __     __   __     __     ______   __  __    
/\__  _\ /\  == \   /\ \   /\ "-.\ \   /\ \   /\__  _\ /\ \_\ \   
\/_/\ \/ \ \  __<   \ \ \  \ \ \-.  \  \ \ \  \/_/\ \/ \ \____ \  
   \ \_\  \ \_\ \_\  \ \_\  \ \_\\"\_\  \ \_\    \ \_\  \/\_____\ 
    \/_/   \/_/ /_/   \/_/   \/_/ \/_/   \/_/     \/_/   \/_____/ 
                                                                  '

echo "This script was created to work with 3 Amazon EC2 machines."
echo "It sets up the machines in host.txt (be sure this file exists)."

while read myLine
do
	#echo "host : $myLine"
	PC[$i]="$myLine"
	i=$(($i+1))
done < hosts.txt

echo "****************************************************************"
echo "hosts are: ${PC[*]}"
echo "****************************************************************"
echo "It installs dependencies (make) and affects .ssh/config on the remote machines."

read -p "Do you want to procede (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes";;
  n|N ) echo "Exit";exit 1;;
  * ) echo "invalid";;
esac

#echo "Rebooting all machines..."
#parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "sudo reboot"
#echo "Awaiting boot"
#sleep 300
echo "Checking dependencies"
parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "sudo apt-get update"

parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "sudo apt-get install --yes make gcc openvpn git"
echo "Uploading bitbucket key to machine.."
parallel-scp -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 bitbucket /home/ubuntu/.ssh/deploy 
echo "Changing ssh config for bitbucket"
parallel-scp -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 config /home/ubuntu/.ssh/config
parallel-scp -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 auxiliary/run_nohup.sh /home/ubuntu/run_nohup.sh
echo "Changing permissions of key"
parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "chmod 700 /home/ubuntu/.ssh/deploy"

read -p "Do you want to do a full install (including git clone) (y/n)?" fullchoice
case "$fullchoice" in 
  y|Y ) echo "yes"
        echo "Cloning repos..."
        parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "rm -rf trinity; nohup git clone git@bitbucket.org:aknahs/trinity.git > foo.out 2> foo.err < /dev/null &"

        echo -ne "Awaiting for cloning to conclude...(0%)\r"
        for i in `seq 1 100`;
        do
          echo -ne "Awaiting for cloning to conclude...($i%)\r"
          sleep 3
        done
        echo ""
        ;;
  n|N ) echo "Skipping";;
  * ) echo "invalid";;
esac

echo "Testing projects..."
echo "Testing worker (1/2)..."
parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "cd trinity/trinity-worker; make"
echo "Testing scheduler (2/2)..."
parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "cd trinity/trinity-scheduler; make"
echo "Done!"
#!/bin/bash

SCHEDULERIP="46.137.143.180"
echo "Scheduler IP = $SCHEDULERIP. If this is not your scheduler IP please fix the script."

echo "Updating modules..."
parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "cd trinity; git pull"
echo "Build modules..."
echo "Checking dependencies"
parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "sudo apt-get install --yes make"
echo "Build worker (1/2)..."


parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "cd trinity/trinity-worker; make"
echo "Build scheduler (2/2)..."
parallel-ssh -h hosts.txt -l ubuntu -x "-oStrictHostKeyChecking=no  -i traffic.pem" -t -1 -i "cd trinity/trinity-scheduler; make"

SSHPATH='/home/aknahs/.ssh'
SSHCMD="ssh -oStrictHostKeyChecking=no -i $SSHPATH/traffic.pem "
USER='ubuntu'
i=1
while read myLine
do
	echo "host : $myLine"
	PC[$i]="$myLine"
	i=$(($i+1))
done < hosts.txt

echo "hosts are: ${PC[*]}"

WID=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)"| awk '{print $5}')
xdotool windowfocus $WID

xdotool key ctrl+shift+o
wmctrl -i -a $WID
sleep 1; xdotool type --delay 1 --clearmodifiers "$SSHCMD -l $USER ${PC[1]}"; xdotool key Return;
sleep 2; xdotool type --delay 1 --clearmodifiers 'cp run_nohup.sh trinity/trinity-scheduler; cd trinity/trinity-scheduler; sudo kill $(pgrep scheduler)'; xdotool key Return;
sleep 1; xdotool type --delay 1 --clearmodifiers 'chmod +x run_nohup.sh; ./run_nohup.sh "./scheduler"'; xdotool key Return;
sleep 1; xdotool type --delay 1 --clearmodifiers 'tail -f experiments/output.txt'; xdotool key Return;

xdotool key ctrl+shift+e
#wmctrl -i -a $WID
sleep 1; xdotool type --delay 1 --clearmodifiers "$SSHCMD -l $USER ${PC[2]}"; xdotool key Return;
sleep 2; xdotool type --delay 1 --clearmodifiers 'cp run_nohup.sh trinity/trinity-worker;chmod +x run_nohup.sh;cd trinity/trinity-worker; sudo ./setupTun.sh; sudo kill $(pgrep trinity-worker)'; xdotool key Return;
sleep 10; xdotool type --delay 1 --clearmodifiers "./run_nohup.sh \"./trinity-worker ${SCHEDULERIP}\";"; xdotool key Return;
sleep 1; xdotool type --delay 1 --clearmodifiers 'tail -f experiments/output.txt'; xdotool key Return;

xdotool key ctrl+shift+p
xdotool key ctrl+shift+p
xdotool key ctrl+shift+e
#wmctrl -i -a $WID
sleep 1; xdotool type --delay 1 --clearmodifiers "$SSHCMD -l $USER ${PC[3]}"; xdotool key Return;
sleep 2; xdotool type --delay 1 --clearmodifiers 'cp run_nohup.sh trinity/trinity-worker;chmod +x run_nohup.sh;cd trinity/trinity-worker; sudo ./setupTun.sh; sudo kill $(pgrep trinity-worker)'; xdotool key Return;
sleep 5; xdotool type --delay 1 --clearmodifiers "./run_nohup.sh \"./trinity-worker ${SCHEDULERIP}\";"; xdotool key Return;
sleep 1; xdotool type --delay 1 --clearmodifiers 'tail -f experiments/output.txt'; xdotool key Return;

#echo "Launching scheduler..."
#ssh -oStrictHostKeyChecking=no  -i /home/aknahs/.ssh/traffic.pem ${PC[0]} 'cd trinity/trinity-scheduler;'
echo "Done!"